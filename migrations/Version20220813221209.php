<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220813221209 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE purchasable_product ADD tax_type_id INT NOT NULL');
        $this->addSql('ALTER TABLE purchasable_product ADD CONSTRAINT FK_104E187484042C99 FOREIGN KEY (tax_type_id) REFERENCES tax_type (id)');
        $this->addSql('CREATE INDEX IDX_104E187484042C99 ON purchasable_product (tax_type_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE purchasable_product DROP FOREIGN KEY FK_104E187484042C99');
        $this->addSql('DROP INDEX IDX_104E187484042C99 ON purchasable_product');
        $this->addSql('ALTER TABLE purchasable_product DROP tax_type_id');
    }
}
