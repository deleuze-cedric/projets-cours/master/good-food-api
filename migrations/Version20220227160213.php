<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220227160213 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, image_id INT DEFAULT NULL, parent_id VARCHAR(255) DEFAULT NULL, name VARCHAR(50) NOT NULL, slug VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', activated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', deleted_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', UNIQUE INDEX UNIQ_64C19C1989D9B62 (slug), INDEX IDX_64C19C13DA5256D (image_id), INDEX IDX_64C19C1727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE extra (id INT AUTO_INCREMENT NOT NULL, product_id VARCHAR(15) NOT NULL, multiselect TINYINT(1) NOT NULL, INDEX IDX_4D3F0D654584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE extra_group (id INT AUTO_INCREMENT NOT NULL, description LONGTEXT DEFAULT NULL, name VARCHAR(50) NOT NULL, position INT NOT NULL, slug VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', activated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', deleted_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', UNIQUE INDEX UNIQ_975B0465989D9B62 (slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE extra_value (id INT AUTO_INCREMENT NOT NULL, extra_group_id INT NOT NULL, description LONGTEXT DEFAULT NULL, name VARCHAR(50) NOT NULL, position INT NOT NULL, slug VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', activated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', deleted_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', UNIQUE INDEX UNIQ_E7EC1894989D9B62 (slug), INDEX IDX_E7EC1894B0888114 (extra_group_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `order` (reference VARCHAR(15) NOT NULL, billing_address_id INT DEFAULT NULL, carrier_id INT DEFAULT NULL, customer_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', delivery_address_id INT DEFAULT NULL, payment_method_id INT DEFAULT NULL, restaurant_id INT NOT NULL, status_id INT NOT NULL, last_payment_try_at DATETIME DEFAULT NULL, ordered_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', payment_identifier VARCHAR(100) DEFAULT NULL, refunded_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_F529939879D0C0E4 (billing_address_id), INDEX IDX_F529939821DFC797 (carrier_id), INDEX IDX_F52993989395C3F3 (customer_id), INDEX IDX_F5299398EBF23851 (delivery_address_id), INDEX IDX_F52993985AA1164F (payment_method_id), INDEX IDX_F5299398B1E7706E (restaurant_id), INDEX IDX_F52993986BF700BD (status_id), PRIMARY KEY(reference)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE order_line (id INT AUTO_INCREMENT NOT NULL, currency_id VARCHAR(10) NOT NULL, order_reference_id VARCHAR(15) NOT NULL, product_id VARCHAR(15) NOT NULL, commentary LONGTEXT DEFAULT NULL, depth DOUBLE PRECISION DEFAULT NULL, description LONGTEXT DEFAULT NULL, ean13 VARCHAR(13) DEFAULT NULL, height DOUBLE PRECISION DEFAULT NULL, name VARCHAR(50) NOT NULL, qty INT NOT NULL, tax_amount DOUBLE PRECISION NOT NULL, unit_price_tax_excluded DOUBLE PRECISION NOT NULL, upc VARCHAR(12) DEFAULT NULL, weight DOUBLE PRECISION DEFAULT NULL, width DOUBLE PRECISION DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_9CE58EE138248176 (currency_id), INDEX IDX_9CE58EE112854AC3 (order_reference_id), INDEX IDX_9CE58EE14584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE order_status (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, slug VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', activated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', UNIQUE INDEX UNIQ_B88F75C9989D9B62 (slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE payment_method (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, slug VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', activated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', UNIQUE INDEX UNIQ_7B61A1F6989D9B62 (slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_media (media_id INT NOT NULL, product_id VARCHAR(15) NOT NULL, media_default TINYINT(1) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_CB70DA50EA9FDD75 (media_id), INDEX IDX_CB70DA504584665A (product_id), PRIMARY KEY(media_id, product_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_supplier (product_id VARCHAR(15) NOT NULL, supplier_id VARCHAR(255) NOT NULL, currency_id VARCHAR(10) NOT NULL, supplier_prix_tax_excluded DOUBLE PRECISION NOT NULL, supplier_reference VARCHAR(30) DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_509A06E938248176 (currency_id), INDEX IDX_509A06E94584665A (product_id), INDEX IDX_509A06E92ADD6D8C (supplier_id), PRIMARY KEY(product_id, supplier_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE stock (product_id VARCHAR(15) NOT NULL, restaurant_id VARCHAR(255) NOT NULL, low_stock_alert INT DEFAULT NULL, qty INT NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_4B3656604584665A (product_id), INDEX IDX_4B365660B1E7706E (restaurant_id), PRIMARY KEY(product_id, restaurant_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE supplier (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(30) NOT NULL, slug VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', activated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', deleted_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', UNIQUE INDEX UNIQ_9B2A6C7E989D9B62 (slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tag (id INT AUTO_INCREMENT NOT NULL, value VARCHAR(30) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE variation (reference VARCHAR(15) NOT NULL, variable_product_id VARCHAR(15) NOT NULL, INDEX IDX_629B33EA9CB74AEA (variable_product_id), PRIMARY KEY(reference)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE variation_group (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(30) NOT NULL, position INT NOT NULL, slug VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', activated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', deleted_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', UNIQUE INDEX UNIQ_97B42B00989D9B62 (slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE variation_value (id INT AUTO_INCREMENT NOT NULL, variation_group_id INT NOT NULL, name VARCHAR(30) NOT NULL, position INT NOT NULL, slug VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', activated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', deleted_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', UNIQUE INDEX UNIQ_E70337F1989D9B62 (slug), INDEX IDX_E70337F1DC3D4F5B (variation_group_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE voucher (code VARCHAR(10) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', activated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(code)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE voucher_data (id INT AUTO_INCREMENT NOT NULL, currency_id VARCHAR(10) NOT NULL, voucher_id VARCHAR(10) NOT NULL, amount_reduction_tax_excluded DOUBLE PRECISION DEFAULT NULL, available_from DATETIME DEFAULT NULL, available_to DATETIME DEFAULT NULL, free_shipping_cost TINYINT(1) NOT NULL, name VARCHAR(50) NOT NULL, percentage_reduction_tax_excluded DOUBLE PRECISION DEFAULT NULL, INDEX IDX_AC9E73DB38248176 (currency_id), INDEX IDX_AC9E73DB28AA1B6F (voucher_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE category ADD CONSTRAINT FK_64C19C13DA5256D FOREIGN KEY (image_id) REFERENCES media (id)');
        $this->addSql('ALTER TABLE category ADD CONSTRAINT FK_64C19C1727ACA70 FOREIGN KEY (parent_id) REFERENCES category (slug)');
        $this->addSql('ALTER TABLE extra ADD CONSTRAINT FK_4D3F0D654584665A FOREIGN KEY (product_id) REFERENCES food (reference)');
        $this->addSql('ALTER TABLE extra_value ADD CONSTRAINT FK_E7EC1894B0888114 FOREIGN KEY (extra_group_id) REFERENCES extra_group (id)');
        $this->addSql('ALTER TABLE `order` ADD CONSTRAINT FK_F529939879D0C0E4 FOREIGN KEY (billing_address_id) REFERENCES address (id)');
        $this->addSql('ALTER TABLE `order` ADD CONSTRAINT FK_F529939821DFC797 FOREIGN KEY (carrier_id) REFERENCES carrier_data (id)');
        $this->addSql('ALTER TABLE `order` ADD CONSTRAINT FK_F52993989395C3F3 FOREIGN KEY (customer_id) REFERENCES customer (uuid)');
        $this->addSql('ALTER TABLE `order` ADD CONSTRAINT FK_F5299398EBF23851 FOREIGN KEY (delivery_address_id) REFERENCES address (id)');
        $this->addSql('ALTER TABLE `order` ADD CONSTRAINT FK_F52993985AA1164F FOREIGN KEY (payment_method_id) REFERENCES payment_method (id)');
        $this->addSql('ALTER TABLE `order` ADD CONSTRAINT FK_F5299398B1E7706E FOREIGN KEY (restaurant_id) REFERENCES restaurant (id)');
        $this->addSql('ALTER TABLE `order` ADD CONSTRAINT FK_F52993986BF700BD FOREIGN KEY (status_id) REFERENCES order_status (id)');
        $this->addSql('ALTER TABLE order_line ADD CONSTRAINT FK_9CE58EE138248176 FOREIGN KEY (currency_id) REFERENCES currency (iso_code)');
        $this->addSql('ALTER TABLE order_line ADD CONSTRAINT FK_9CE58EE112854AC3 FOREIGN KEY (order_reference_id) REFERENCES `order` (reference)');
        $this->addSql('ALTER TABLE order_line ADD CONSTRAINT FK_9CE58EE14584665A FOREIGN KEY (product_id) REFERENCES purchasable_product (reference)');
        $this->addSql('ALTER TABLE product_media ADD CONSTRAINT FK_CB70DA50EA9FDD75 FOREIGN KEY (media_id) REFERENCES media (id)');
        $this->addSql('ALTER TABLE product_media ADD CONSTRAINT FK_CB70DA504584665A FOREIGN KEY (product_id) REFERENCES product (reference)');
        $this->addSql('ALTER TABLE product_supplier ADD CONSTRAINT FK_509A06E938248176 FOREIGN KEY (currency_id) REFERENCES currency (iso_code)');
        $this->addSql('ALTER TABLE product_supplier ADD CONSTRAINT FK_509A06E94584665A FOREIGN KEY (product_id) REFERENCES product (reference)');
        $this->addSql('ALTER TABLE product_supplier ADD CONSTRAINT FK_509A06E92ADD6D8C FOREIGN KEY (supplier_id) REFERENCES supplier (slug)');
        $this->addSql('ALTER TABLE stock ADD CONSTRAINT FK_4B3656604584665A FOREIGN KEY (product_id) REFERENCES product (reference)');
        $this->addSql('ALTER TABLE stock ADD CONSTRAINT FK_4B365660B1E7706E FOREIGN KEY (restaurant_id) REFERENCES restaurant (slug)');
        $this->addSql('ALTER TABLE variation ADD CONSTRAINT FK_629B33EA9CB74AEA FOREIGN KEY (variable_product_id) REFERENCES purchasable_product (reference)');
        $this->addSql('ALTER TABLE variation ADD CONSTRAINT FK_629B33EAAEA34913 FOREIGN KEY (reference) REFERENCES product (reference) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE variation_value ADD CONSTRAINT FK_E70337F1DC3D4F5B FOREIGN KEY (variation_group_id) REFERENCES variation_group (id)');
        $this->addSql('ALTER TABLE voucher_data ADD CONSTRAINT FK_AC9E73DB38248176 FOREIGN KEY (currency_id) REFERENCES currency (iso_code)');
        $this->addSql('ALTER TABLE voucher_data ADD CONSTRAINT FK_AC9E73DB28AA1B6F FOREIGN KEY (voucher_id) REFERENCES voucher (code)');
        $this->addSql('ALTER TABLE category_purchasable_product ADD CONSTRAINT FK_9B1DF24D1306E125 FOREIGN KEY (category_slug) REFERENCES category (slug)');
        $this->addSql('ALTER TABLE extra_extra_value ADD CONSTRAINT FK_7411C1902B959FC6 FOREIGN KEY (extra_id) REFERENCES extra (id)');
        $this->addSql('ALTER TABLE extra_extra_value ADD CONSTRAINT FK_7411C190B7FCE3F1 FOREIGN KEY (extra_value_id) REFERENCES extra_value (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE extra_group_restaurant ADD CONSTRAINT FK_75F41A04B0888114 FOREIGN KEY (extra_group_id) REFERENCES extra_group (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE order_voucher_data ADD CONSTRAINT FK_A85EE7B1122432EB FOREIGN KEY (order_reference) REFERENCES `order` (reference)');
        $this->addSql('ALTER TABLE order_voucher_data ADD CONSTRAINT FK_A85EE7B1F4388B3B FOREIGN KEY (voucher_data_id) REFERENCES voucher_data (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE order_line_extra_value ADD CONSTRAINT FK_B29C8C6BBB01DC09 FOREIGN KEY (order_line_id) REFERENCES order_line (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE order_line_extra_value ADD CONSTRAINT FK_B29C8C6BB7FCE3F1 FOREIGN KEY (extra_value_id) REFERENCES extra_value (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE order_line_food ADD CONSTRAINT FK_E3F2A1D6BB01DC09 FOREIGN KEY (order_line_id) REFERENCES order_line (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE purchasable_product_tag ADD CONSTRAINT FK_CBC71995BAD26311 FOREIGN KEY (tag_id) REFERENCES tag (id)');
        $this->addSql('ALTER TABLE variation_variation_value ADD CONSTRAINT FK_125B469CBA807CCA FOREIGN KEY (variation_reference) REFERENCES variation (reference)');
        $this->addSql('ALTER TABLE variation_variation_value ADD CONSTRAINT FK_125B469CDB492DBE FOREIGN KEY (variation_value_id) REFERENCES variation_value (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE voucher_data_customer ADD CONSTRAINT FK_CA5D5E0AF4388B3B FOREIGN KEY (voucher_data_id) REFERENCES voucher_data (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE voucher_data_purchasable_product ADD CONSTRAINT FK_A3C5FF2FF4388B3B FOREIGN KEY (voucher_data_id) REFERENCES voucher_data (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE category DROP FOREIGN KEY FK_64C19C1727ACA70');
        $this->addSql('ALTER TABLE category_purchasable_product DROP FOREIGN KEY FK_9B1DF24D1306E125');
        $this->addSql('ALTER TABLE extra_extra_value DROP FOREIGN KEY FK_7411C1902B959FC6');
        $this->addSql('ALTER TABLE extra_group_restaurant DROP FOREIGN KEY FK_75F41A04B0888114');
        $this->addSql('ALTER TABLE extra_value DROP FOREIGN KEY FK_E7EC1894B0888114');
        $this->addSql('ALTER TABLE extra_extra_value DROP FOREIGN KEY FK_7411C190B7FCE3F1');
        $this->addSql('ALTER TABLE order_line_extra_value DROP FOREIGN KEY FK_B29C8C6BB7FCE3F1');
        $this->addSql('ALTER TABLE order_voucher_data DROP FOREIGN KEY FK_A85EE7B1122432EB');
        $this->addSql('ALTER TABLE order_line DROP FOREIGN KEY FK_9CE58EE112854AC3');
        $this->addSql('ALTER TABLE order_line_extra_value DROP FOREIGN KEY FK_B29C8C6BBB01DC09');
        $this->addSql('ALTER TABLE order_line_food DROP FOREIGN KEY FK_E3F2A1D6BB01DC09');
        $this->addSql('ALTER TABLE `order` DROP FOREIGN KEY FK_F52993986BF700BD');
        $this->addSql('ALTER TABLE `order` DROP FOREIGN KEY FK_F52993985AA1164F');
        $this->addSql('ALTER TABLE product_supplier DROP FOREIGN KEY FK_509A06E92ADD6D8C');
        $this->addSql('ALTER TABLE purchasable_product_tag DROP FOREIGN KEY FK_CBC71995BAD26311');
        $this->addSql('ALTER TABLE variation_variation_value DROP FOREIGN KEY FK_125B469CBA807CCA');
        $this->addSql('ALTER TABLE variation_value DROP FOREIGN KEY FK_E70337F1DC3D4F5B');
        $this->addSql('ALTER TABLE variation_variation_value DROP FOREIGN KEY FK_125B469CDB492DBE');
        $this->addSql('ALTER TABLE voucher_data DROP FOREIGN KEY FK_AC9E73DB28AA1B6F');
        $this->addSql('ALTER TABLE order_voucher_data DROP FOREIGN KEY FK_A85EE7B1F4388B3B');
        $this->addSql('ALTER TABLE voucher_data_customer DROP FOREIGN KEY FK_CA5D5E0AF4388B3B');
        $this->addSql('ALTER TABLE voucher_data_purchasable_product DROP FOREIGN KEY FK_A3C5FF2FF4388B3B');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE extra');
        $this->addSql('DROP TABLE extra_group');
        $this->addSql('DROP TABLE extra_value');
        $this->addSql('DROP TABLE `order`');
        $this->addSql('DROP TABLE order_line');
        $this->addSql('DROP TABLE order_status');
        $this->addSql('DROP TABLE payment_method');
        $this->addSql('DROP TABLE product_media');
        $this->addSql('DROP TABLE product_supplier');
        $this->addSql('DROP TABLE stock');
        $this->addSql('DROP TABLE supplier');
        $this->addSql('DROP TABLE tag');
        $this->addSql('DROP TABLE variation');
        $this->addSql('DROP TABLE variation_group');
        $this->addSql('DROP TABLE variation_value');
        $this->addSql('DROP TABLE voucher');
        $this->addSql('DROP TABLE voucher_data');
    }
}
