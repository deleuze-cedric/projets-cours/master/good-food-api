<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220122162801 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE extra_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE extra (id INT NOT NULL, extra_value_id INT NOT NULL, product_id VARCHAR(15) NOT NULL, multiselect BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_4D3F0D65B7FCE3F1 ON extra (extra_value_id)');
        $this->addSql('CREATE INDEX IDX_4D3F0D654584665A ON extra (product_id)');
        $this->addSql('ALTER TABLE extra ADD CONSTRAINT FK_4D3F0D65B7FCE3F1 FOREIGN KEY (extra_value_id) REFERENCES extra_value (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE extra ADD CONSTRAINT FK_4D3F0D654584665A FOREIGN KEY (product_id) REFERENCES food (reference) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE extra_id_seq CASCADE');
        $this->addSql('DROP TABLE extra');
    }
}
