<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211211092933 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE customer (uuid UUID NOT NULL, birthdate DATE NOT NULL, confirmed_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(uuid))');
        $this->addSql('COMMENT ON COLUMN customer.uuid IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN customer.confirmed_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE employee (uuid UUID NOT NULL, PRIMARY KEY(uuid))');
        $this->addSql('COMMENT ON COLUMN employee.uuid IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE franchise_employee (uuid UUID NOT NULL, PRIMARY KEY(uuid))');
        $this->addSql('COMMENT ON COLUMN franchise_employee.uuid IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE franchisor_employee (uuid UUID NOT NULL, PRIMARY KEY(uuid))');
        $this->addSql('COMMENT ON COLUMN franchisor_employee.uuid IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE super_admin (uuid UUID NOT NULL, PRIMARY KEY(uuid))');
        $this->addSql('COMMENT ON COLUMN super_admin.uuid IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE "user" (uuid UUID NOT NULL, roles JSON NOT NULL, password VARCHAR(64) NOT NULL, lastname VARCHAR(100) NOT NULL, firstname VARCHAR(100) NOT NULL, email VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, activated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(uuid))');
        $this->addSql('COMMENT ON COLUMN "user".uuid IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN "user".created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN "user".updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN "user".activated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN "user".deleted_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE customer ADD CONSTRAINT FK_81398E09D17F50A6 FOREIGN KEY (uuid) REFERENCES "user" (uuid) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE employee ADD CONSTRAINT FK_5D9F75A1D17F50A6 FOREIGN KEY (uuid) REFERENCES "user" (uuid) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE franchise_employee ADD CONSTRAINT FK_D2AF8E5DD17F50A6 FOREIGN KEY (uuid) REFERENCES "user" (uuid) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE franchisor_employee ADD CONSTRAINT FK_9FD76EE9D17F50A6 FOREIGN KEY (uuid) REFERENCES "user" (uuid) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE super_admin ADD CONSTRAINT FK_BC8C2783D17F50A6 FOREIGN KEY (uuid) REFERENCES "user" (uuid) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE customer DROP CONSTRAINT FK_81398E09D17F50A6');
        $this->addSql('ALTER TABLE employee DROP CONSTRAINT FK_5D9F75A1D17F50A6');
        $this->addSql('ALTER TABLE franchise_employee DROP CONSTRAINT FK_D2AF8E5DD17F50A6');
        $this->addSql('ALTER TABLE franchisor_employee DROP CONSTRAINT FK_9FD76EE9D17F50A6');
        $this->addSql('ALTER TABLE super_admin DROP CONSTRAINT FK_BC8C2783D17F50A6');
        $this->addSql('DROP TABLE customer');
        $this->addSql('DROP TABLE employee');
        $this->addSql('DROP TABLE franchise_employee');
        $this->addSql('DROP TABLE franchisor_employee');
        $this->addSql('DROP TABLE super_admin');
        $this->addSql('DROP TABLE "user"');
    }
}
