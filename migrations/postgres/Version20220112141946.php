<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220112141946 extends AbstractMigration
{

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE currency ALTER rate_for_one_euro TYPE NUMERIC(10, 5)');
        $this->addSql('ALTER TABLE tax ALTER rate TYPE NUMERIC(5, 3)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE currency ALTER rate_for_one_euro TYPE NUMERIC(10, 0)');
        $this->addSql('ALTER TABLE tax ALTER rate TYPE NUMERIC(10, 0)');
    }

}
