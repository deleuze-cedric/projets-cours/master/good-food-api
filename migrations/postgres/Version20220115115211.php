<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220115115211 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE tag_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE purchasable_product_tag (purchasable_product_reference VARCHAR(15) NOT NULL, tag_id INT NOT NULL, PRIMARY KEY(purchasable_product_reference, tag_id))');
        $this->addSql('CREATE INDEX IDX_CBC71995504C6194 ON purchasable_product_tag (purchasable_product_reference)');
        $this->addSql('CREATE INDEX IDX_CBC71995BAD26311 ON purchasable_product_tag (tag_id)');
        $this->addSql('CREATE TABLE tag (id INT NOT NULL, value VARCHAR(30) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN tag.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE purchasable_product_tag ADD CONSTRAINT FK_CBC71995504C6194 FOREIGN KEY (purchasable_product_reference) REFERENCES purchasable_product (reference) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE purchasable_product_tag ADD CONSTRAINT FK_CBC71995BAD26311 FOREIGN KEY (tag_id) REFERENCES tag (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE purchasable_product_tag DROP CONSTRAINT FK_CBC71995BAD26311');
        $this->addSql('DROP SEQUENCE tag_id_seq CASCADE');
        $this->addSql('DROP TABLE purchasable_product_tag');
        $this->addSql('DROP TABLE tag');
    }
}
