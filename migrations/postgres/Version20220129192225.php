<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220129192225 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE product_purchasable_product_food_extra_extra_group_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE extra_group_restaurant (extra_group_id INT NOT NULL, restaurant_id INT NOT NULL, PRIMARY KEY(extra_group_id, restaurant_id))');
        $this->addSql('CREATE INDEX IDX_75F41A04B0888114 ON extra_group_restaurant (extra_group_id)');
        $this->addSql('CREATE INDEX IDX_75F41A04B1E7706E ON extra_group_restaurant (restaurant_id)');
        $this->addSql('CREATE TABLE product_purchasable_product_food_extra_extra_group (id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE product_purchasable_product_food_extra_extra_group_restaurant (product_purchasable_product_food_extra_extra_group_id INT NOT NULL, restaurant_id INT NOT NULL, PRIMARY KEY(product_purchasable_product_food_extra_extra_group_id, restaurant_id))');
        $this->addSql('CREATE INDEX IDX_A428D96B307610C4 ON product_purchasable_product_food_extra_extra_group_restaurant (product_purchasable_product_food_extra_extra_group_id)');
        $this->addSql('CREATE INDEX IDX_A428D96BB1E7706E ON product_purchasable_product_food_extra_extra_group_restaurant (restaurant_id)');
        $this->addSql('ALTER TABLE extra_group_restaurant ADD CONSTRAINT FK_75F41A04B0888114 FOREIGN KEY (extra_group_id) REFERENCES extra_group (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE extra_group_restaurant ADD CONSTRAINT FK_75F41A04B1E7706E FOREIGN KEY (restaurant_id) REFERENCES restaurant (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product_purchasable_product_food_extra_extra_group_restaurant ADD CONSTRAINT FK_A428D96B307610C4 FOREIGN KEY (product_purchasable_product_food_extra_extra_group_id) REFERENCES product_purchasable_product_food_extra_extra_group (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product_purchasable_product_food_extra_extra_group_restaurant ADD CONSTRAINT FK_A428D96BB1E7706E FOREIGN KEY (restaurant_id) REFERENCES restaurant (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "order" DROP CONSTRAINT fk_f529939838248176');
        $this->addSql('DROP INDEX idx_f529939838248176');
        $this->addSql('ALTER TABLE "order" DROP currency_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE product_purchasable_product_food_extra_extra_group_restaurant DROP CONSTRAINT FK_A428D96B307610C4');
        $this->addSql('DROP SEQUENCE product_purchasable_product_food_extra_extra_group_id_seq CASCADE');
        $this->addSql('DROP TABLE extra_group_restaurant');
        $this->addSql('DROP TABLE product_purchasable_product_food_extra_extra_group');
        $this->addSql('DROP TABLE product_purchasable_product_food_extra_extra_group_restaurant');
        $this->addSql('ALTER TABLE "order" ADD currency_id VARCHAR(10) DEFAULT NULL');
        $this->addSql('ALTER TABLE "order" ADD CONSTRAINT fk_f529939838248176 FOREIGN KEY (currency_id) REFERENCES currency (iso_code) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_f529939838248176 ON "order" (currency_id)');
    }
}
