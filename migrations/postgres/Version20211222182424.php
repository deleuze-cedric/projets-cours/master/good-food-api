<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211222182424 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE user_request_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE account_confirmation_request (id INT NOT NULL, customer_target_id UUID NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_741C1B71BE73F37C ON account_confirmation_request (customer_target_id)');
        $this->addSql('COMMENT ON COLUMN account_confirmation_request.customer_target_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE new_password_request (id INT NOT NULL, user_target_id UUID NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_375FF3C4156E8682 ON new_password_request (user_target_id)');
        $this->addSql('COMMENT ON COLUMN new_password_request.user_target_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE user_request (id INT NOT NULL, expired_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, already_used BOOLEAN NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN user_request.expired_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN user_request.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE account_confirmation_request ADD CONSTRAINT FK_741C1B71BE73F37C FOREIGN KEY (customer_target_id) REFERENCES customer (uuid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE account_confirmation_request ADD CONSTRAINT FK_741C1B71BF396750 FOREIGN KEY (id) REFERENCES user_request (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE new_password_request ADD CONSTRAINT FK_375FF3C4156E8682 FOREIGN KEY (user_target_id) REFERENCES "user" (uuid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE new_password_request ADD CONSTRAINT FK_375FF3C4BF396750 FOREIGN KEY (id) REFERENCES user_request (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP INDEX uniq_8d93d649e7927c74');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE account_confirmation_request DROP CONSTRAINT FK_741C1B71BF396750');
        $this->addSql('ALTER TABLE new_password_request DROP CONSTRAINT FK_375FF3C4BF396750');
        $this->addSql('DROP SEQUENCE user_request_id_seq CASCADE');
        $this->addSql('DROP TABLE account_confirmation_request');
        $this->addSql('DROP TABLE new_password_request');
        $this->addSql('DROP TABLE user_request');
        $this->addSql('CREATE UNIQUE INDEX uniq_8d93d649e7927c74 ON "user" (email)');
    }
}
