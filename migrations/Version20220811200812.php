<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220811200812 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526C3BE258A');
        $this->addSql('DROP INDEX IDX_9474526C3BE258A ON comment');
        $this->addSql('ALTER TABLE comment ADD purchasable_product VARCHAR(255) DEFAULT NULL, DROP purchasableProduct_id, CHANGE restaurant restaurant VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE comment ADD purchasableProduct_id VARCHAR(15) DEFAULT NULL, DROP purchasable_product, CHANGE restaurant restaurant VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526C3BE258A FOREIGN KEY (purchasableProduct_id) REFERENCES purchasable_product (reference)');
        $this->addSql('CREATE INDEX IDX_9474526C3BE258A ON comment (purchasableProduct_id)');
    }
}
