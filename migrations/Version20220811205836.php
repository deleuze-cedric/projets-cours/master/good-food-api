<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220811205836 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE comment ADD restaurant_id VARCHAR(255) DEFAULT NULL, ADD purchasableProduct_id VARCHAR(15) DEFAULT NULL, DROP restaurant, DROP purchasable_product');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526CB1E7706E FOREIGN KEY (restaurant_id) REFERENCES restaurant (slug)');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526C3BE258A FOREIGN KEY (purchasableProduct_id) REFERENCES purchasable_product (reference)');
        $this->addSql('CREATE INDEX IDX_9474526CB1E7706E ON comment (restaurant_id)');
        $this->addSql('CREATE INDEX IDX_9474526C3BE258A ON comment (purchasableProduct_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526CB1E7706E');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526C3BE258A');
        $this->addSql('DROP INDEX IDX_9474526CB1E7706E ON comment');
        $this->addSql('DROP INDEX IDX_9474526C3BE258A ON comment');
        $this->addSql('ALTER TABLE comment ADD purchasable_product VARCHAR(255) DEFAULT NULL, DROP purchasableProduct_id, CHANGE restaurant_id restaurant VARCHAR(255) DEFAULT NULL');
    }
}
