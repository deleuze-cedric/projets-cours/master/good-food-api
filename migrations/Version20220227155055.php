<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220227155055 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE account_confirmation_request (id INT NOT NULL, customer_target_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_741C1B71BE73F37C (customer_target_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE address (id INT AUTO_INCREMENT NOT NULL, customer_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', country_id VARCHAR(10) NOT NULL, name VARCHAR(50) NOT NULL, slug VARCHAR(255) NOT NULL, additional_address LONGTEXT DEFAULT NULL, city VARCHAR(64) NOT NULL, phone_number VARCHAR(30) NOT NULL, street VARCHAR(100) NOT NULL, zip_code VARCHAR(12) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', deleted_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', UNIQUE INDEX UNIQ_D4E6F81989D9B62 (slug), INDEX IDX_D4E6F819395C3F3 (customer_id), INDEX IDX_D4E6F81F92F3E70 (country_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE carrier (reference VARCHAR(10) NOT NULL, name VARCHAR(30) NOT NULL, site_url VARCHAR(255) DEFAULT NULL, activated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', deleted_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(reference)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE carrier_data (id INT AUTO_INCREMENT NOT NULL, carrier_id VARCHAR(10) NOT NULL, currency_id VARCHAR(10) NOT NULL, tax_id INT NOT NULL, delay VARCHAR(30) NOT NULL, shipping_cost DOUBLE PRECISION DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_FF9E8FE421DFC797 (carrier_id), INDEX IDX_FF9E8FE438248176 (currency_id), INDEX IDX_FF9E8FE4B2A824D8 (tax_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE category_purchasable_product (category_slug VARCHAR(255) NOT NULL, purchasable_product_reference VARCHAR(15) NOT NULL, INDEX IDX_9B1DF24D1306E125 (category_slug), INDEX IDX_9B1DF24D504C6194 (purchasable_product_reference), PRIMARY KEY(category_slug, purchasable_product_reference)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE country (iso_code VARCHAR(10) NOT NULL, default_currency_id VARCHAR(10) NOT NULL, name VARCHAR(50) NOT NULL, phone_prefix VARCHAR(10) DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', activated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', deleted_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_5373C966ECD792C0 (default_currency_id), PRIMARY KEY(iso_code)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE currency (iso_code VARCHAR(10) NOT NULL, name VARCHAR(50) NOT NULL, rate_for_one_euro NUMERIC(10, 5) NOT NULL, symbol VARCHAR(20) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', activated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', deleted_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(iso_code)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE customer (uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', birthdate DATE NOT NULL, confirmed_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(uuid)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dessert (reference VARCHAR(15) NOT NULL, PRIMARY KEY(reference)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dish (reference VARCHAR(15) NOT NULL, PRIMARY KEY(reference)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE drink (reference VARCHAR(15) NOT NULL, PRIMARY KEY(reference)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE employee (uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', PRIMARY KEY(uuid)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE entree (reference VARCHAR(15) NOT NULL, PRIMARY KEY(reference)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE extra_extra_value (extra_id INT NOT NULL, extra_value_id INT NOT NULL, INDEX IDX_7411C1902B959FC6 (extra_id), INDEX IDX_7411C190B7FCE3F1 (extra_value_id), PRIMARY KEY(extra_id, extra_value_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE extra_group_restaurant (extra_group_id INT NOT NULL, restaurant_id INT NOT NULL, INDEX IDX_75F41A04B0888114 (extra_group_id), INDEX IDX_75F41A04B1E7706E (restaurant_id), PRIMARY KEY(extra_group_id, restaurant_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE food (reference VARCHAR(15) NOT NULL, PRIMARY KEY(reference)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE franchise_employee (uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', restaurant_id INT NOT NULL, INDEX IDX_D2AF8E5DB1E7706E (restaurant_id), PRIMARY KEY(uuid)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE franchisor_employee (uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', PRIMARY KEY(uuid)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE mail (id INT AUTO_INCREMENT NOT NULL, addresses LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', content LONGTEXT NOT NULL, debug LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', failed_at DATETIME DEFAULT NULL, last_attempt_send_at DATETIME NOT NULL, sender VARCHAR(255) NOT NULL, subject VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE media (id INT AUTO_INCREMENT NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE menu (reference VARCHAR(15) NOT NULL, PRIMARY KEY(reference)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dessert_menu_choice (menu VARCHAR(15) NOT NULL, dessert VARCHAR(15) NOT NULL, INDEX IDX_AD2C89537D053A93 (menu), INDEX IDX_AD2C895379291B96 (dessert), PRIMARY KEY(menu, dessert)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dish_menu_choice (menu VARCHAR(15) NOT NULL, dish VARCHAR(15) NOT NULL, INDEX IDX_C0FD31AF7D053A93 (menu), INDEX IDX_C0FD31AF957D8CB8 (dish), PRIMARY KEY(menu, dish)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE drink_menu_choice (menu VARCHAR(15) NOT NULL, drink VARCHAR(15) NOT NULL, INDEX IDX_3C708C057D053A93 (menu), INDEX IDX_3C708C05DBE40D1 (drink), PRIMARY KEY(menu, drink)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE entree_menu_choice (menu VARCHAR(15) NOT NULL, entree VARCHAR(15) NOT NULL, INDEX IDX_DF40A3137D053A93 (menu), INDEX IDX_DF40A313598377A6 (entree), PRIMARY KEY(menu, entree)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE new_password_request (id INT NOT NULL, user_target_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_375FF3C4156E8682 (user_target_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE order_voucher_data (order_reference VARCHAR(15) NOT NULL, voucher_data_id INT NOT NULL, INDEX IDX_A85EE7B1122432EB (order_reference), INDEX IDX_A85EE7B1F4388B3B (voucher_data_id), PRIMARY KEY(order_reference, voucher_data_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE order_line_extra_value (order_line_id INT NOT NULL, extra_value_id INT NOT NULL, INDEX IDX_B29C8C6BBB01DC09 (order_line_id), INDEX IDX_B29C8C6BB7FCE3F1 (extra_value_id), PRIMARY KEY(order_line_id, extra_value_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE order_line_food (order_line_id INT NOT NULL, food_reference VARCHAR(15) NOT NULL, INDEX IDX_E3F2A1D6BB01DC09 (order_line_id), INDEX IDX_E3F2A1D636592488 (food_reference), PRIMARY KEY(order_line_id, food_reference)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product (reference VARCHAR(15) NOT NULL, depth DOUBLE PRECISION DEFAULT NULL, description LONGTEXT DEFAULT NULL, ean13 VARCHAR(13) DEFAULT NULL, height DOUBLE PRECISION DEFAULT NULL, name VARCHAR(50) NOT NULL, slug VARCHAR(50) NOT NULL, upc VARCHAR(12) DEFAULT NULL, weight DOUBLE PRECISION DEFAULT NULL, width DOUBLE PRECISION DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', activated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', deleted_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', type VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_D34A04AD989D9B62 (slug), PRIMARY KEY(reference)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_in_restaurant (product VARCHAR(15) NOT NULL, restaurant VARCHAR(255) NOT NULL, INDEX IDX_7EC5667BD34A04AD (product), INDEX IDX_7EC5667BEB95123F (restaurant), PRIMARY KEY(product, restaurant)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE purchasable_product (reference VARCHAR(15) NOT NULL, currency_id VARCHAR(10) NOT NULL, available_from DATETIME DEFAULT NULL, available_to DATETIME DEFAULT NULL, ecotax DOUBLE PRECISION NOT NULL, min_qty_in_stock_to_sell INT NOT NULL, on_sale TINYINT(1) NOT NULL, price_tax_excluded DOUBLE PRECISION NOT NULL, INDEX IDX_104E187438248176 (currency_id), PRIMARY KEY(reference)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE purchasable_product_tag (purchasable_product_reference VARCHAR(15) NOT NULL, tag_id INT NOT NULL, INDEX IDX_CBC71995504C6194 (purchasable_product_reference), INDEX IDX_CBC71995BAD26311 (tag_id), PRIMARY KEY(purchasable_product_reference, tag_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE restaurant (id INT AUTO_INCREMENT NOT NULL, image_id INT DEFAULT NULL, country_id VARCHAR(10) NOT NULL, description LONGTEXT DEFAULT NULL, name VARCHAR(50) NOT NULL, slug VARCHAR(255) NOT NULL, latitude DOUBLE PRECISION NOT NULL, longitude DOUBLE PRECISION NOT NULL, additional_address LONGTEXT DEFAULT NULL, city VARCHAR(64) NOT NULL, phone_number VARCHAR(30) NOT NULL, street VARCHAR(100) NOT NULL, zip_code VARCHAR(12) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', activated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', deleted_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', UNIQUE INDEX UNIQ_EB95123F989D9B62 (slug), INDEX IDX_EB95123F3DA5256D (image_id), INDEX IDX_EB95123FF92F3E70 (country_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE super_admin (uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', PRIMARY KEY(uuid)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tax (id INT AUTO_INCREMENT NOT NULL, country_id VARCHAR(10) NOT NULL, type_id INT NOT NULL, name VARCHAR(30) NOT NULL, rate NUMERIC(5, 3) NOT NULL, slug VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', activated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', deleted_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', UNIQUE INDEX UNIQ_8E81BA76989D9B62 (slug), INDEX IDX_8E81BA76F92F3E70 (country_id), INDEX IDX_8E81BA76C54C8C93 (type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tax_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(30) NOT NULL, slug VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', activated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', deleted_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', UNIQUE INDEX UNIQ_905158D1989D9B62 (slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `user` (uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', firstname VARCHAR(100) NOT NULL, lastname VARCHAR(100) NOT NULL, password VARCHAR(64) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', email VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', activated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', deleted_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', type VARCHAR(255) NOT NULL, PRIMARY KEY(uuid)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_request (id INT AUTO_INCREMENT NOT NULL, already_used TINYINT(1) NOT NULL, expired_at DATETIME NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', type VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE variation_variation_value (variation_reference VARCHAR(15) NOT NULL, variation_value_id INT NOT NULL, INDEX IDX_125B469CBA807CCA (variation_reference), INDEX IDX_125B469CDB492DBE (variation_value_id), PRIMARY KEY(variation_reference, variation_value_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE voucher_data_customer (voucher_data_id INT NOT NULL, customer_uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_CA5D5E0AF4388B3B (voucher_data_id), INDEX IDX_CA5D5E0A85EEF4D1 (customer_uuid), PRIMARY KEY(voucher_data_id, customer_uuid)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE voucher_data_purchasable_product (voucher_data_id INT NOT NULL, purchasable_product_reference VARCHAR(15) NOT NULL, INDEX IDX_A3C5FF2FF4388B3B (voucher_data_id), INDEX IDX_A3C5FF2F504C6194 (purchasable_product_reference), PRIMARY KEY(voucher_data_id, purchasable_product_reference)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE account_confirmation_request ADD CONSTRAINT FK_741C1B71BE73F37C FOREIGN KEY (customer_target_id) REFERENCES customer (uuid)');
        $this->addSql('ALTER TABLE account_confirmation_request ADD CONSTRAINT FK_741C1B71BF396750 FOREIGN KEY (id) REFERENCES user_request (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE address ADD CONSTRAINT FK_D4E6F819395C3F3 FOREIGN KEY (customer_id) REFERENCES customer (uuid)');
        $this->addSql('ALTER TABLE address ADD CONSTRAINT FK_D4E6F81F92F3E70 FOREIGN KEY (country_id) REFERENCES country (iso_code)');
        $this->addSql('ALTER TABLE carrier_data ADD CONSTRAINT FK_FF9E8FE421DFC797 FOREIGN KEY (carrier_id) REFERENCES carrier (reference)');
        $this->addSql('ALTER TABLE carrier_data ADD CONSTRAINT FK_FF9E8FE438248176 FOREIGN KEY (currency_id) REFERENCES currency (iso_code)');
        $this->addSql('ALTER TABLE carrier_data ADD CONSTRAINT FK_FF9E8FE4B2A824D8 FOREIGN KEY (tax_id) REFERENCES tax_type (id)');
        $this->addSql('ALTER TABLE category_purchasable_product ADD CONSTRAINT FK_9B1DF24D504C6194 FOREIGN KEY (purchasable_product_reference) REFERENCES purchasable_product (reference)');
        $this->addSql('ALTER TABLE country ADD CONSTRAINT FK_5373C966ECD792C0 FOREIGN KEY (default_currency_id) REFERENCES currency (iso_code)');
        $this->addSql('ALTER TABLE customer ADD CONSTRAINT FK_81398E09D17F50A6 FOREIGN KEY (uuid) REFERENCES `user` (uuid) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE dessert ADD CONSTRAINT FK_79291B96AEA34913 FOREIGN KEY (reference) REFERENCES product (reference) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE dish ADD CONSTRAINT FK_957D8CB8AEA34913 FOREIGN KEY (reference) REFERENCES product (reference) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE drink ADD CONSTRAINT FK_DBE40D1AEA34913 FOREIGN KEY (reference) REFERENCES product (reference) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE employee ADD CONSTRAINT FK_5D9F75A1D17F50A6 FOREIGN KEY (uuid) REFERENCES `user` (uuid) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE entree ADD CONSTRAINT FK_598377A6AEA34913 FOREIGN KEY (reference) REFERENCES product (reference) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE extra_group_restaurant ADD CONSTRAINT FK_75F41A04B1E7706E FOREIGN KEY (restaurant_id) REFERENCES restaurant (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE food ADD CONSTRAINT FK_D43829F7AEA34913 FOREIGN KEY (reference) REFERENCES product (reference) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE franchise_employee ADD CONSTRAINT FK_D2AF8E5DB1E7706E FOREIGN KEY (restaurant_id) REFERENCES restaurant (id)');
        $this->addSql('ALTER TABLE franchise_employee ADD CONSTRAINT FK_D2AF8E5DD17F50A6 FOREIGN KEY (uuid) REFERENCES `user` (uuid) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE franchisor_employee ADD CONSTRAINT FK_9FD76EE9D17F50A6 FOREIGN KEY (uuid) REFERENCES `user` (uuid) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE menu ADD CONSTRAINT FK_7D053A93AEA34913 FOREIGN KEY (reference) REFERENCES product (reference) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE dessert_menu_choice ADD CONSTRAINT FK_AD2C89537D053A93 FOREIGN KEY (menu) REFERENCES menu (reference)');
        $this->addSql('ALTER TABLE dessert_menu_choice ADD CONSTRAINT FK_AD2C895379291B96 FOREIGN KEY (dessert) REFERENCES dessert (reference)');
        $this->addSql('ALTER TABLE dish_menu_choice ADD CONSTRAINT FK_C0FD31AF7D053A93 FOREIGN KEY (menu) REFERENCES menu (reference)');
        $this->addSql('ALTER TABLE dish_menu_choice ADD CONSTRAINT FK_C0FD31AF957D8CB8 FOREIGN KEY (dish) REFERENCES dish (reference)');
        $this->addSql('ALTER TABLE drink_menu_choice ADD CONSTRAINT FK_3C708C057D053A93 FOREIGN KEY (menu) REFERENCES menu (reference)');
        $this->addSql('ALTER TABLE drink_menu_choice ADD CONSTRAINT FK_3C708C05DBE40D1 FOREIGN KEY (drink) REFERENCES drink (reference)');
        $this->addSql('ALTER TABLE entree_menu_choice ADD CONSTRAINT FK_DF40A3137D053A93 FOREIGN KEY (menu) REFERENCES menu (reference)');
        $this->addSql('ALTER TABLE entree_menu_choice ADD CONSTRAINT FK_DF40A313598377A6 FOREIGN KEY (entree) REFERENCES entree (reference)');
        $this->addSql('ALTER TABLE new_password_request ADD CONSTRAINT FK_375FF3C4156E8682 FOREIGN KEY (user_target_id) REFERENCES `user` (uuid)');
        $this->addSql('ALTER TABLE new_password_request ADD CONSTRAINT FK_375FF3C4BF396750 FOREIGN KEY (id) REFERENCES user_request (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE order_line_food ADD CONSTRAINT FK_E3F2A1D636592488 FOREIGN KEY (food_reference) REFERENCES food (reference)');
        $this->addSql('ALTER TABLE product_in_restaurant ADD CONSTRAINT FK_7EC5667BD34A04AD FOREIGN KEY (product) REFERENCES product (reference)');
        $this->addSql('ALTER TABLE product_in_restaurant ADD CONSTRAINT FK_7EC5667BEB95123F FOREIGN KEY (restaurant) REFERENCES restaurant (slug)');
        $this->addSql('ALTER TABLE purchasable_product ADD CONSTRAINT FK_104E187438248176 FOREIGN KEY (currency_id) REFERENCES currency (iso_code)');
        $this->addSql('ALTER TABLE purchasable_product ADD CONSTRAINT FK_104E1874AEA34913 FOREIGN KEY (reference) REFERENCES product (reference) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE purchasable_product_tag ADD CONSTRAINT FK_CBC71995504C6194 FOREIGN KEY (purchasable_product_reference) REFERENCES purchasable_product (reference)');
        $this->addSql('ALTER TABLE restaurant ADD CONSTRAINT FK_EB95123F3DA5256D FOREIGN KEY (image_id) REFERENCES media (id)');
        $this->addSql('ALTER TABLE restaurant ADD CONSTRAINT FK_EB95123FF92F3E70 FOREIGN KEY (country_id) REFERENCES country (iso_code)');
        $this->addSql('ALTER TABLE super_admin ADD CONSTRAINT FK_BC8C2783D17F50A6 FOREIGN KEY (uuid) REFERENCES `user` (uuid) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tax ADD CONSTRAINT FK_8E81BA76F92F3E70 FOREIGN KEY (country_id) REFERENCES country (iso_code)');
        $this->addSql('ALTER TABLE tax ADD CONSTRAINT FK_8E81BA76C54C8C93 FOREIGN KEY (type_id) REFERENCES tax_type (id)');
        $this->addSql('ALTER TABLE voucher_data_customer ADD CONSTRAINT FK_CA5D5E0A85EEF4D1 FOREIGN KEY (customer_uuid) REFERENCES customer (uuid)');
        $this->addSql('ALTER TABLE voucher_data_purchasable_product ADD CONSTRAINT FK_A3C5FF2F504C6194 FOREIGN KEY (purchasable_product_reference) REFERENCES purchasable_product (reference)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE carrier_data DROP FOREIGN KEY FK_FF9E8FE421DFC797');
        $this->addSql('ALTER TABLE address DROP FOREIGN KEY FK_D4E6F81F92F3E70');
        $this->addSql('ALTER TABLE restaurant DROP FOREIGN KEY FK_EB95123FF92F3E70');
        $this->addSql('ALTER TABLE tax DROP FOREIGN KEY FK_8E81BA76F92F3E70');
        $this->addSql('ALTER TABLE carrier_data DROP FOREIGN KEY FK_FF9E8FE438248176');
        $this->addSql('ALTER TABLE country DROP FOREIGN KEY FK_5373C966ECD792C0');
        $this->addSql('ALTER TABLE purchasable_product DROP FOREIGN KEY FK_104E187438248176');
        $this->addSql('ALTER TABLE account_confirmation_request DROP FOREIGN KEY FK_741C1B71BE73F37C');
        $this->addSql('ALTER TABLE address DROP FOREIGN KEY FK_D4E6F819395C3F3');
        $this->addSql('ALTER TABLE voucher_data_customer DROP FOREIGN KEY FK_CA5D5E0A85EEF4D1');
        $this->addSql('ALTER TABLE dessert_menu_choice DROP FOREIGN KEY FK_AD2C895379291B96');
        $this->addSql('ALTER TABLE dish_menu_choice DROP FOREIGN KEY FK_C0FD31AF957D8CB8');
        $this->addSql('ALTER TABLE drink_menu_choice DROP FOREIGN KEY FK_3C708C05DBE40D1');
        $this->addSql('ALTER TABLE entree_menu_choice DROP FOREIGN KEY FK_DF40A313598377A6');
        $this->addSql('ALTER TABLE order_line_food DROP FOREIGN KEY FK_E3F2A1D636592488');
        $this->addSql('ALTER TABLE restaurant DROP FOREIGN KEY FK_EB95123F3DA5256D');
        $this->addSql('ALTER TABLE dessert_menu_choice DROP FOREIGN KEY FK_AD2C89537D053A93');
        $this->addSql('ALTER TABLE dish_menu_choice DROP FOREIGN KEY FK_C0FD31AF7D053A93');
        $this->addSql('ALTER TABLE drink_menu_choice DROP FOREIGN KEY FK_3C708C057D053A93');
        $this->addSql('ALTER TABLE entree_menu_choice DROP FOREIGN KEY FK_DF40A3137D053A93');
        $this->addSql('ALTER TABLE dessert DROP FOREIGN KEY FK_79291B96AEA34913');
        $this->addSql('ALTER TABLE dish DROP FOREIGN KEY FK_957D8CB8AEA34913');
        $this->addSql('ALTER TABLE drink DROP FOREIGN KEY FK_DBE40D1AEA34913');
        $this->addSql('ALTER TABLE entree DROP FOREIGN KEY FK_598377A6AEA34913');
        $this->addSql('ALTER TABLE food DROP FOREIGN KEY FK_D43829F7AEA34913');
        $this->addSql('ALTER TABLE menu DROP FOREIGN KEY FK_7D053A93AEA34913');
        $this->addSql('ALTER TABLE product_in_restaurant DROP FOREIGN KEY FK_7EC5667BD34A04AD');
        $this->addSql('ALTER TABLE purchasable_product DROP FOREIGN KEY FK_104E1874AEA34913');
        $this->addSql('ALTER TABLE category_purchasable_product DROP FOREIGN KEY FK_9B1DF24D504C6194');
        $this->addSql('ALTER TABLE purchasable_product_tag DROP FOREIGN KEY FK_CBC71995504C6194');
        $this->addSql('ALTER TABLE voucher_data_purchasable_product DROP FOREIGN KEY FK_A3C5FF2F504C6194');
        $this->addSql('ALTER TABLE extra_group_restaurant DROP FOREIGN KEY FK_75F41A04B1E7706E');
        $this->addSql('ALTER TABLE franchise_employee DROP FOREIGN KEY FK_D2AF8E5DB1E7706E');
        $this->addSql('ALTER TABLE product_in_restaurant DROP FOREIGN KEY FK_7EC5667BEB95123F');
        $this->addSql('ALTER TABLE carrier_data DROP FOREIGN KEY FK_FF9E8FE4B2A824D8');
        $this->addSql('ALTER TABLE tax DROP FOREIGN KEY FK_8E81BA76C54C8C93');
        $this->addSql('ALTER TABLE customer DROP FOREIGN KEY FK_81398E09D17F50A6');
        $this->addSql('ALTER TABLE employee DROP FOREIGN KEY FK_5D9F75A1D17F50A6');
        $this->addSql('ALTER TABLE franchise_employee DROP FOREIGN KEY FK_D2AF8E5DD17F50A6');
        $this->addSql('ALTER TABLE franchisor_employee DROP FOREIGN KEY FK_9FD76EE9D17F50A6');
        $this->addSql('ALTER TABLE new_password_request DROP FOREIGN KEY FK_375FF3C4156E8682');
        $this->addSql('ALTER TABLE super_admin DROP FOREIGN KEY FK_BC8C2783D17F50A6');
        $this->addSql('ALTER TABLE account_confirmation_request DROP FOREIGN KEY FK_741C1B71BF396750');
        $this->addSql('ALTER TABLE new_password_request DROP FOREIGN KEY FK_375FF3C4BF396750');
        $this->addSql('DROP TABLE account_confirmation_request');
        $this->addSql('DROP TABLE address');
        $this->addSql('DROP TABLE carrier');
        $this->addSql('DROP TABLE carrier_data');
        $this->addSql('DROP TABLE category_purchasable_product');
        $this->addSql('DROP TABLE country');
        $this->addSql('DROP TABLE currency');
        $this->addSql('DROP TABLE customer');
        $this->addSql('DROP TABLE dessert');
        $this->addSql('DROP TABLE dish');
        $this->addSql('DROP TABLE drink');
        $this->addSql('DROP TABLE employee');
        $this->addSql('DROP TABLE entree');
        $this->addSql('DROP TABLE extra_extra_value');
        $this->addSql('DROP TABLE extra_group_restaurant');
        $this->addSql('DROP TABLE food');
        $this->addSql('DROP TABLE franchise_employee');
        $this->addSql('DROP TABLE franchisor_employee');
        $this->addSql('DROP TABLE mail');
        $this->addSql('DROP TABLE media');
        $this->addSql('DROP TABLE menu');
        $this->addSql('DROP TABLE dessert_menu_choice');
        $this->addSql('DROP TABLE dish_menu_choice');
        $this->addSql('DROP TABLE drink_menu_choice');
        $this->addSql('DROP TABLE entree_menu_choice');
        $this->addSql('DROP TABLE new_password_request');
        $this->addSql('DROP TABLE order_voucher_data');
        $this->addSql('DROP TABLE order_line_extra_value');
        $this->addSql('DROP TABLE order_line_food');
        $this->addSql('DROP TABLE product');
        $this->addSql('DROP TABLE product_in_restaurant');
        $this->addSql('DROP TABLE purchasable_product');
        $this->addSql('DROP TABLE purchasable_product_tag');
        $this->addSql('DROP TABLE restaurant');
        $this->addSql('DROP TABLE super_admin');
        $this->addSql('DROP TABLE tax');
        $this->addSql('DROP TABLE tax_type');
        $this->addSql('DROP TABLE `user`');
        $this->addSql('DROP TABLE user_request');
        $this->addSql('DROP TABLE variation_variation_value');
        $this->addSql('DROP TABLE voucher_data_customer');
        $this->addSql('DROP TABLE voucher_data_purchasable_product');
    }
}
