<?php
// tests/AuthenticationTest.php

namespace App\Tests\unit;

use App\Classes\CrudApiTestCase;
use App\Entity\Country\Tax\TaxType;
use App\Entity\User\User;
use App\Service\Tools\Tools;
use App\Traits\Tests\AdminExampleTrait;
use App\Traits\Tests\TaxTypeExampleTrait;
use Exception;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class TaxTypeTest extends CrudApiTestCase
{

    use AdminExampleTrait;
    use TaxTypeExampleTrait;

    // Fixtures
    const TAX_TYPE_FIXTURES_FILES = 'tax_type.yml';

    // Routes
    const TAX_TYPE_ROUTE = self::API_ROUTE . '/tax_types';
    const TAX_TYPE_ITEM_ROUTE = self::TAX_TYPE_ROUTE . '/{slug}';
    const TAX_TYPE_ACTIVE_ROUTE = self::TAX_TYPE_ITEM_ROUTE . '/active';

    // JSON LD Data
    const TAX_TYPE_TYPE = 'TaxType';
    const TAX_TYPE_CONTEXT = self::CONTEXT . '/' . self::TAX_TYPE_TYPE;

    // Data
    const ALL_FIELDS_VIOLATION = ['name'];
    const TAX_TYPE_EXAMPLE = "tauxStandard";

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws Exception
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAdmin();
        $this->loadTaxType();
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws ExceptionInterface
     */
    public function testPostInvalidItem()
    {
        // Empty request error
        $requestEmptyData = $this->doRequest(
            method: self::POST_METHOD,
            url: self::TAX_TYPE_ROUTE,
            json: [],
            token: $this->adminExampleToken,
        );

        $this->assertViolations(self::ALL_FIELDS_VIOLATION, $requestEmptyData);

        // Min carac error

        $requestNotRespectMinValidation = $this->doRequest(
            method: self::POST_METHOD,
            url: self::TAX_TYPE_ROUTE,
            json: [
                'name' => "",
            ],
            token: $this->adminExampleToken,
        );

        $this->assertViolations(self::ALL_FIELDS_VIOLATION, $requestNotRespectMinValidation);

        // Max carac error

        $requestNotRespectMaxValidation = $this->doRequest(
            method: self::POST_METHOD,
            url: self::TAX_TYPE_ROUTE,
            json: [
                'name' => self::STRING_LENGTH_256,
            ],
            token: $this->adminExampleToken,
        );

        $this->assertViolations(self::ALL_FIELDS_VIOLATION, $requestNotRespectMaxValidation);

    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws ExceptionInterface
     * @throws Exception
     */
    public function testPostItem()
    {
        $data = $this->getValidTestTaxTypeInJSON();
        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                self::CEDRIC_ADMIN,
                self::ARTHUR_ADMIN,
                self::CORENTIN_ADMIN,
                self::ROLE_ADMIN,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $validJSON = $json;

                $taxTypeCreated = $response->toArray();

                $this->assertSuccess();
                $this->assertJsonContains(
                    [
                        self::CONTEXT_INDEX_JSON_LD => self::TAX_TYPE_CONTEXT,
                        self::TYPE_INDEX_JSON_LD    => self::TAX_TYPE_TYPE,
                        'deleted'                   => false,
                    ]
                );
                $this->assertJsonContains($validJSON);

                $this->assertIdentifierOfResponseData(self::TAX_TYPE_ROUTE, Tools::SLUG_REGEX, $taxTypeCreated);
                $this->assertMatchesResourceItemJsonSchema(TaxType::class);

            },
            method: self::POST_METHOD,
            url: self::TAX_TYPE_ROUTE,
            json: $data
        );

    }

    /**
     * @throws Exception
     * @throws ExceptionInterface
     */
    public static function getValidTestTaxTypeInJSON(): array
    {
        $taxType = self::getValidTestTaxType();
        $data = self::parseEntityToArray($taxType);
        unset($data['slug']);

        return $data;
    }

    /**
     * @throws Exception
     */
    public static function getValidTestTaxType(): TaxType
    {
        $taxType = new TaxType();
        $taxType->setName("TaxType Test");
        $taxType->setSlug(self::slugify($taxType->getName()));

        return $taxType;
    }

    /**
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws Exception
     */
    public function testDeleteItem(): void
    {
        $superAdminToken = $this->getTokenFromUserIndex(self::CEDRIC_ADMIN);
        $nbCustomersInCollectionBefore = $this->getTotalItemsForRequest(self::TAX_TYPE_ROUTE, $superAdminToken);

        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                self::CEDRIC_ADMIN,
                self::ARTHUR_ADMIN,
                self::CORENTIN_ADMIN,
                self::ROLE_ADMIN,
                $this->adminExampleIndex,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $this->assertSuccessDelete();

                $this->assertNull(self::getEntityManager()->getRepository(TaxType::class)->findOneBy(['slug' => $this->taxTypeExample->getSlug()]));

                $this->doRequest(
                    method: self::GET_METHOD,
                    url: $this->taxTypeExampleItemIRI,
                    token: $superAdminToken,
                );

                $this->assertNotFound();

                $nbCustomersInCollectionAfter = $this->getTotalItemsForRequest(self::TAX_TYPE_ROUTE, $superAdminToken);

                $this->assertEquals($nbCustomersInCollectionBefore - 1, $nbCustomersInCollectionAfter);

                $this->initClient();

            },
            method: self::DELETE_METHOD,
            url: $this->taxTypeExampleItemIRI,
            dataToSendToFunctionAssertAhtorizeUser: compact('nbCustomersInCollectionBefore', 'superAdminToken')
        );
    }

    /**
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function testGetCollection(): void
    {

        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                self::ALL_USERS,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $this->assertCollection(
                    response: $response,
                    endpointCollectionRoute: self::TAX_TYPE_ROUTE,
                    context: self::TAX_TYPE_CONTEXT,
                    classToCheckValidity: TaxType::class,
                    totalItems: count(self::getTaxTypeFixtures())
                );
            },
            method: self::GET_METHOD,
            url: self::TAX_TYPE_ROUTE,
        );
    }

    /**
     * @return TaxType[]
     */
    #[Pure] public static function getTaxTypeFixtures(): array
    {
        return self::getDataFixturesOfClass(TaxType::class);
    }

    /**
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function testGetItem(): void
    {

        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                self::ALL_USERS,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $this->assertSuccess();

                //        Vérification du contenu du retour
                $this->assertJsonContains(
                    [
                        self::CONTEXT_INDEX_JSON_LD => self::TAX_TYPE_CONTEXT,
                        self::TYPE_INDEX_JSON_LD    => self::TAX_TYPE_TYPE,
                        'active'                    => $this->taxTypeExample->isActive(),
                        'deleted'                   => $this->taxTypeExample->isDeleted(),
                    ]
                );

                $validJson = self::parseEntityToArray($this->taxTypeExample);

                $this->assertJsonContains($validJson);

                $taxType = $response->toArray();

                $this->assertIdentifierOfResponseData(self::TAX_TYPE_ROUTE, Tools::SLUG_REGEX, $taxType);
                $this->assertMatchesResourceItemJsonSchema(TaxType::class);

            },
            method: self::GET_METHOD,
            url: $this->taxTypeExampleItemIRI,
        );
    }

    /**
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ExceptionInterface
     * @throws Exception
     */
    public function testPatchItem(): void
    {
        $taxTypeTest = new TaxType();
        $taxTypeTest->setName("a" . $this->taxTypeExample->getName());
        $taxTypeTest->setSlug(self::slugify($taxTypeTest->getName()));
        $taxTypeTest->setActive(!$this->taxTypeExample->isActive());

        $json = self::parseEntityToArray($taxTypeTest);

        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                self::CEDRIC_ADMIN,
                self::ARTHUR_ADMIN,
                self::CORENTIN_ADMIN,
                self::ROLE_ADMIN,
                $this->adminExampleIndex,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $this->assertSuccess();
                //        Vérification du contenu du retour
                $this->assertJsonContains(
                    [
                        self::CONTEXT_INDEX_JSON_LD => self::TAX_TYPE_CONTEXT,
                        self::TYPE_INDEX_JSON_LD    => self::TAX_TYPE_TYPE,
                        'deleted'                   => false,
                    ]
                );
                $this->assertJsonContains($json);

                $taxType = $response->toArray();

                $this->assertIdentifierOfResponseData(self::TAX_TYPE_ROUTE, Tools::SLUG_REGEX, $taxType);
                $this->assertMatchesResourceItemJsonSchema(TaxType::class);

                $this->initClient();
            },
            method: self::PATCH_METHOD,
            url: $this->taxTypeExampleItemIRI,
            json: $json,
        );
    }

    public function testPatchInvalidItem()
    {
        // TODO: Implement testPatchInvalidItem() method.
    }

    public function testGetCollectionWithFilter()
    {
        // TODO: Implement testGetCollectionWithFilter() method.
    }

}
