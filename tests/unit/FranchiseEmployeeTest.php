<?php
// tests/AuthenticationTest.php

namespace App\Tests\unit;

use App\Classes\CrudApiTestCase;
use App\Entity\Restaurant\Restaurant;
use App\Entity\User\Employee\Employee;
use App\Entity\User\Employee\FranchiseEmployee;
use App\Entity\User\User;
use App\Service\Tools\Tools;
use App\Traits\Tests\LogisticianExampleTrait;
use App\Traits\Tests\RestaurantExampleTrait;
use App\Traits\Tests\SuperAdminExampleTrait;
use Exception;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Uid\Uuid;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class FranchiseEmployeeTest extends CrudApiTestCase
{

    use LogisticianExampleTrait;
    use SuperAdminExampleTrait;
    use RestaurantExampleTrait;

    // Fixtures
    const FRANCHISE_EMPLOYEE_FIXTURES_FILE = 'franchise_employee.yml';

    // Routes
    const FRANCHISE_EMPLOYEE_ROUTE = self::API_ROUTE . '/franchise_employees';
    const FRANCHISE_EMPLOYEE_ITEM_ROUTE = self::FRANCHISE_EMPLOYEE_ROUTE . '/{uuid}';
    const FRANCHISE_EMPLOYEE_ACTIVE_ACCOUNT_ROUTE = self::FRANCHISE_EMPLOYEE_ITEM_ROUTE . '/active';

    // JSON LD Data
    const FRANCHISE_EMPLOYEE_TYPE = 'FranchiseEmployee';
    const FRANCHISE_EMPLOYEE_CONTEXT = self::CONTEXT . '/' . self::FRANCHISE_EMPLOYEE_TYPE;

    // Data
    const ALL_FIELDS_VIOLATION = ['email', 'lastname', 'firstname', 'password'];

    /**
     * @return FranchiseEmployee[]
     */
    #[Pure] public static function getFranchiseEmployeeFixtures(): array
    {
        return self::getDataFixturesOfClass(FranchiseEmployee::class);
    }

    /**
     * @throws Exception
     */
    public static function getValidTestFranchiseEmployee(
        Restaurant $restaurant
    ): FranchiseEmployee
    {
        $franchiseEmployee = new FranchiseEmployee();
        $franchiseEmployee->setUuid(Uuid::v4());
        $franchiseEmployee->setPassword("aaAA&&12");
        $franchiseEmployee->setLastname("FRANCHISE_EMLPOYEE");
        $franchiseEmployee->setFirstname("Good Food");
        $franchiseEmployee->setEmail(self::getTestEmail());
        $franchiseEmployee->setRestaurant($restaurant);

        return $franchiseEmployee;
    }

    /**
     * @throws Exception
     * @throws ExceptionInterface
     */
    public static function getValidTestFranchiseEmployeeInJSON(
        Restaurant $restaurant
    ): array
    {
        $franchiseEmployee = self::getValidTestFranchiseEmployee($restaurant);
        $data = self::parseEntityToArray($franchiseEmployee);
        $data['password'] = $franchiseEmployee->getPassword() ?? $franchiseEmployee->getPlainPassword();
        $data['restaurant'] = self::replaceParamsRoute(RestaurantTest::RESTAURANT_ITEM_ROUTE, ['slug' => $restaurant->getSlug()]);
        unset($data['uuid']);

        return $data;
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws Exception
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadSuperAdmin();
        $this->loadLogistician();
        $this->loadRestaurant();
    }

    /**
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ExceptionInterface
     */
    public function testPostItem()
    {
        $testUser = $this->getValidTestFranchiseEmployeeInJSON($this->restaurantExample);

        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                self::CEDRIC_ADMIN,
                self::ARTHUR_ADMIN,
                self::CORENTIN_ADMIN,
                self::ROLE_ADMIN,
                self::ROLE_LEAD,
                self::ROLE_HR,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $validUser = $json;
                $password = $validUser['password'];
                $email = $validUser['email'];
                unset($validUser['password']);

                $validUser['restaurant'] = [
                    self::ID_INDEX_JSON_LD   => $validUser['restaurant'],
                    self::TYPE_INDEX_JSON_LD => RestaurantTest::RESTAURANT_TYPE,
                    'street'                 => $this->restaurantExample->getStreet(),
                    'zipCode'                => $this->restaurantExample->getZipCode(),
                    'city'                   => $this->restaurantExample->getCity(),
                    'phoneNumber'            => $this->restaurantExample->getPhoneNumber(),
                    'active'                 => $this->restaurantExample->isActive(),
                    'deleted'                => $this->restaurantExample->isDeleted(),
                ];

                $this->assertSuccess();
                $this->assertJsonContains(
                    [
                        self::CONTEXT_INDEX_JSON_LD => self::FRANCHISE_EMPLOYEE_CONTEXT,
                        self::TYPE_INDEX_JSON_LD    => self::FRANCHISE_EMPLOYEE_TYPE,
                        "roles"                     => [Employee::DEFAULT_ROLE_EMPLOYEE, FranchiseEmployee::DEFAULT_ROLE_FRANCHISE],
                        'active'                    => true,
                        'deleted'                   => false,
                    ]
                );
                self::assertJsonContains($validUser);

                $employeeCreated = $response->toArray();

                $this->assertIdentifierOfResponseData(self::FRANCHISE_EMPLOYEE_ROUTE, Tools::UUID_REGEX, $employeeCreated);
                $this->assertMatchesResourceItemJsonSchema(FranchiseEmployee::class);

                $token = $this->getTokenForUser($email, $password);
                $this->assertNotNull($token);

                // Vérification que l'employé remonte bien dans la liste du restaurant
                $routeToRestaurantEmployees = $this->replaceParamsRoute(RestaurantTest::RESTAURANT_EMPLOYEES_ROUTE, ['slug' => $this->restaurantExample->getSlug()]);

                $response = $this->doRequest(
                    method: self::GET_METHOD,
                    url: $routeToRestaurantEmployees,
                    token: $token
                );

                $this->assertSubResourceCollection(
                    response: $response,
                    endpointSubResourceCollectionRoute: $routeToRestaurantEmployees,
                    context: self::FRANCHISE_EMPLOYEE_CONTEXT,
                    classToCheckValidity: FranchiseEmployee::class,
                    totalItems: count(self::getFranchiseEmployeeFixtures()) + 1,
                    entityCreated: $employeeCreated
                );

                $this->initClient();
                $this->loadRestaurant();

            },
            method: self::POST_METHOD,
            url: self::FRANCHISE_EMPLOYEE_ROUTE,
            json: $testUser
        );
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function testPostInvalidItem()
    {
        $requestEmptyData = $this->doRequest(
            method: self::POST_METHOD,
            url: self::FRANCHISE_EMPLOYEE_ROUTE,
            json: [],
            token: $this->superAdminExampleToken
        );

        $this->assertViolations(self::ALL_FIELDS_VIOLATION, $requestEmptyData);

        // Max carac lastname, password, firstname, birthdate
        // wrong format email
        $requestNotRespectMinValidation = $this->doRequest(
            method: self::POST_METHOD,
            url: self::FRANCHISE_EMPLOYEE_ROUTE,
            json: [
                "password"  => 'aa&&11',
                "lastname"  => 'a',
                "firstname" => 'a',
                "email"     => 'mail non valide',
            ],
            token: $this->superAdminExampleToken
        );

        $this->assertViolations(self::ALL_FIELDS_VIOLATION, $requestNotRespectMinValidation);

//        Max carac lastname, firstname
        $requestNotRespectMaxValidation = $this->doRequest(
            method: self::POST_METHOD,
            url: self::FRANCHISE_EMPLOYEE_ROUTE,
            json: [
                "lastname"  => self::STRING_LENGTH_256,
                "firstname" => self::STRING_LENGTH_256,
            ],
            token: $this->superAdminExampleToken
        );

        $this->assertViolations(self::ALL_FIELDS_VIOLATION, $requestNotRespectMaxValidation);

    }

    /**
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function testGetCollection(): void
    {
        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                self::CEDRIC_ADMIN,
                self::ARTHUR_ADMIN,
                self::CORENTIN_ADMIN,
                self::ROLE_ADMIN,
                self::ROLE_HR,
                self::ROLE_LEAD,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $this->assertCollection(
                    response: $response,
                    endpointCollectionRoute: self::FRANCHISE_EMPLOYEE_ROUTE,
                    context: self::FRANCHISE_EMPLOYEE_CONTEXT,
                    classToCheckValidity: FranchiseEmployee::class,
                    totalItems: count(self::getFranchiseEmployeeFixtures())
                );

            },
            method: self::GET_METHOD,
            url: self::FRANCHISE_EMPLOYEE_ROUTE,
        );
    }

    /**
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function testGetItem(): void
    {
        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                self::CEDRIC_ADMIN,
                self::ARTHUR_ADMIN,
                self::CORENTIN_ADMIN,
                self::ROLE_ADMIN,
                self::ROLE_HR,
                self::ROLE_LEAD,
                $this->logisticianExampleIndex,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $this->assertSuccess();
                $this->assertJsonContains(
                    [
                        self::CONTEXT_INDEX_JSON_LD => self::FRANCHISE_EMPLOYEE_CONTEXT,
                        self::TYPE_INDEX_JSON_LD    => self::FRANCHISE_EMPLOYEE_TYPE,
                        "lastname"                  => $this->logisticianExample->getLastname(),
                        "firstname"                 => $this->logisticianExample->getFirstname(),
                        "email"                     => $this->logisticianExample->getEmail(),
                        "roles"                     => $this->logisticianExample->getRoles(),
                        'active'                    => $this->logisticianExample->isActive(),
                        'deleted'                   => $this->logisticianExample->isDeleted(),
                    ]
                );

                $franchiseEmployee = $response->toArray();

                $this->assertIdentifierOfResponseData(self::FRANCHISE_EMPLOYEE_ROUTE, Tools::UUID_REGEX, $franchiseEmployee);
                $this->assertMatchesResourceItemJsonSchema(FranchiseEmployee::class);

            },
            method: self::GET_METHOD,
            url: $this->logisticianExampleItemIRI,
        );
    }

    /**
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testPatchItem(): void
    {
        $json = [
            "password"  => self::PASSWORDS[$this->logisticianExampleIndex],
            "lastname"  => "1" . $this->logisticianExample->getLastname(),
            "firstname" => "1" . $this->logisticianExample->getFirstname(),
            "email"     => "1" . $this->logisticianExample->getEmail(),
        ];

        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                $this->logisticianExampleIndex,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $verifJSON = $json;
                unset($verifJSON['password']);

                $this->assertSuccess();
                $this->assertJsonContains($verifJSON);
                $this->assertJsonContains(
                    [
                        self::CONTEXT_INDEX_JSON_LD => self::FRANCHISE_EMPLOYEE_CONTEXT,
                        self::TYPE_INDEX_JSON_LD    => self::FRANCHISE_EMPLOYEE_TYPE,
                        'active'                    => $this->logisticianExample->isActive(),
                        'deleted'                   => $this->logisticianExample->isDeleted(),
                    ]
                );

                $franchiseEmployee = $response->toArray();

                $this->assertIdentifierOfResponseData(self::FRANCHISE_EMPLOYEE_ROUTE, Tools::UUID_REGEX, $franchiseEmployee);
                $this->assertMatchesResourceItemJsonSchema(FranchiseEmployee::class);

            },
            method: self::PATCH_METHOD,
            url: $this->logisticianExampleItemIRI,
            json: $json
        );
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function testPatchInvalidItem()
    {
        $this->doRequest(
            method: self::PATCH_METHOD,
            url: $this->logisticianExampleItemIRI,
            json: [],
            token: $this->logisticianExampleToken
        );

        $this->assertSuccess();
        $this->assertJsonContains(
            [
                self::CONTEXT_INDEX_JSON_LD => self::FRANCHISE_EMPLOYEE_CONTEXT,
                self::TYPE_INDEX_JSON_LD    => self::FRANCHISE_EMPLOYEE_TYPE,
                "lastname"                  => $this->logisticianExample->getLastname(),
                "firstname"                 => $this->logisticianExample->getFirstname(),
                "email"                     => $this->logisticianExample->getEmail(),
                'active'                    => $this->logisticianExample->isActive(),
                'deleted'                   => $this->logisticianExample->isDeleted(),
            ]
        );

        // Max carac lastname, password, firstname, birthdate
        // wrong format email
        $requestNotRespectMinValidation = $this->doRequest(
            method: self::PATCH_METHOD,
            url: $this->logisticianExampleItemIRI,
            json: [
                "password"  => 'aa&&11',
                "lastname"  => 'a',
                "firstname" => 'a',
                "email"     => 'mail non valide',
            ],
            token: $this->logisticianExampleToken
        );

        $this->assertViolations(self::ALL_FIELDS_VIOLATION, $requestNotRespectMinValidation);

        // Max carac lastname, firstname
        $requestNotRespectMaxValidation = $this->doRequest(
            method: self::PATCH_METHOD,
            url: $this->logisticianExampleItemIRI,
            json: [
                "lastname"  => self::STRING_LENGTH_256,
                "firstname" => self::STRING_LENGTH_256,
            ],
            token: $this->logisticianExampleToken
        );

        $this->assertViolations(['lastname', 'firstname'], $requestNotRespectMaxValidation);

    }

    public function testDeleteItem()
    {
        $this->inexistantEndpointTest();
    }

    public function testGetCollectionWithFilter()
    {
        // TODO: Implement testGetCollectionWithFilter() method.
    }

    /**
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testActiveFranchiseEmployee()
    {
        $franchiseEmployeeActiveRoute = self::replaceParamsRoute(self::FRANCHISE_EMPLOYEE_ACTIVE_ACCOUNT_ROUTE, ['uuid' => $this->logisticianExample->getUuid()]);

        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                self::CEDRIC_ADMIN,
                self::ARTHUR_ADMIN,
                self::CORENTIN_ADMIN,
                self::ROLE_ADMIN,
                self::ROLE_HR,
                self::ROLE_LEAD,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $this->assertSuccess();

                //        Vérification du contenu du retour
                $this->assertJsonContains(
                    [
                        self::CONTEXT_INDEX_JSON_LD => self::FRANCHISE_EMPLOYEE_CONTEXT,
                        self::TYPE_INDEX_JSON_LD    => self::FRANCHISE_EMPLOYEE_TYPE,
                        "lastname"                  => $this->logisticianExample->getLastname(),
                        "firstname"                 => $this->logisticianExample->getFirstname(),
                        "email"                     => $this->logisticianExample->getEmail(),
                        "roles"                     => $this->logisticianExample->getRoles(),
                        'active'                    => false,
                        'deleted'                   => $this->logisticianExample->isDeleted(),
                    ]
                );

                $franchiseEmployee = $response->toArray();

                $this->assertIdentifierOfResponseData(self::FRANCHISE_EMPLOYEE_ROUTE, Tools::UUID_REGEX, $franchiseEmployee);
                $this->assertMatchesResourceItemJsonSchema(FranchiseEmployee::class);

                // Impossibilité de créer un token
                $this->getTokenForUser($this->logisticianExample->getUserIdentifier(), $this->logisticianExamplePassword, true);
                $this->assertNotLogin();

                // Token inutilisable
                $this->doRequest(
                    method: self::GET_METHOD,
                    url: $this->logisticianExampleItemIRI,
                    token: $this->logisticianExampleToken
                );

                $this->assertNotLogin();

                // On réactive le compte
                $this->doRequest(
                    method: self::PATCH_METHOD,
                    url: $franchiseEmployeeActiveRoute,
                    json: [
                        'active' => true,
                    ],
                    token: $token
                );

                //        Vérification du contenu du retour
                $this->assertJsonContains(
                    [
                        self::CONTEXT_INDEX_JSON_LD => self::FRANCHISE_EMPLOYEE_CONTEXT,
                        self::TYPE_INDEX_JSON_LD    => self::FRANCHISE_EMPLOYEE_TYPE,
                        "lastname"                  => $this->logisticianExample->getLastname(),
                        "firstname"                 => $this->logisticianExample->getFirstname(),
                        "email"                     => $this->logisticianExample->getEmail(),
                        "roles"                     => $this->logisticianExample->getRoles(),
                        'active'                    => true,
                        'deleted'                   => $this->logisticianExample->isDeleted(),
                    ]
                );

                // Possibilité de créer des tokens d'authentification
                $this->getTokenForUser($this->logisticianExample->getUserIdentifier(), $this->logisticianExamplePassword);

                // Le token est de nouveau utilisable
                $this->doRequest(
                    method: self::GET_METHOD,
                    url: $this->logisticianExampleItemIRI,
                    token: $this->logisticianExampleToken
                );
                $this->assertSuccess();

                $this->initClient();

            },
            method: self::PATCH_METHOD,
            url: $franchiseEmployeeActiveRoute,
            json: [
                'active' => false,
            ],
            dataToSendToFunctionAssertAhtorizeUser: compact('franchiseEmployeeActiveRoute')
        );
    }

    /**
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ExceptionInterface
     */
    public function testEmailAlreadyUsed()
    {
        $testUser = $this->getValidTestFranchiseEmployeeInJSON($this->restaurantExample);

        // Création du compte
        $this->doRequest(
            method: self::POST_METHOD,
            url: self::FRANCHISE_EMPLOYEE_ROUTE,
            json: $testUser,
            token: $this->superAdminExampleToken
        );

        $this->assertSuccess();

        // Tentative de création du compte avec le même mail
        $responseDuplicateAccountEmail = $this->doRequest(
            method: self::POST_METHOD,
            url: self::FRANCHISE_EMPLOYEE_ROUTE,
            json: $testUser,
            token: $this->superAdminExampleToken
        );

        $violations = ['email'];

        $this->assertViolations($violations, $responseDuplicateAccountEmail);
    }

    /**
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testEmailAlreadyUsedOnUpdate()
    {
        $otherAdminIndex = self::ROLE_LEAD;
        $userData = self::getUserFixturesClean()[$otherAdminIndex];

        $json = [
            "email" => $userData->getEmail(),
        ];

        // Tentative de création du compte avec le même mail
        $responseDuplicateAccountEmail = $this->doRequest(
            method: self::PATCH_METHOD,
            url: $this->logisticianExampleItemIRI,
            json: $json,
            token: $this->logisticianExampleToken
        );

        $violations = ['email'];

        $this->assertViolations($violations, $responseDuplicateAccountEmail);
    }

}
