<?php
// tests/AuthenticationTest.php

namespace App\Tests\unit;

use App\Classes\CustomApiTestCase;
use Exception;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class AuthenticationTest extends CustomApiTestCase
{
	private ?string $username;
	private ?string $password;

	/**
	 * @throws Exception
     */
	public function setUp(): void
	{
        parent::setUp();

		$userExampleIndex = self::CEDRIC_ADMIN;
		$user = self::getUserFixturesClean()[$userExampleIndex];
		$this->username = $user->getUserIdentifier();
		$this->password = self::PASSWORDS[$userExampleIndex];
	}

    /**
	 * @throws TransportExceptionInterface
	 * @throws ServerExceptionInterface
	 * @throws RedirectionExceptionInterface
	 * @throws DecodingExceptionInterface
	 * @throws ClientExceptionInterface
	 */
	public function testLogin(): void
	{
		$token = $this->getTokenForUser($this->username, $this->password);

		// test not authorized
        $this->doRequest(
            method: self::GET_METHOD,
            url: CustomerTest::CUSTOMER_ROUTE
        );
		$this->assertNotLogin();

		// test authorized
        $this->doRequest(
            method: self::GET_METHOD,
            url: CustomerTest::CUSTOMER_ROUTE,
            token: $token
        );
		$this->assertSuccess();
	}

	/**
	 * @throws TransportExceptionInterface
	 * @throws ServerExceptionInterface
	 * @throws RedirectionExceptionInterface
	 * @throws DecodingExceptionInterface
	 * @throws ClientExceptionInterface
	 */
	public function testWrongCredentials(): void
	{
		$response = $this->getTokenForUser($this->username, $this->password . "a", true);

		$json = $response->toArray(false);

		$this->assertNotLogin();
		$this->assertArrayHasKey('code', $json);
		$this->assertArrayHasKey('message', $json);
		$this->assertEquals(401, $json['code'] ?? '');

	}
}