<?php
// tests/AuthenticationTest.php

namespace App\Tests\unit;

use App\Classes\CrudApiTestCase;
use App\Entity\Country\Currency;
use App\Entity\User\User;
use App\Service\Tools\Tools;
use App\Traits\Tests\AdminExampleTrait;
use App\Traits\Tests\CurrencyExampleTrait;
use Exception;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class CurrencyTest extends CrudApiTestCase
{

    use AdminExampleTrait;
    use CurrencyExampleTrait;

    // Fixtures
    const CURRENCY_FIXTURES_FILES = 'currency.yml';

    // Routes
    const CURRENCY_ROUTE = self::API_ROUTE . '/currencies';
    const CURRENCY_ITEM_ROUTE = self::CURRENCY_ROUTE . '/{isoCode}';
    const CURRENCY_ACTIVE_ROUTE = self::CURRENCY_ITEM_ROUTE . '/active';

    // JSON LD Data
    const CURRENCY_TYPE = 'Currency';
    const CURRENCY_CONTEXT = self::CONTEXT . '/' . self::CURRENCY_TYPE;

    // Data
    const ALL_FIELDS_VIOLATION = ['name', 'isoCode', "rateForOneEuro", "symbol"];
    const CURRENCY_EXAMPLE = "EUR";

    /**
     * @return Currency[]
     */
    #[Pure] public static function getCurrencyFixtures(): array
    {
        return self::getDataFixturesOfClass(Currency::class);
    }

    /**
     * @throws ExceptionInterface
     */
    public static function getJSONFromCurrency(Currency $currency): array
    {
        $data = self::parseEntityToArray($currency);
        if (!is_null($currency->getRateForOneEuro())) {
            $data['rateForOneEuro'] = (string)$currency->getRateForOneEuro();
        }

        return $data;
    }

    /**
     * @throws Exception
     */
    public static function getValidTestCurrency(): Currency
    {
        $currency = new Currency();
        $currency->setName('Test currency');
        $currency->setRateForOneEuro(2.3);
        $currency->setIsoCode("TEST");
        $currency->setSymbol('$€£');

        return $currency;
    }

    /**
     * @throws Exception
     * @throws ExceptionInterface
     */
    public static function getValidTestCurrencyInJSON(): array
    {
        $currency = self::getValidTestCurrency();

        return self::getJSONFromCurrency($currency);
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws Exception
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAdmin();
        $this->loadCurrency();
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws Exception|ExceptionInterface
     */
    public function testPostItem()
    {
        $data = $this->getValidTestCurrencyInJSON();

        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                self::CEDRIC_ADMIN,
                self::ARTHUR_ADMIN,
                self::CORENTIN_ADMIN,
                self::ROLE_ADMIN,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $currencyCreated = $response->toArray();

                $this->assertSuccess();
                $this->assertJsonContains(
                    [
                        self::CONTEXT_INDEX_JSON_LD => self::CURRENCY_CONTEXT,
                        self::TYPE_INDEX_JSON_LD    => self::CURRENCY_TYPE,
                        'deleted'                   => false,
                        'active'                    => true,
                    ]
                );

                $this->assertJsonContains($json);
                $this->assertIdentifierOfResponseData(self::CURRENCY_ROUTE, Tools::ISO_CODE_REGEX, $currencyCreated);
                $this->assertMatchesResourceItemJsonSchema(Currency::class);

                $this->initClient();
            },
            method: self::POST_METHOD,
            url: self::CURRENCY_ROUTE,
            json: $data,
        );

    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function testPostInvalidItem()
    {
        // Empty request error
        $requestEmptyData = $this->doRequest(
            method: self::POST_METHOD,
            url: self::CURRENCY_ROUTE,
            json: [],
            token: $this->adminExampleToken,
        );

        $this->assertViolations(self::ALL_FIELDS_VIOLATION, $requestEmptyData);

        // Min carac error

        $requestNotRespectMinValidation = $this->doRequest(
            method: self::POST_METHOD,
            url: self::CURRENCY_ROUTE,
            json: [
                "name"           => "",
                "rateForOneEuro" => "0",
                "isoCode"        => "",
                "sumbol"         => "",
            ],
            token: $this->adminExampleToken,
        );

        $this->assertViolations(self::ALL_FIELDS_VIOLATION, $requestNotRespectMinValidation);

        // Max carac error

        $requestNotRespectMaxValidation = $this->doRequest(
            method: self::POST_METHOD,
            url: self::CURRENCY_ROUTE,
            json: [
                "name"           => self::STRING_LENGTH_256,
                "rateForOneEuro" => "0",
                "isoCode"        => self::STRING_LENGTH_256,
                "sumbol"         => self::STRING_LENGTH_256,
            ],
            token: $this->adminExampleToken,
        );

        $this->assertViolations(self::ALL_FIELDS_VIOLATION, $requestNotRespectMaxValidation);

        // Envoie d'un chiffre au lieu d'une chaîne de caractère pour le champ "rateForOneEuro"
        $this->doRequest(
            method: self::POST_METHOD,
            url: self::CURRENCY_ROUTE,
            json: [
                "rateForOneEuro" => 0,
            ],
            token: $this->adminExampleToken,
        );

        $this->assertBadRequest();
    }

    /**
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function testGetCollection(): void
    {

        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                self::ALL_USERS,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $this->assertCollection(
                    response: $response,
                    endpointCollectionRoute: self::CURRENCY_ROUTE,
                    context: self::CURRENCY_CONTEXT,
                    classToCheckValidity: Currency::class,
                    totalItems: count(self::getCurrencyFixtures())
                );
            },
            method: self::GET_METHOD,
            url: self::CURRENCY_ROUTE,
        );
    }

    // TODO : Test currency with same iso code

    /**
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function testGetItem(): void
    {

        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                self::ALL_USERS,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $this->assertSuccess();

                //        Vérification du contenu du retour
                $this->assertJsonContains(
                    [
                        self::CONTEXT_INDEX_JSON_LD => self::CURRENCY_CONTEXT,
                        self::TYPE_INDEX_JSON_LD    => self::CURRENCY_TYPE,
                        'active'                    => $this->currencyExample->isActive(),
                        'deleted'                   => $this->currencyExample->isDeleted(),
                    ]
                );

                $this->assertJsonContains(self::getJSONFromCurrency($this->currencyExample));

                $currency = $response->toArray();

                $this->assertIdentifierOfResponseData(self::CURRENCY_ROUTE, Tools::ISO_CODE_REGEX, $currency);
                $this->assertMatchesResourceItemJsonSchema(Currency::class);

            },
            method: self::GET_METHOD,
            url: $this->currencyExampleItemIRI,
        );
    }

    /**
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws Exception
     */
    public function testDeleteItem(): void
    {
        $superAdminToken = $this->getTokenFromUserIndex(self::CEDRIC_ADMIN);
        $nbCustomersInCollectionBefore = $this->getTotalItemsForRequest(self::CURRENCY_ROUTE, $superAdminToken);

        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                self::CEDRIC_ADMIN,
                self::ARTHUR_ADMIN,
                self::CORENTIN_ADMIN,
                self::ROLE_ADMIN,
                $this->adminExampleIndex,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $this->assertSuccessDelete();

                $this->assertNull(self::getEntityManager()->getRepository(Currency::class)->findOneBy(['isoCode' => $this->currencyExample->getIsoCode()]));

                $this->doRequest(
                    method: self::GET_METHOD,
                    url: $this->currencyExampleItemIRI,
                    token: $superAdminToken,
                );

                $this->assertNotFound();

                $nbCustomersInCollectionAfter = $this->getTotalItemsForRequest(self::CURRENCY_ROUTE, $superAdminToken);

                $this->assertEquals($nbCustomersInCollectionBefore - 1, $nbCustomersInCollectionAfter);

                $this->initClient();

            },
            method: self::DELETE_METHOD,
            url: $this->currencyExampleItemIRI,
            dataToSendToFunctionAssertAhtorizeUser: compact('nbCustomersInCollectionBefore', 'superAdminToken')
        );
    }

    /**
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ExceptionInterface
     */
    public function testPatchItem(): void
    {
        $currency = new Currency();
        $currency->setIsoCode($this->currencyExample->getIsoCode());
        $currency->setName("a" . $this->currencyExample->getName());
        $currency->setRateForOneEuro(1 + $this->currencyExample->getRateForOneEuro());
        $currency->setSymbol("a" . $this->currencyExample->getSymbol());
        $currency->setActive(!$this->currencyExample->isActive());

        $json = self::getJSONFromCurrency($currency);

        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                self::CEDRIC_ADMIN,
                self::ARTHUR_ADMIN,
                self::CORENTIN_ADMIN,
                self::ROLE_ADMIN,
                $this->adminExampleIndex,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $this->assertSuccess();
                //        Vérification du contenu du retour
                $this->assertJsonContains(
                    [
                        self::CONTEXT_INDEX_JSON_LD => self::CURRENCY_CONTEXT,
                        self::TYPE_INDEX_JSON_LD    => self::CURRENCY_TYPE,
                        'deleted'                   => false,
                    ]
                );
                $this->assertJsonContains($json);

                $currency = $response->toArray();

                $this->assertIdentifierOfResponseData(self::CURRENCY_ROUTE, Tools::ISO_CODE_REGEX, $currency);
                $this->assertMatchesResourceItemJsonSchema(Currency::class);

                $this->initClient();
            },
            method: self::PATCH_METHOD,
            url: $this->currencyExampleItemIRI,
            json: $json,
        );
    }

    public function testPatchInvalidItem()
    {
        // TODO: Implement testPatchInvalidItem() method.
    }

    public function testGetCollectionWithFilter()
    {
        // TODO: Implement testGetCollectionWithFilter() method.
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws Exception|ExceptionInterface
     */
    public function testCreateCurrencySameIsoCode()
    {
        $data = $this->getValidTestCurrencyInJSON();

        $this->doRequest(
            method: self::POST_METHOD,
            url: self::CURRENCY_ROUTE,
            json: $data,
            token: $this->adminExampleToken
        );

        $this->assertSuccess();

        $requestResult = $this->doRequest(
            method: self::POST_METHOD,
            url: self::CURRENCY_ROUTE,
            json: $data,
            token: $this->adminExampleToken
        );

        $this->assertViolations(['isoCode'], $requestResult);

    }

}
