<?php
// tests/AuthenticationTest.php

namespace App\Tests\unit;

use App\Classes\CustomApiTestCase;
use App\Controller\User\Request\UserRequestCreationAction;
use App\Entity\User\User;
use App\Entity\User\UserRequest\AccountConfirmationRequest;
use App\Repository\AccountConfirmationRequestRepository;
use App\Traits\Tests\CustomerConfirmedExampleTrait;
use App\Traits\Tests\CustomerNotConfirmedExampleTrait;
use Exception;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class AccountConfirmationRequestTest extends CustomApiTestCase
{

    use CustomerConfirmedExampleTrait;
    use CustomerNotConfirmedExampleTrait;

    // Routes
    const ACCOUNT_CONFIRM_ROUTE = self::API_ROUTE . '/account_confirmation_requests';
    const ACCOUNT_CONFIRM_VALIDATE_ROUTE = self::ACCOUNT_CONFIRM_ROUTE . '/{id}/validate';

    // JSON LD Data
    const ACCOUNT_CONFIRMATION_REQUEST_TYPE = 'AccountConfirmationRequest';
    const ACCOUNT_CONFIRMATION_REQUEST_CONTEXT = self::CONTEXT . '/' . self::ACCOUNT_CONFIRMATION_REQUEST_TYPE;

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadCustomerConfirmed();
        $this->loadCustomerNotConfirmed();
    }

    /**
     * Tentative de demande de confirmation de compte avec le mauvais token d'auth
     * (se connecter en admin pour confirmé le compte d'un client)
     *
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws Exception
     */
    public function testCreateNewAccountConfirmationRequest()
    {
        $this->removeAllRequestForUserWithUuid($this->customerNotConfirmedExample->getUuid());
        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                $this->customerNotConfirmedExampleIndex,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $this->assertSuccess();

                $this->assertJsonContains(
                    [
                        self::CONTEXT_INDEX_JSON_LD => self::ACCOUNT_CONFIRMATION_REQUEST_CONTEXT,
                        self::TYPE_INDEX_JSON_LD    => self::ACCOUNT_CONFIRMATION_REQUEST_TYPE,
                        'alreadyUsed'               => false,
                        'customerTarget'            => $this->customerNotConfirmedExampleItemIRI,
                    ]
                );

                $this->assertMatchesResourceItemJsonSchema(AccountConfirmationRequest::class);

            },
            method: self::POST_METHOD,
            url: AccountConfirmationRequestTest::ACCOUNT_CONFIRM_ROUTE,
            json: [
                'customerTarget' => $this->customerNotConfirmedExampleItemIRI,
            ]
        );
    }

    /**
     * @throws Exception
     */
    public function removeAllRequestForUserWithUuid(string $uuid)
    {
        self::removeAllRequestForUserWithUuidAndEntityManager($uuid);
    }

    /**
     * @throws Exception
     */
    public static function removeAllRequestForUserWithUuidAndEntityManager(string $uuid)
    {
        /**
         * @var AccountConfirmationRequestRepository $accountConfirmationRepo
         */
        $accountConfirmationRepo = self::getEntityManager()->getRepository(AccountConfirmationRequest::class);
        $accountConfirmationRepo->removeAllRequestsForUserIdentifier($uuid);
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface|DecodingExceptionInterface
     * @throws Exception
     */
    public function testCreateNewAccountConfirmationRequestUserAlreadyConfirmed()
    {
        $this->removeAllRequestForUserWithUuid($this->customerConfirmedExample->getUuid());

        $this->doRequest(
            method: self::POST_METHOD,
            url: AccountConfirmationRequestTest::ACCOUNT_CONFIRM_ROUTE,
            json: [
                'customerTarget' => $this->customerConfirmedExampleItemIRI,
            ],
            token: $this->customerConfirmedExampleToken
        );

        $this->assertBadRequest();
    }

    /**
     * Test d'une erreur si le temps entre deux requêtes n'est pas valide (il faut attendre deux minutes)
     *
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws Exception
     */
    public function testCreateNewAccountConfirmationRequestWhenDelayBetweenTwoRequestIsNotRespected()
    {

        $this->removeAllRequestForUserWithUuid($this->customerNotConfirmedExample->getUuid());

        // Première requête OK
        $this->doRequest(
            method: self::POST_METHOD,
            url: AccountConfirmationRequestTest::ACCOUNT_CONFIRM_ROUTE,
            json: [
                'customerTarget' => $this->customerNotConfirmedExampleItemIRI,
            ],
            token: $this->customerNotConfirmedExampleToken
        );

        $this->assertSuccess();

        // Deuxième rqt Error
        $response = $this->doRequest(
            method: self::POST_METHOD,
            url: AccountConfirmationRequestTest::ACCOUNT_CONFIRM_ROUTE,
            json: [
                'customerTarget' => $this->customerNotConfirmedExampleItemIRI,
            ],
            token: $this->customerNotConfirmedExampleToken
        );

        $this->assertBadRequest(
            $response,
            UserRequestCreationAction::getDelayMessageError(AccountConfirmationRequest::DELAY_IN_MINUTE_BETWEEN_TWO_REQUEST)
        );

    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws Exception
     */
    public function testCreateNewAccountConfirmationRequestWrongData()
    {
        $this->removeAllRequestForUserWithUuid($this->customerNotConfirmedExample->getUuid());

        $this->doRequest(
            method: self::POST_METHOD,
            url: AccountConfirmationRequestTest::ACCOUNT_CONFIRM_ROUTE,
            json: [],
            token: $this->customerNotConfirmedExampleToken
        );

        $this->assertAccessDenied();

        $this->doRequest(
            method: self::POST_METHOD,
            url: AccountConfirmationRequestTest::ACCOUNT_CONFIRM_ROUTE,
            json: [
                'customerTarget' => '1ec6593a-8b60-608c-869c-89ede9f2c00a',
            ],
            token: $this->customerNotConfirmedExampleToken
        );

        $this->assertBadRequest();

    }

    /**
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws TransportExceptionInterface
     * @throws Exception
     */
    public function testValidateAccountConfirmationRequestAlreadyValidated()
    {
        $this->removeAllRequestForUserWithUuid($this->customerNotConfirmedExample->getUuid());

        $response = $this->doRequest(
            method: self::POST_METHOD,
            url: AccountConfirmationRequestTest::ACCOUNT_CONFIRM_ROUTE,
            json: [
                'customerTarget' => $this->customerNotConfirmedExampleItemIRI,
            ],
            token: $this->customerNotConfirmedExampleToken
        );

        $this->assertSuccess();

        $idRequest = $response->toArray()['id'];

        // Validation du compte

        $this->doRequest(
            method: self::PATCH_METHOD,
            url: $this->replaceParamsRoute(AccountConfirmationRequestTest::ACCOUNT_CONFIRM_VALIDATE_ROUTE, ['id' => $idRequest]),
            json: []
        );

        $this->assertSuccess();

        // Réutilisation de la demande => erreur (déjà utilisée)

        $this->doRequest(
            method: self::PATCH_METHOD,
            url: $this->replaceParamsRoute(AccountConfirmationRequestTest::ACCOUNT_CONFIRM_VALIDATE_ROUTE, ['id' => $idRequest]),
            json: []
        );

        $this->assertBadRequest();

    }

}
