<?php
// tests/AuthenticationTest.php

namespace App\Tests\unit;

use App\Classes\CrudApiTestCase;
use App\Entity\User\Employee\Employee;
use App\Entity\User\Employee\FranchisorEmployee;
use App\Entity\User\User;
use App\Service\Tools\Tools;
use App\Traits\Tests\EditorExampleTrait;
use App\Traits\Tests\SuperAdminExampleTrait;
use Exception;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Uid\Uuid;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class FranchisorEmployeeTest extends CrudApiTestCase
{

    use EditorExampleTrait;
    use SuperAdminExampleTrait;

    // Fixtures
    const FRANCHISOR_EMPLOYEE_FIXTURES_FILE = 'franchisor_employee.yml';

    // Routes
    const FRANCHISOR_EMPLOYEE_ROUTE = self::API_ROUTE . '/franchisor_employees';
    const FRANCHISOR_EMPLOYEE_ITEM_ROUTE = self::FRANCHISOR_EMPLOYEE_ROUTE . '/{uuid}';
    const FRANCHISOR_EMPLOYEE_ACTIVE_ACCOUNT_ROUTE = self::FRANCHISOR_EMPLOYEE_ITEM_ROUTE . '/active';

    // JSON LD Data
    const FRANCHISOR_EMPLOYEE_TYPE = 'FranchisorEmployee';
    const FRANCHISOR_EMPLOYEE_CONTEXT = self::CONTEXT . '/' . self::FRANCHISOR_EMPLOYEE_TYPE;

    // Data
    const ALL_FIELDS_VIOLATION = ['email', 'lastname', 'firstname', 'password'];

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws Exception
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadEditor();
        $this->loadSuperAdmin();
    }

    /**
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ExceptionInterface
     */
    public function testPostItem()
    {
        $testUser = $this->getValidTestFranchisorEmployeeInJSON();

        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                self::CEDRIC_ADMIN,
                self::ARTHUR_ADMIN,
                self::CORENTIN_ADMIN,
                self::ROLE_ADMIN,
                self::ROLE_HR,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $lastname = $json['lastname'];
                $firstname = $json['firstname'];
                $email = $json['email'];
                $password = $json['password'];

                $this->assertSuccess();
                $this->assertJsonContains(
                    [
                        self::CONTEXT_INDEX_JSON_LD => self::FRANCHISOR_EMPLOYEE_CONTEXT,
                        self::TYPE_INDEX_JSON_LD    => self::FRANCHISOR_EMPLOYEE_TYPE,
                        "lastname"                  => $lastname,
                        "firstname"                 => $firstname,
                        "email"                     => $email,
                        "roles"                     => [Employee::DEFAULT_ROLE_EMPLOYEE, FranchisorEmployee::DEFAULT_ROLE_FRANCHISOR],
                        'active'                    => true,
                        'deleted'                   => false,
                    ]
                );

                $employee = $response->toArray();

                $this->assertIdentifierOfResponseData(self::FRANCHISOR_EMPLOYEE_ROUTE, Tools::UUID_REGEX, $employee);
                $this->assertMatchesResourceItemJsonSchema(FranchisorEmployee::class);

                $token = $this->getTokenForUser($email, $password);
                $this->assertNotNull($token);

                $this->initClient();
            },
            method: self::POST_METHOD,
            url: self::FRANCHISOR_EMPLOYEE_ROUTE,
            json: $testUser,
        );

    }

    /**
     * @throws Exception
     * @throws ExceptionInterface
     */
    public static function getValidTestFranchisorEmployeeInJSON(): array
    {
        $franchisorEmployee = self::getValidTestFranchisorEmployee();

        return self::getJSONFromFranchisorEmployee($franchisorEmployee);
    }

    /**
     * @throws Exception
     */
    public static function getValidTestFranchisorEmployee(): FranchisorEmployee
    {
        $franchisorEmployee = new FranchisorEmployee();
        $franchisorEmployee->setUuid(Uuid::v4());
        $franchisorEmployee->setPassword("aaAA&&12");
        $franchisorEmployee->setLastname("FRANCHISOR_EMLPOYEE");
        $franchisorEmployee->setFirstname("Good Food");
        $franchisorEmployee->setEmail(self::getTestEmail());

        return $franchisorEmployee;
    }

    /**
     * @throws ExceptionInterface
     */
    public static function getJSONFromFranchisorEmployee(FranchisorEmployee $franchisorEmployee): array
    {
        $data = self::parseEntityToArray($franchisorEmployee);
        $data['password'] = $franchisorEmployee->getPassword() ?? $franchisorEmployee->getPlainPassword();
        unset($data['uuid']);

        return $data;
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function testPostInvalidItem()
    {
        $requestEmptyData = $this->doRequest(
            method: self::POST_METHOD,
            url: self::FRANCHISOR_EMPLOYEE_ROUTE,
            json: [],
            token: $this->superAdminExampleToken,
        );

        $this->assertViolations(self::ALL_FIELDS_VIOLATION, $requestEmptyData);

        // Max carac lastname, password, firstname, birthdate
        // wrong format email
        $requestNotRespectMinValidation = $this->doRequest(
            method: self::POST_METHOD,
            url: self::FRANCHISOR_EMPLOYEE_ROUTE,
            json: [
                "password"  => 'aa&&11',
                "lastname"  => 'a',
                "firstname" => 'a',
                "email"     => 'mail non valide',
            ],
            token: $this->superAdminExampleToken,
        );

        $this->assertViolations(self::ALL_FIELDS_VIOLATION, $requestNotRespectMinValidation);

        // Max carac lastname, firstname
        $requestNotRespectMaxValidation = $this->doRequest(
            method: self::POST_METHOD,
            url: self::FRANCHISOR_EMPLOYEE_ROUTE,
            json: [
                "lastname"  => self::STRING_LENGTH_256,
                "firstname" => self::STRING_LENGTH_256,
            ],
            token: $this->superAdminExampleToken,
        );

        $this->assertViolations(self::ALL_FIELDS_VIOLATION, $requestNotRespectMaxValidation);

    }

    /**
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function testGetCollection(): void
    {
        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                self::CEDRIC_ADMIN,
                self::ARTHUR_ADMIN,
                self::CORENTIN_ADMIN,
                self::ROLE_ADMIN,
                self::ROLE_HR,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $this->assertCollection(
                    response: $response,
                    endpointCollectionRoute: self::FRANCHISOR_EMPLOYEE_ROUTE,
                    context: self::FRANCHISOR_EMPLOYEE_CONTEXT,
                    classToCheckValidity: FranchisorEmployee::class,
                    totalItems: count(self::getFranchisorEmployeeFixtures())
                );
            },
            method: self::GET_METHOD,
            url: self::FRANCHISOR_EMPLOYEE_ROUTE
        );

    }

    /**
     * @return FranchisorEmployee[]
     */
    #[Pure] public static function getFranchisorEmployeeFixtures(): array
    {
        return self::getDataFixturesOfClass(FranchisorEmployee::class);
    }

    /**
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function testGetItem(): void
    {
        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                self::CEDRIC_ADMIN,
                self::ARTHUR_ADMIN,
                self::CORENTIN_ADMIN,
                self::ROLE_ADMIN,
                self::ROLE_HR,
                $this->editorExampleIndex,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $this->assertSuccess();
                $this->assertJsonContains(
                    [
                        self::CONTEXT_INDEX_JSON_LD => self::FRANCHISOR_EMPLOYEE_CONTEXT,
                        self::TYPE_INDEX_JSON_LD    => self::FRANCHISOR_EMPLOYEE_TYPE,
                        "lastname"                  => $this->editorExample->getLastname(),
                        "firstname"                 => $this->editorExample->getFirstname(),
                        "email"                     => $this->editorExample->getEmail(),
                        "roles"                     => $this->editorExample->getRoles(),
                        'active'                    => $this->editorExample->isActive(),
                        'deleted'                   => $this->editorExample->isDeleted(),
                    ]
                );

                $franchisorEmployee = $response->toArray();

                $this->assertIdentifierOfResponseData(self::FRANCHISOR_EMPLOYEE_ROUTE, Tools::UUID_REGEX, $franchisorEmployee);
                $this->assertMatchesResourceItemJsonSchema(FranchisorEmployee::class);

            },
            method: self::GET_METHOD,
            url: $this->editorExampleItemIRI
        );
    }

    /**
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testPatchItem(): void
    {
        $json = [
            "password"  => self::PASSWORDS[$this->editorExampleIndex],
            "lastname"  => "1" . $this->editorExample->getLastname(),
            "firstname" => "1" . $this->editorExample->getFirstname(),
            "email"     => "1" . $this->editorExample->getEmail(),
        ];

        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                $this->editorExampleIndex,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $verifJSON = $json;
                unset($verifJSON['password']);

                $this->assertSuccess();
                $this->assertJsonContains($verifJSON);
                $this->assertJsonContains(
                    [
                        self::CONTEXT_INDEX_JSON_LD => self::FRANCHISOR_EMPLOYEE_CONTEXT,
                        self::TYPE_INDEX_JSON_LD    => self::FRANCHISOR_EMPLOYEE_TYPE,
                        'active'                    => $this->editorExample->isActive(),
                        'deleted'                   => $this->editorExample->isDeleted(),
                    ]
                );

                $franchisorEmployee = $response->toArray();

                $this->assertIdentifierOfResponseData(self::FRANCHISOR_EMPLOYEE_ROUTE, Tools::UUID_REGEX, $franchisorEmployee);
                $this->assertMatchesResourceItemJsonSchema(FranchisorEmployee::class);

            },
            method: self::PATCH_METHOD,
            url: $this->editorExampleItemIRI,
            json: $json
        );
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function testPatchInvalidItem()
    {
        $this->doRequest(
            method: self::PATCH_METHOD,
            url: $this->editorExampleItemIRI,
            json: [],
            token: $this->editorExampleToken,
        );

        $this->assertSuccess();
        $this->assertJsonContains(
            [
                self::CONTEXT_INDEX_JSON_LD => self::FRANCHISOR_EMPLOYEE_CONTEXT,
                self::TYPE_INDEX_JSON_LD    => self::FRANCHISOR_EMPLOYEE_TYPE,
                "lastname"                  => $this->editorExample->getLastname(),
                "firstname"                 => $this->editorExample->getFirstname(),
                "email"                     => $this->editorExample->getEmail(),
                'active'                    => $this->editorExample->isActive(),
                'deleted'                   => $this->editorExample->isDeleted(),
            ]
        );

        // Max carac lastname, password, firstname, birthdate
        // wrong format email
        $requestNotRespectMinValidation = $this->doRequest(
            method: self::PATCH_METHOD,
            url: $this->editorExampleItemIRI,
            json: [
                "password"  => 'aa&&11',
                "lastname"  => 'a',
                "firstname" => 'a',
                "email"     => 'mail non valide',
            ],
            token: $this->editorExampleToken,
        );

        $this->assertViolations(self::ALL_FIELDS_VIOLATION, $requestNotRespectMinValidation);

//        Max carac lastname, firstname
        $requestNotRespectMaxValidation = $this->doRequest(
            method: self::PATCH_METHOD,
            url: $this->editorExampleItemIRI,
            json: [
                "lastname"  => self::STRING_LENGTH_256,
                "firstname" => self::STRING_LENGTH_256,
            ],
            token: $this->editorExampleToken,
        );

        $this->assertViolations(['lastname', 'firstname'], $requestNotRespectMaxValidation);

    }

    public function testDeleteItem()
    {
        $this->inexistantEndpointTest();
    }

    public function testGetCollectionWithFilter()
    {
        // TODO: Implement testGetCollectionWithFilter() method.
    }

    /**
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testActiveFranchisorEmployee()
    {
        $franchisorEmployeeActiveRoute = self::replaceParamsRoute(self::FRANCHISOR_EMPLOYEE_ACTIVE_ACCOUNT_ROUTE, ['uuid' => $this->editorExample->getUuid()]);

        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                self::CEDRIC_ADMIN,
                self::ARTHUR_ADMIN,
                self::CORENTIN_ADMIN,
                self::ROLE_ADMIN,
                self::ROLE_HR,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $this->assertSuccess();

                //        Vérification du contenu du retour
                $this->assertJsonContains(
                    [
                        self::CONTEXT_INDEX_JSON_LD => self::FRANCHISOR_EMPLOYEE_CONTEXT,
                        self::TYPE_INDEX_JSON_LD    => self::FRANCHISOR_EMPLOYEE_TYPE,
                        "lastname"                  => $this->editorExample->getLastname(),
                        "firstname"                 => $this->editorExample->getFirstname(),
                        "email"                     => $this->editorExample->getEmail(),
                        "roles"                     => $this->editorExample->getRoles(),
                        'active'                    => false,
                        'deleted'                   => $this->editorExample->isDeleted(),
                    ]
                );

                $franchisorEmployee = $response->toArray();

                $this->assertIdentifierOfResponseData(self::FRANCHISOR_EMPLOYEE_ROUTE, Tools::UUID_REGEX, $franchisorEmployee);
                $this->assertMatchesResourceItemJsonSchema(FranchisorEmployee::class);

                // Impossibilité de créer un token
                $this->getTokenForUser($this->editorExample->getUserIdentifier(), $this->editorExamplePassword, true);
                $this->assertNotLogin();

                // Token inutilisable
                $this->doRequest(
                    method: self::GET_METHOD,
                    url: $this->editorExampleItemIRI,
                    token: $this->editorExampleToken,
                );

                $this->assertNotLogin();

                // On réactive le compte
                $this->doRequest(
                    method: self::PATCH_METHOD,
                    url: $franchisorEmployeeActiveRoute,
                    json: [
                        'active' => true,
                    ],
                    token: $token,
                );

                //        Vérification du contenu du retour
                $this->assertJsonContains(
                    [
                        self::CONTEXT_INDEX_JSON_LD => self::FRANCHISOR_EMPLOYEE_CONTEXT,
                        self::TYPE_INDEX_JSON_LD    => self::FRANCHISOR_EMPLOYEE_TYPE,
                        "lastname"                  => $this->editorExample->getLastname(),
                        "firstname"                 => $this->editorExample->getFirstname(),
                        "email"                     => $this->editorExample->getEmail(),
                        "roles"                     => $this->editorExample->getRoles(),
                        'active'                    => true,
                        'deleted'                   => $this->editorExample->isDeleted(),
                    ]
                );

                // Possibilité de créer des tokens d'authentification
                $this->getTokenForUser($this->editorExample->getUserIdentifier(), $this->editorExamplePassword);

                // Le token est de nouveau utilisable
                $this->doRequest(
                    method: self::GET_METHOD,
                    url: $this->editorExampleItemIRI,
                    token: $this->editorExampleToken,
                );
                $this->assertSuccess();

            },
            method: self::PATCH_METHOD,
            url: $franchisorEmployeeActiveRoute,
            json: [
                'active' => false,
            ],
            dataToSendToFunctionAssertAhtorizeUser: compact('franchisorEmployeeActiveRoute')
        );
    }

    /**
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ExceptionInterface
     */
    public function testEmailAlreadyUsed()
    {
        $testUser = $this->getValidTestFranchisorEmployeeInJSON();

//        Création du compte
        $this->doRequest(
            method: self::POST_METHOD,
            url: self::FRANCHISOR_EMPLOYEE_ROUTE,
            json: $testUser,
            token: $this->superAdminExampleToken,
        );

//        Tentative de création du compte avec le même mail
        $responseDuplicateAccountEmail = $this->doRequest(
            method: self::POST_METHOD,
            url: self::FRANCHISOR_EMPLOYEE_ROUTE,
            json: $testUser,
            token: $this->superAdminExampleToken,
        );

        $violations = ['email'];

        $this->assertViolations($violations, $responseDuplicateAccountEmail);
    }

    /**
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testEmailAlreadyUsedOnUpdate()
    {
        $otherAdminIndex = self::ROLE_HR;
        $userData = self::getUserFixturesClean()[$otherAdminIndex];

        $json = [
            "email" => $userData->getEmail(),
        ];

        // Tentative de création du compte avec le même mail
        $responseDuplicateAccountEmail = $this->doRequest(
            method: self::PATCH_METHOD,
            url: $this->editorExampleItemIRI,
            json: $json,
            token: $this->editorExampleToken,
        );

        $violations = ['email'];

        $this->assertViolations($violations, $responseDuplicateAccountEmail);
    }

}
