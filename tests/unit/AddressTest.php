<?php
// tests/AuthenticationTest.php

namespace App\Tests\unit;

use App\Classes\CrudApiTestCase;
use App\Entity\Country\Country;
use App\Entity\User\Customer\Address;
use App\Entity\User\Customer\Customer;
use App\Entity\User\User;
use App\Service\Tools\Tools;
use App\Traits\Tests\AddressExampleTrait;
use App\Traits\Tests\CountryExampleTrait;
use App\Traits\Tests\CustomerConfirmedExampleTrait;
use Exception;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class AddressTest extends CrudApiTestCase
{

    use CustomerConfirmedExampleTrait;
    use AddressExampleTrait;
    use CountryExampleTrait;

    // Fixtures
    const ADDRESS_FIXTURES_FILES = 'address.yml';

    // Routes
    const ADDRESS_ROUTE = self::API_ROUTE . '/addresses';
    const ADDRESS_ITEM_ROUTE = self::ADDRESS_ROUTE . '/{slug}';
    const ADDRESS_ACTIVE_ROUTE = self::ADDRESS_ITEM_ROUTE . '/active';

    // JSON LD Data
    const ADDRESS_TYPE = 'Address';
    const ADDRESS_CONTEXT = self::CONTEXT . '/' . self::ADDRESS_TYPE;

    // Data
    const ALL_FIELDS_VIOLATION = ['name', 'street', 'zipCode', 'city', 'phoneNumber', 'country'];
    const ADDRESS_EXAMPLE = "address_main_1";

    /**
     * @return Address[]
     */
    #[Pure] public static function getAddressFixtures(): array
    {
        return self::getDataFixturesOfClass(Address::class);
    }

    /**
     * @throws ExceptionInterface
     */
    public static function getJSONFromAddress(Address $address): array
    {
        $data = self::parseEntityToArray($address);
        $data['customer'] = self::replaceParamsRoute(CustomerTest::CUSTOMER_ITEM_ROUTE, ['uuid' => $address->getCustomer()->getUuid()]);
        $data['country'] = self::replaceParamsRoute(CountryTest::COUNTRY_ITEM_ROUTE, ['isoCode' => $address->getCountry()->getIsoCode()]);
        unset($data['slug']);
        unset($data['id']);

        return $data;
    }

    /**
     * @throws Exception
     */
    public static function getValidTestAddress(
        Customer $customer,
        Country  $country
    ): Address
    {
        $address = new Address();
        $address->setName("Address Test");
        $address->setSlug(self::slugify($address->getName()));
        $address->setStreet("2 test street");
        $address->setZipCode("25000");
        $address->setCity("Paris");
        $address->setCountry($country);
        $address->setPhoneNumber("+33123456789");
        $address->setCustomer($customer);

        return $address;
    }

    /**
     * @throws Exception
     * @throws ExceptionInterface
     */
    public static function getValidTestAddressInJSON(
        Customer $customer,
        Country  $country
    ): array
    {
        $address = self::getValidTestAddress($customer, $country);

        return self::getJSONFromAddress($address);
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws Exception
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadCustomerConfirmed();
        $this->loadAddress();
        $this->loadCountry();
    }

    /**
     * @return void
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testDeleteItem()
    {
        $superAdminToken = $this->getTokenFromUserIndex(self::CEDRIC_ADMIN);
        $nbCustomersInCollectionBefore = $this->getTotalItemsForRequest(self::ADDRESS_ROUTE, $superAdminToken);

        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                $this->customerConfirmedExampleIndex,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $this->assertSuccessDelete();

                $this->assertNull(self::getEntityManager()->getRepository(Address::class)->findOneBy(['slug' => $this->addressExample->getSlug()]));

                $this->doRequest(
                    method: self::GET_METHOD,
                    url: $this->addressExampleItemIRI,
                    token: $superAdminToken
                );

                $this->assertNotFound();

                $nbCustomersInCollectionAfter = $this->getTotalItemsForRequest(self::ADDRESS_ROUTE, $superAdminToken);

                $this->assertEquals($nbCustomersInCollectionBefore - 1, $nbCustomersInCollectionAfter);

                $this->initClient();

            },
            method: self::DELETE_METHOD,
            url: $this->addressExampleItemIRI,
            dataToSendToFunctionAssertAhtorizeUser: compact(
                'superAdminToken',
                'nbCustomersInCollectionBefore',
            )
        );
    }

    /**
     * @return void
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testGetCollection()
    {
        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                self::CEDRIC_ADMIN,
                self::ARTHUR_ADMIN,
                self::CORENTIN_ADMIN,
                self::ROLE_ADMIN,
                self::ROLE_LOGISTICIAN,
                self::ROLE_LEAD,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $this->assertCollection(
                    response: $response,
                    endpointCollectionRoute: self::ADDRESS_ROUTE,
                    context: self::ADDRESS_CONTEXT,
                    classToCheckValidity: Address::class,
                    totalItems: count(self::getAddressFixtures())
                );

            },
            method: self::GET_METHOD,
            url: self::ADDRESS_ROUTE,
        );
    }

    /**
     * @return void
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testGetItem()
    {
        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                self::CEDRIC_ADMIN,
                self::ARTHUR_ADMIN,
                self::CORENTIN_ADMIN,
                self::ROLE_ADMIN,
                self::ROLE_LOGISTICIAN,
                self::ROLE_LEAD,
                $this->customerConfirmedExampleIndex,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $this->assertSuccess();

                //        Vérification du contenu du retour
                $this->assertJsonContains(
                    [
                        self::CONTEXT_INDEX_JSON_LD => self::ADDRESS_CONTEXT,
                        self::TYPE_INDEX_JSON_LD    => self::ADDRESS_TYPE,
                        'name'                      => $this->addressExample->getName(),
                        'street'                    => $this->addressExample->getStreet(),
                        'zipCode'                   => $this->addressExample->getZipCode(),
                        'city'                      => $this->addressExample->getCity(),
                        'phoneNumber'               => $this->addressExample->getPhoneNumber(),
                        'customer'                  => $this->customerConfirmedExampleItemIRI,
                        'deleted'                   => $this->addressExample->isDeleted(),
                    ]
                );

                $address = $response->toArray();

                $this->assertIdentifierOfResponseData(self::ADDRESS_ROUTE, Tools::SLUG_REGEX, $address);

                $this->assertMatchesResourceItemJsonSchema(Address::class);

            },
            method: self::GET_METHOD,
            url: $this->addressExampleItemIRI,
        );
    }

    /**
     * @return void
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testPatchItem()
    {
        $json = self::getValidTestAddressInJSON($this->customerConfirmedExample, $this->countryExample);
        unset($json['customer']);

        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                $this->customerConfirmedExampleIndex,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json) {

                $verifJSON = $json;
                $verifJSON['customer'] = $this->customerConfirmedExampleItemIRI;
                $verifJSON['country'] = self::parseEntityToArray($this->countryExample);
                unset($verifJSON['country']['defaultCurrency']);

                $this->assertSuccess();

                //        Vérification du contenu du retour
                $this->assertJsonContains(
                    [
                        self::CONTEXT_INDEX_JSON_LD => self::ADDRESS_CONTEXT,
                        self::TYPE_INDEX_JSON_LD    => self::ADDRESS_TYPE,
                        'deleted'                   => false,
                    ]
                );
                $this->assertJsonContains($verifJSON);

                $address = $response->toArray();

                $this->assertIdentifierOfResponseData(self::ADDRESS_ROUTE, Tools::SLUG_REGEX, $address);
                $this->assertMatchesResourceItemJsonSchema(Address::class);

                $this->initClient();

            },
            method: self::PATCH_METHOD,
            url: $this->addressExampleItemIRI,
            json: $json
        );

    }

    /**
     * @return void
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testPostItem()
    {
        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                $this->customerConfirmedExampleIndex,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json) {

                $verifJSON = $json;
                $verifJSON['customer'] = $this->customerConfirmedExampleItemIRI;
                $verifJSON['country'] = self::parseEntityToArray($this->countryExample);
                unset($verifJSON['country']['defaultCurrency']);

                $addressCreated = $response->toArray();

                $this->assertSuccess();
                $this->assertJsonContains(
                    [
                        self::CONTEXT_INDEX_JSON_LD => self::ADDRESS_CONTEXT,
                        self::TYPE_INDEX_JSON_LD    => self::ADDRESS_TYPE,
                        'deleted'                   => false,
                    ]
                );

                $this->assertJsonContains($verifJSON);
                $this->assertIdentifierOfResponseData(self::ADDRESS_ROUTE, Tools::SLUG_REGEX, $addressCreated);
                $this->assertMatchesResourceItemJsonSchema(Address::class);

                // Vérification que l'adresse remonte bien dans la liste du client
                $routeToCustomerAddresses = $this->replaceParamsRoute(CustomerTest::CUSTOMER_ADDRESSES_ROUTE, ['uuid' => $this->customerConfirmedExample->getUuid()]);

                $addressesRequest = $this->doRequest(
                    method: self::GET_METHOD,
                    url: $routeToCustomerAddresses,
                    token: $this->customerConfirmedExampleToken
                );

                $this->assertSubResourceCollection(
                    response: $addressesRequest,
                    endpointSubResourceCollectionRoute: $routeToCustomerAddresses,
                    context: self::ADDRESS_CONTEXT,
                    classToCheckValidity: Address::class,
                    totalItems: 3,
                    entityCreated: $addressCreated
                );

            },
            method: self::POST_METHOD,
            url: self::ADDRESS_ROUTE,
            json: self::getValidTestAddressInJSON(
                $this->customerConfirmedExample,
                $this->countryExample
            )
        );

    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws ExceptionInterface
     */
    public function testPostInvalidItem()
    {
        $validAddress = self::getValidTestAddressInJSON($this->customerConfirmedExample, $this->countryExample);

        // Empty request error
        $this->doRequest(
            method: self::POST_METHOD,
            url: self::ADDRESS_ROUTE,
            json: [],
            token: $this->customerConfirmedExampleToken
        );

        $this->assertAccessDenied();

        $requestEmptyData = $this->doRequest(
            method: self::POST_METHOD,
            url: self::ADDRESS_ROUTE,
            json: [
                'customer' => $this->customerConfirmedExampleItemIRI,
            ],
            token: $this->customerConfirmedExampleToken
        );

        $this->assertViolations(self::ALL_FIELDS_VIOLATION, $requestEmptyData);

        // Min carac error

        $requestNotRespectMinValidation = $this->doRequest(
            method: self::POST_METHOD,
            url: self::ADDRESS_ROUTE,
            json: [
                'name'        => "",
                'street'      => "",
                'zipCode'     => "",
                'city'        => "",
                'phoneNumber' => "",
                'customer'    => $this->customerConfirmedExampleItemIRI,
            ],
            token: $this->customerConfirmedExampleToken
        );

        $this->assertViolations(self::ALL_FIELDS_VIOLATION, $requestNotRespectMinValidation);

        // Max carac error

        $requestNotRespectMaxValidation = $this->doRequest(
            method: self::POST_METHOD,
            url: self::ADDRESS_ROUTE,
            json: [
                'name'        => self::STRING_LENGTH_256,
                'street'      => self::STRING_LENGTH_256,
                'zipCode'     => self::STRING_LENGTH_256,
                'city'        => self::STRING_LENGTH_256,
                'phoneNumber' => self::STRING_LENGTH_256,
                'customer'    => $this->customerConfirmedExampleItemIRI,
            ],
            token: $this->customerConfirmedExampleToken
        );

        $this->assertViolations(self::ALL_FIELDS_VIOLATION, $requestNotRespectMaxValidation);

        // Numéro de téléphone incorrect

        $data = $validAddress;
        $data['phoneNumber'] = 'azer';

        $this->doRequest(
            method: self::POST_METHOD,
            url: self::ADDRESS_ROUTE,
            json: $data,
            token: $this->customerConfirmedExampleToken
        );

        $this->assertViolations(['phoneNumber'], $requestNotRespectMaxValidation);

//        Malformatted customer iri error

        $data = $validAddress;
        $data['customer'] = 'azer';

        $this->doRequest(
            method: self::POST_METHOD,
            url: self::ADDRESS_ROUTE,
            json: $data,
            token: $this->customerConfirmedExampleToken
        );

        $this->assertBadRequest();

//        Malformatted country iri error

        $data = $validAddress;
        $data['country'] = 'azer';

        $this->doRequest(
            method: self::POST_METHOD,
            url: self::ADDRESS_ROUTE,
            json: $data,
            token: $this->customerConfirmedExampleToken
        );

        $this->assertBadRequest();
    }

    public function testPatchInvalidItem()
    {
        // TODO: Implement testPatchInvalidItem() method.
    }

    public function testGetCollectionWithFilter()
    {
        // TODO: Implement testGetCollectionWithFilter() method.
    }

}
