stages:
  - build
  - linter
  - test
  - deploy

default:
  image: h4e6r54h6e6h54s6alui49/symfony-mysql
  services:
    - name: mariadb:10.3.34
      alias: mysql

  before_script:
    # Installation des dépendances
    - composer install
    - php bin/console d:d:c -q --if-not-exists --env=test;
    - php bin/console d:m:m -q --env=test;
    - php bin/console lexik:jwt:generate-keypair --overwrite

  cache:
    key: $CI_COMMIT_REF_SLUG
    paths:
      - vendor

.build_test_variables: &build_test_variables
  #  DB
  DB_NAME: "good_food"
  DB_USER: "root"
  DB_PASSWORD: "root"
  DB_PORT: "3306"
  DB_HOST: "mysql"
  DB_DRIVER: "mysql"
  DB_VERSION: 'mariadb-10.3.34'
  MARIADB_ROOT_PASSWORD: $DB_PASSWORD
  DATABASE_URL: "$DB_DRIVER://$DB_USER:$DB_PASSWORD@$DB_HOST:$DB_PORT/$DB_NAME?serverVersion=$DB_VERSION&charset=utf8"
  #  Mail
  MAILER_DSN: "$MAILER_DSN"
  DEFAULT_SENDER_EMAIL: "$DEFAULT_SENDER_EMAIL"
  EMAIL_TEST: "$EMAIL_TEST"
  # Divers
  JWT_PASSPHRASE: "$JWT_PASSPHRASE"
  WEB_SITE_URL: "$WEB_SITE_URL"
  ACCOUNT_CONFIRMATION_PASSPHRASE: "$ACCOUNT_CONFIRMATION_PASSPHRASE"

build:
  stage: build
  variables:
    <<: *build_test_variables
  script:
    - php bin/phpunit --version
  environment:
    name: test
  only:
    - dev
    - main

unit-test:
  stage: test
  variables:
    <<: *build_test_variables
  script:
    - php bin/phpunit tests/unit/ --colors=never -d memory_limit=256M
  dependencies:
    - build
  environment:
    name: test
  only:
#    - dev
    - main

func-test:
  stage: test
  variables:
    <<: *build_test_variables
  script:
    - php bin/phpunit tests/func/ --colors=never -d memory_limit=256M
  dependencies:
    - build
  environment:
    name: test
  only:
#    - dev
    - main

.config_ssh: &config_ssh
  - "which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )"
  - eval $(ssh-agent -s)
  - printenv SSH_PRIVATE_KEY | ssh-add -
  - mkdir -p ~/.ssh
  - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
  - >
    ssh -p $SSH_PORT $SSH_USER@$SSH_HOST "
    mkdir -p $PATH_TO_PROJECT/$CI_COMMIT_BRANCH;
    echo "Fin du script";
    exit;
    "

.before_script_deploy: &before_script_deploy
  # Génération du .env.local
  - PASSWORD_FILE="ansible/password.txt"
  - printenv ANSIBLE_PASSWORD > $PASSWORD_FILE
  - mkdir -p /ansible
  - ansible-playbook --vault-password-file=$PASSWORD_FILE "ansible/$CI_COMMIT_BRANCH.yml"

.script_deploy: &script_deploy
  - ssh -p $SSH_PORT $SSH_USER@$SSH_HOST "mkdir -p $PATH_TO_PROJECT/$CI_COMMIT_BRANCH"
  - sftp -P $SSH_PORT $SSH_USER@$SSH_HOST:$PATH_TO_PROJECT/$CI_COMMIT_BRANCH <<< $'put /ansible/.env.local'
  - sftp -P $SSH_PORT $SSH_USER@$SSH_HOST:$PATH_TO_PROJECT/$CI_COMMIT_BRANCH <<< $'put /ansible/.env.local .env.test.local'
  - >
    ssh -p $SSH_PORT $SSH_USER@$SSH_HOST "
    cd $PATH_TO_PROJECT/$CI_COMMIT_BRANCH;
    
    echo "Si il y a un repo git";
    
    if git config --get remote.origin.url; then
    
    echo "On reset les modifs et pull";
    
    git reset --hard HEAD;
    git clean -f -d;
    git checkout $CI_COMMIT_BRANCH;
    git pull;
    
    else
    
    echo "On supprime le répertoire et clone le projet";
    
    cd ..;
    rm -rf $CI_COMMIT_BRANCH;
    mkdir $CI_COMMIT_BRANCH;
    
    git clone $REPO -b $CI_COMMIT_BRANCH $CI_COMMIT_BRANCH;
    
    cd $CI_COMMIT_BRANCH;
    
    fi;
    
    echo "Installation de composer";
    
    if [ ! -f './composer.phar' ]; then
    
    php -r 'echo copy(\"https://getcomposer.org/installer\", \"composer-setup.php\");';
    php -r 'if (hash_file("sha384", "composer-setup.php") === "906a84df04cea2aa72f40b5f787e49f22d4c2f19492ac310e8cba5b96ac8b64115ac402c8cd292b8a03482574915d1a8") { echo "Installer verified"; } else { echo "Installer corrupt"; unlink("composer-setup.php"); } echo PHP_EOL;';
    php composer-setup.php;
    php -r 'unlink("composer-setup.php");';
    
    fi;
    
    echo "Installation des dépendances et de la db";
    
    php composer.phar install;
    php bin/console doctrine:database:create -q --if-not-exists;
    php bin/console doctrine:migration:migrate -q;


    echo "Fin du programme";
    
    exit;"

.deploy_variables: &deploy_variables
  SSH_PRIVATE_KEY: "$SSH_PRIVATE_KEY"
  REPO: "$REPO"
  ANSIBLE_PASSWORD: "$ANSIBLE_PASSWORD"
  SSH_HOST: "$SSH_HOST"
  SSH_USER: "$SSH_USER"
  SSH_PORT: "$SSH_PORT"
  PATH_TO_PROJECT: "$PATH_TO_PROJECT"

deploy-dev:
  stage: deploy
  variables:
    <<: *deploy_variables
  before_script:
    - *before_script_deploy
    - *config_ssh
  script:
    - *script_deploy
    - >
      ssh -p $SSH_PORT $SSH_USER@$SSH_HOST "
      cd $PATH_TO_PROJECT/$CI_COMMIT_BRANCH;

      echo 'Chargement des fixtures';      

      php bin/console hautelook:fixtures:load -q;
      
      php bin/console lexik:jwt:generate-keypair --forec;
      
      echo "Fin du programme";
      
      exit;
      "
  environment:
    name: preprod
    url: "https://api.good-food.deleuze-cedric.com"
    on_stop: remove_env
  dependencies:
    - func-test
    - unit-test
  only:
    - dev
    - Fix-deployment

remove_env:
  before_script:
    - *config_ssh
  script:
    - >
      ssh -p $SSH_PORT $SSH_USER@$SSH_HOST "

      cd $PATH_TO_PROJECT;
      
      cd ..;
      
      mkdir -p backup;
      
      cd $PATH_TO_PROJECT;

      DATE=$(date +"%Y-%m-%d %T")

      BCKP_PATH="../backup/$CI_COMMIT_BRANCH_\$DATE"

      tar czvf "$BCKP_PATH.tar.gz" $CI_COMMIT_BRANCH;
      
      php app/console doctrine:schema:create --dump-sql > $BCKP_PATH.sql
      
      php app/console doctrine:database:drop --force
      
      rm -rf $CI_COMMIT_BRANCH;
      
      exit;
      "
  environment:
    name: preprod
    action: stop
  rules:
    - if: $CI_MERGE_REQUEST_ID
      when: manual

#deploy-prod:
#  stage: deploy
#  variables:
#    <<: *deploy_variables
#  before_script:
#    - *before_script_deploy
#    - *config_ssh
#  script:
#    - *script_deploy
#  environment:
#    name: prod
#    url: https://good-food.com
#  when: manual
#  dependencies:
#    - unit-test
#    - func-test
#  only:
#    - main