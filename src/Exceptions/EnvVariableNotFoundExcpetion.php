<?php


namespace App\Exceptions;


use Exception;
use JetBrains\PhpStorm\Pure;
use Throwable;

class EnvVariableNotFoundExcpetion extends Exception
{

    #[Pure] public function __construct($envVariable = "", $code = 0, Throwable $previous = null)
    {
        $message = "La variable d'environnement '$envVariable' n'est pas renseignée.";
        parent::__construct($message, $code, $previous);
    }

}