<?php

namespace App\Controller\Restaurant;

use App\Repository\RestaurantRepository;
use Doctrine\DBAL\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[AsController]
class GetRestaurantAroundLocationController extends AbstractController
{

    public function __construct(
        private RestaurantRepository $restaurantRepository
    )
    {
    }

    /**
     * @throws Exception
     */
    #[Route(
        path: '/api/restaurants/around_location/{longitude}/{latitude}',
        name: 'get_restaurant_around_location',
        requirements: [
            'longitide' => '[-+]?[0-9]*\.?[0-9]*',
            'latitude' => '[-+]?[0-9]*\.?[0-9]*',
        ],
        defaults: [
            '_api_item_operation_name' => 'get_restaurant_around_location',
        ],
        methods: ['GET']
    )]
    public function __invoke(float $longitude, float $latitude, SerializerInterface $serializer): JsonResponse
    {

        if($longitude < 0 || $longitude > 180){
            throw new BadRequestException('La longitude doit être compris en 0 et 180');
        }

        if( $latitude < -90 || $latitude > 90){
            throw new BadRequestException('La latitude doit être compris en -90 et 90');
        }

        $data = $this->restaurantRepository->getRestaurantAroundLocation($longitude, $latitude);

        $json = $serializer->serialize($data, 'json', ['groups' => ['restaurant', 'restaurant:read']]);

        return new JsonResponse($json, 200, [], true);
    }

}