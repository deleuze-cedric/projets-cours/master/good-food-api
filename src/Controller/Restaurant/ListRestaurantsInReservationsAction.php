<?php

namespace App\Controller\Restaurant;

use App\Repository\CountryRepository;
use App\Repository\RestaurantRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ListRestaurantsInReservationsAction extends AbstractController
{

    public function __construct(
        private RestaurantRepository $restaurantRepository
    )
    {
    }

    public function __invoke(): array
    {
        return $this->restaurantRepository->getRestaurantsInReservations();
    }

}