<?php

namespace App\Controller\Order;

use App\Entity\Order\Order;
use App\Entity\User\Customer\Customer;
use App\Repository\OrderRepository;
use App\Repository\OrderStatusRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\SerializerInterface;

class GetCartAction extends AbstractController
{

    #[Route(
        path: '/api/customers/cart',
        name: 'get_cart',
        defaults: [
            '_api_item_operation_name' => 'get_cart',
        ],
        methods: ['GET']
    )]
    public function __invoke(
        Security               $security,
        EntityManagerInterface $entityManager,
        OrderStatusRepository  $orderStatusRepository,
        OrderRepository        $orderRepository,
        SerializerInterface    $serializer
    ): JsonResponse
    {

        $customer = $security->getUser();

        if ($customer instanceof Customer) {

            $cartStatus = $orderStatusRepository->findOneBy(['slug' => 'panier']);
            $lastOrders = $orderRepository->findBy(['status' => $cartStatus, 'orderedAt' => null, 'customer' => $customer]);

            if (!empty($lastOrders)) {
                $cart = $lastOrders[0];
            } else {
                $cart = new Order();
                $cart->setReference(uniqid());
                $cart->setCustomer($customer);
                $cart->setStatus($cartStatus);
                $entityManager->persist($cart);
                $entityManager->flush();
            }

            $json = $serializer->serialize($cart, 'json', ['groups' => ['order', 'order:read']]);

            return new JsonResponse($json, 200, [], true);

        } else {
            throw new AccessDeniedException();
        }

    }

}