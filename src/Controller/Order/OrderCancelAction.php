<?php

namespace App\Controller\Order;

use App\Entity\Order\Order;
use App\Repository\OrderStatusRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;

class OrderCancelAction extends AbstractController
{

    public function __invoke(Order $data, OrderStatusRepository $orderStatusRepository): Order
    {
        $cartStatus = $orderStatusRepository->findOneBy(['slug' => 'annulee']);
        $data->setStatus($cartStatus);

        return $data;
    }

}