<?php

namespace App\Controller\Order;

use App\Entity\Order\Order;
use App\Repository\OrderStatusRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;

class OrderRefundAction extends AbstractController
{

    public function __invoke(Order $data, OrderStatusRepository $orderStatusRepository): Order
    {
        $refundStatus = $orderStatusRepository->findOneBy(['slug' => 'remboursee']);
        $data->setStatus($refundStatus);
        $data->setRefundedAt();

        return $data;
    }

}