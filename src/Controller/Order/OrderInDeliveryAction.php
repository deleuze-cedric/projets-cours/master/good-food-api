<?php

namespace App\Controller\Order;

use App\Entity\Order\Order;
use App\Repository\OrderStatusRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;

class OrderInDeliveryAction extends AbstractController
{

    public function __invoke(Order $data, OrderStatusRepository $orderStatusRepository): Order
    {
        $status = $orderStatusRepository->findOneBy(['slug' => 'en-cours-de-livraison']);
        $data->setStatus($status);

        return $data;
    }

}