<?php

namespace App\Controller\Tools;

use App\Service\Tools\PasswordGenerator;
use stdClass;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class PasswordGenerateController extends AbstractController
{

    /**
     * @param PasswordGenerator $passwordGenerator
     *
     * @return JsonResponse
     */
    #[Route(path: "/api/generate_password", name: "generate_password", methods: ["GET"])]
    public function generate(PasswordGenerator $passwordGenerator): JsonResponse
    {
        $obj = new stdClass();
        $obj->password = $passwordGenerator->generate();

        return new JsonResponse($obj, 200);
    }

}