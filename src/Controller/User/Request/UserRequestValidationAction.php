<?php


namespace App\Controller\User\Request;


use App\Entity\User\UserRequest\UserRequest;
use App\Event\UserRequestCompleteEvent;
use Doctrine\ORM\EntityManagerInterface;
use JetBrains\PhpStorm\NoReturn;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class UserRequestValidationAction extends AbstractController
{
	const REQUEST_ALREADY_USED = "La requête a déjà été utilisée..";
	const REQUEST_EXPIRED = "La requête a expiré, veuillez en recréer une.";

	#[NoReturn] public function __invoke(UserRequest $data, EntityManagerInterface $entityManager, EventDispatcherInterface $dispatcher): UserRequest
	{
		if ($data->getAlreadyUsed()) {
			throw new BadRequestHttpException(self::REQUEST_ALREADY_USED);
		}

		if ($data->requestHasExpired()) {
			throw new BadRequestHttpException(self::REQUEST_EXPIRED);
		}

		$event = new UserRequestCompleteEvent($data);
		$dispatcher->dispatch($event, UserRequestCompleteEvent::NAME);

		$data->setAlreadyUsed();

		return $data;
	}

}