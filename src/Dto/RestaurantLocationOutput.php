<?php

namespace App\Dto;

use App\Entity\Restaurant\Restaurant;
use Symfony\Component\Serializer\Annotation\Groups;

class RestaurantLocationOutput
{

    #[Groups('restaurant')]
    public float $distance;

    #[Groups('restaurant')]
    public float $searchedLongitude;

    #[Groups('restaurant')]
    public float $searchedLatitude;

    #[Groups('restaurant')]
    public Restaurant $restaurant;

}