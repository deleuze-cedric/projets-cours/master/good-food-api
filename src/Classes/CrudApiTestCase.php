<?php

namespace App\Classes;

abstract class CrudApiTestCase extends CustomApiTestCase
{

    /**
     * Appeler cette fonction pour les endpoints inexistants (suppression d'un super admin par exemple)
     */
    public function inexistantEndpointTest()
    {
        $this->assertEquals(true, true);
    }

    public abstract function testDeleteItem();

    public abstract function testGetCollection();

    public abstract function testGetCollectionWithFilter();

    public abstract function testGetItem();

    public abstract function testPatchInvalidItem();

    public abstract function testPatchItem();

    public abstract function testPostInvalidItem();

    public abstract function testPostItem();

}