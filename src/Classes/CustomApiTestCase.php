<?php

namespace App\Classes;


use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\Client;
use App\Entity\User\User;
use App\Exceptions\EnvVariableNotFoundExcpetion;
use App\Service\Tools\Tools;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;
use JetBrains\PhpStorm\Pure;
use Nelmio\Alice\Loader\NativeLoader;
use Nelmio\Alice\ObjectSet;
use Nelmio\Alice\Throwable\LoadingThrowable;
use ReflectionObject;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\String\AbstractUnicodeString;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

abstract class CustomApiTestCase extends ApiTestCase
{

    use ReloadDatabaseTrait;

    // Content type request
    const CONTENT_TYPE_LD_JSON = 'application/ld+json; charset=utf-8';
    const CONTENT_TYPE_PATCH_JSON = 'application/merge-patch+json';
    const CONTENT_TYPE_JSON = 'application/json';

    // Base route
    const API_ROUTE = '/api';
    const LOGIN_ROUTE = self::API_ROUTE . '/login';

    // Base return ld json data
    const CONTEXT = '/api/contexts';

    // JSON LD DATA
    const CONTEXT_INDEX_JSON_LD = '@context';
    const ID_INDEX_JSON_LD = '@id';
    const TYPE_INDEX_JSON_LD = '@type';
    const HYDRA_TITLE_INDEX = 'hydra:title';
    const HYDRA_COLLECTION_TYPE_VALUE_JSON_LD = 'hydra:Collection';
    const HYDRA_TOTAL_ITEMS_INDEX_JSON_LD = 'hydra:totalItems';
    const HYDRA_VIEW_INDEX_JSON_LD = 'hydra:view';
    const HYDRA_PARTIAL_COLLECTION_VIEW_TYPE_VALUE_JSON_LD = 'hydra:PartialCollectionView';
    const HYDRA_FIRST_INDEX_JSON_LD = 'hydra:first';
    const HYDRA_LAST_INDEX_JSON_LD = 'hydra:last';
    const HYDRA_NEXT_INDEX_JSON_LD = 'hydra:next';
    const HYDRA_MEMBER_INDEX_JSON_LD = 'hydra:member';

    // Violations
    const VIOLATION_TYPE = 'ConstraintViolationList';
    const CONTEXT_CONSTRAINT_VIOLATION = self::CONTEXT . '/' . self::VIOLATION_TYPE;
    const ERROR_OCCURED_ERROR = 'Une erreur est survenue';

    // Verbes HTTP
    const GET_METHOD = 'GET';
    const POST_METHOD = 'POST';
    const PATCH_METHOD = 'PATCH';
    const PUT_METHOD = 'PUT';
    const DELETE_METHOD = 'DELETE';

    // USERS
    const CEDRIC_ADMIN = 'cedric_admin';
    const ARTHUR_ADMIN = 'arthur_admin';
    const CORENTIN_ADMIN = 'corentin_admin';
    const ROLE_ADMIN = 'role_admin';
    const ROLE_EDITOR = 'role_editor';
    const ROLE_ACCOUNTANT = 'role_accountant';
    const ROLE_HR = 'role_hr';
    const ROLE_CUSTOMER_NOT_CONFIRMED = 'customer_not_confirmed';
    const ROLE_CUSTOMER_CONFIRMED = 'customer_confirmed';
    const ROLE_RESTORATOR = 'role_restorator';
    const ROLE_LOGISTICIAN = 'role_logistician';
    const ROLE_LEAD = 'role_lead';
    const NOT_AUTH = 'not_auth';
    const ALL_USERS = 'all_users';

    const PASSWORDS = [
        self::CEDRIC_ADMIN                => 'aaAA&&11',
        self::ARTHUR_ADMIN                => 'aaAA&&22',
        self::CORENTIN_ADMIN              => 'aaAA&&33',
        self::ROLE_ADMIN                  => 'aaAA&&44',
        self::ROLE_EDITOR                 => 'aaAA&&55',
        self::ROLE_ACCOUNTANT             => 'aaAA&&66',
        self::ROLE_HR                     => 'aaAA&&77',
        self::ROLE_CUSTOMER_NOT_CONFIRMED => 'aaAA&&88',
        self::ROLE_CUSTOMER_CONFIRMED     => 'aaAA&&1212',
        self::ROLE_RESTORATOR             => 'aaAA&&99',
        self::ROLE_LOGISTICIAN            => 'aaAA&&1010',
        self::ROLE_LEAD                   => 'aaAA&&1111',
    ];

    // Test value (violations)
    const STRING_LENGTH_256 = "rG20uMnjOmfeUKPpD6aeUlg1nfmspzuUcekyiO477J6nvcJkkLVeKmotFA52xVXivaebw33msrPtkQQSxAAEGyeIBz0KEWFp5ZfKtDQVMtBC7SWglzXS1vrTJHL5DF511VyD0Vgg3NgTSn47cZClZnw4atz6ekwMl2Jvjv1JplzLdL5wXziBidyt5ExwdpvOnjwlS03rHWju1IyCTcjePunL8ZanBTrFEiJRgKrR2UO4jdzeNiFeuFjz3JuHZ9MO";

    // DEFAULT DATA
    const COLLECTION_OPERATION_DEFAULT_RESULT_NUMBER = 30;

    private ?Client $client;

    private static ?EntityManagerInterface $entityManager;

    private static ?NormalizerInterface $normalizer;

    private static ?SluggerInterface $slugger;

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function assertBadRequest(?ResponseInterface $response = null, ?string $message = null)
    {
        $this->assertResponseHeaderSame('content-type', self::CONTENT_TYPE_LD_JSON);
        $this->assertResponseStatusCodeSame(400);

        if (!empty($response) && !empty($message)) {
            $data = $response->toArray(false);
            $this->assertEquals($message, $data['hydra:description']);
        }
    }

    /**
     * Vérifie la validité de la collection renvoyée par l'api
     *
     * @param ResponseInterface $response                => Réponse renvoyée par l'api
     * @param string            $endpointCollectionRoute => Route utilisé pour récupérer la collection (Ex:
     *                                                   /api/customers)
     * @param string            $context                 => Type d'entité dans la collection (Ex: /api/Customer)
     * @param string            $classToCheckValidity    => Entité utilisé pour validé le contenu de la réponse (Ex:
     *                                                   Customer::class)
     * @param int               $totalItems              => Nombre total d'items devant être présent dans la réponse
     * @param int               $currentPage             => Page actullement visitée (Par défaut 1)
     * @param int               $nbEntityPerPage         => Nombre d'entité par page (Par défaut 30)
     *
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function assertCollection(
        ResponseInterface $response,
        string            $endpointCollectionRoute,
        string            $context,
        string            $classToCheckValidity,
        int               $totalItems,
        int               $currentPage = 1,
        int               $nbEntityPerPage = self::COLLECTION_OPERATION_DEFAULT_RESULT_NUMBER
    )
    {

        $firstPage = 1;
        // Le nombre est égale au (nombre d'entité au total / nombre d'entité par page) + 1 page => (20 / 30) + 1 = 1 page
        $nbPage = (int)($totalItems / $nbEntityPerPage) + 1;
        // La page suivante doit être inférieur ou égale au nombre de page
        $nextPage = min($currentPage + 1, $nbPage);
        // Le nombre d'entité dans cette page dépend si l'on est sur la dernière ou pas.
        // Si c'est le cas on récupère le reste de la division utilisé pour le nombre de page => 20 / 30 (q=0, r=20) = Il y aura 20 items dans cette page
        // Sinon la page sera entière (30 items dans la page si on se trouve à la page 1 et qu'il y a 40 entités au total) 40/30 (q=1, r=10) => nb page = 2
        $nbEtityInThisPage = $currentPage == $nbPage ? $totalItems % $nbEntityPerPage : $nbEntityPerPage;

        $this->assertSuccess();

        $this->assertJsonContains(
            [
                self::CONTEXT_INDEX_JSON_LD           => $context,
                self::ID_INDEX_JSON_LD                => $endpointCollectionRoute,
                self::TYPE_INDEX_JSON_LD              => self::HYDRA_COLLECTION_TYPE_VALUE_JSON_LD,
                self::HYDRA_TOTAL_ITEMS_INDEX_JSON_LD => $totalItems,
            ]
        );

        if ($nbPage > 1) {
            $this->assertJsonContains(
                [
                    self::HYDRA_VIEW_INDEX_JSON_LD => [
                        self::TYPE_INDEX_JSON_LD        => self::HYDRA_PARTIAL_COLLECTION_VIEW_TYPE_VALUE_JSON_LD,
                        self::ID_INDEX_JSON_LD          => $endpointCollectionRoute . '?page=' . $currentPage,
                        self::HYDRA_FIRST_INDEX_JSON_LD => $endpointCollectionRoute . '?page=' . $firstPage,
                        self::HYDRA_LAST_INDEX_JSON_LD  => $endpointCollectionRoute . '?page=' . $nbPage,
                        self::HYDRA_NEXT_INDEX_JSON_LD  => $endpointCollectionRoute . '?page=' . $nextPage,
                    ],
                ]
            );
        }

        // Because test fixtures are automatically loaded between each test, you can assert on them
        $this->assertCount($nbEtityInThisPage, $response->toArray()[self::HYDRA_MEMBER_INDEX_JSON_LD]);

        // Asserts that the returned JSON is validated by the JSON Schema generated for this resource by API Platform
        // This generated JSON Schema is also used in the OpenAPI spec!
        $this->assertMatchesResourceCollectionJsonSchema($classToCheckValidity);
    }

    public function assertSuccess()
    {
        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', self::CONTENT_TYPE_LD_JSON);
    }

    /**
     * Permet de valider la valeur du @id dans la réponse en json ld
     *
     * @param string $routeToEndpoint      => chemin jusqu'au endpoint de l'entité (Ex: /api/customer) !!!! ne pas
     *                                     mettre de / à la fin
     * @param string $regexUsedToConfirmID => regex validant l'identifiant (Tools::UUID_REGEX|Tools::SLUG_REGEX|...)
     * @param array  $data                 => données renvoyés dans la réponse (doit correspondre àune entité)
     */
    public function assertIdentifierOfResponseData(string $routeToEndpoint, string $regexUsedToConfirmID, array $data)
    {
        $this->assertMatchesRegularExpression('~^' . $routeToEndpoint . '/' . $regexUsedToConfirmID . '$~', $data[self::ID_INDEX_JSON_LD]);
    }

    public function assertNotFound()
    {
        $this->assertResponseStatusCodeSame(404);
    }

    /**
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function assertSubResourceCollection(
        ResponseInterface $response,
        string            $endpointSubResourceCollectionRoute,
        string            $context,
        string            $classToCheckValidity,
        int               $totalItems,
        array             $entityCreated
    )
    {
        $this->assertSuccess();
        $this->assertJsonContains(
            [
                self::CONTEXT_INDEX_JSON_LD           => $context,
                self::ID_INDEX_JSON_LD                => $endpointSubResourceCollectionRoute,
                self::TYPE_INDEX_JSON_LD              => self::HYDRA_COLLECTION_TYPE_VALUE_JSON_LD,
                self::HYDRA_TOTAL_ITEMS_INDEX_JSON_LD => $totalItems,
            ]
        );

        $entities = $response->toArray()[self::HYDRA_MEMBER_INDEX_JSON_LD];
        $this->assertMatchesResourceCollectionJsonSchema($classToCheckValidity);

        $entityFound = false;

        foreach ($entities as $entity) {

            if ($entity['@id'] == $entityCreated['@id']) {
                $entityFound = true;
                break;
            }
        }

        $this->assertEquals(true, $entityFound);
    }

    public function assertSuccessDelete()
    {
        $this->assertResponseStatusCodeSame(204);
    }

    public function assertUnprocessableEntity()
    {
        $this->assertResponseStatusCodeSame(422);
        $this->assertResponseHeaderSame('content-type', self::CONTENT_TYPE_LD_JSON);
    }

    /**
     * Vérifie que la réponse retourne bien les erreurs attendues
     *
     * @param array             $violationsToCheck => Tableau des champs qui devraient avoir une erreur
     * @param ResponseInterface $response          => réponse retournée par l'api
     *
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    protected function assertViolations(array $violationsToCheck, ResponseInterface $response)
    {

        $this->assertResponseStatusCodeSame(422);
        $this->assertResponseHeaderSame('content-type', self::CONTENT_TYPE_LD_JSON);

        $this->assertJsonContains(
            [
                self::CONTEXT_INDEX_JSON_LD => self::CONTEXT_CONSTRAINT_VIOLATION,
                self::TYPE_INDEX_JSON_LD    => self::VIOLATION_TYPE,
                self::HYDRA_TITLE_INDEX     => self::ERROR_OCCURED_ERROR,
            ]
        );

        $data = $response->toArray(false);

        $this->assertArrayHasKey('violations', $data);

        $violations = $data['violations'];
        $nbViolationFound = 0;

        foreach ($violationsToCheck as $violationToCheck) {

            $violationFound = null;

            foreach ($violations as $violation) {

                if ($violation['propertyPath'] == $violationToCheck) {
                    $violationFound = $violation;
                    $nbViolationFound++;
                    break;
                }
            }

            $this->assertNotNull($violationFound);
            $this->assertArrayHasKey('message', $violationFound);
        }

        $this->assertCount($nbViolationFound, $violationsToCheck);
    }

    /**
     * @param array          $authorizedUserIndex                 => Liste des index des utilisateurs étant authorisés
     *                                                            à faire cette rqt.
     * @param array|callable $functionAssertRequestAuthorizedUser => Fonction de callback appellé après avoir fait la
     *                                                            rqt pour les utilisateurs authorisés. Ex : function(
     *                                                            ResponseInterface
     *                                                            $response, string $token, string $userIndex, User
     *                                                            $userData,
     *                                                            ?array $json
     *                                                            )
     * @param string         $method                              => Verbe HTTP (self::GET_METHOD, self::POST_METHOD,
     *                                                            ...)
     * @param string         $url                                 => URL appelé lors de la rqt
     * @param array|null     $json                                => Data envoyé au format json
     * @param array|null     $complementaryRequestOptions         => Options complémentaires utilisés par la rqt
     *
     * @return void
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function doRequestForEachUsers(
        array          $authorizedUserIndex,
        array|callable $functionAssertRequestAuthorizedUser,
        string         $method,
        string         $url,
        ?array         $json = null,
        ?string        $contentType = null,
        ?array         $complementaryRequestOptions = null,
        array          $dataToSendToFunctionAssertAhtorizeUser = []
    )
    {
        // Pour chaque utilisateur
        foreach (self::getUserFixturesClean() as $userIndex => $userData) {
            // On génère un token d'authentification
            $token = $this->getTokenFromUserData($userData, $userIndex);

            $response = $this->doRequest(
                $method,
                $url,
                $json,
                $token,
                $contentType,
                $complementaryRequestOptions
            );

            // Si l'utilisateur en cours fait partie des utilisateurs étant authorisé à faire cette requête
            if (in_array($userIndex, $authorizedUserIndex) || in_array(self::ALL_USERS, $authorizedUserIndex)) {
                // On appel la fonction callback
                call_user_func($functionAssertRequestAuthorizedUser, $response, $token, $userIndex, $userData, $json, $dataToSendToFunctionAssertAhtorizeUser);
            } else {

                // Sinon
                switch ($userIndex) {

                    // Soit c'est un utilisateur sans compte => no token found
                    case self::NOT_AUTH:
                        $this->assertNotLogin();
                        break;

                    // Soit utilisateur non autorisé => Access denied
                    default:
                        $this->assertAccessDenied();
                        break;
                }
            }
        }
    }

    public static function getUserFixturesClean(): array
    {
        $users = [];

        foreach (self::$fixtures as $index => $data) {

            if ($data instanceof User &&
                (
                    isset(self::PASSWORDS[$index]) || $index == self::NOT_AUTH
                )
            ) {
                $users[$index] = $data;
            }
        }

        return $users;
    }

    /**
     * Find a user from his userIndex and login it
     *
     * @param User   $userData
     * @param string $userIndex
     *
     * @return string|null
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    protected function getTokenFromUserData(User $userData, string $userIndex): ?string
    {
        $username = $userData->getUserIdentifier();
        $password = self::PASSWORDS[$userIndex] ?? null;

        return $this->getTokenForUser($username, $password);
    }

    /**
     * Login a user and retrieve his auth token
     *
     * @param string|null $username
     * @param string|null $password
     * @param bool        $returnResponse
     *
     * @return string|ResponseInterface|null
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    protected function getTokenForUser(?string $username, ?string $password, bool $returnResponse = false): string|ResponseInterface|null
    {
        if (!$returnResponse && empty($password) && empty($username)) {
            return null;
        }

        $response = $this->doRequest(
            method: self::POST_METHOD,
            url: self::LOGIN_ROUTE,
            json: [
                'username' => $username,
                'password' => $password,
            ],
            contentType: self::CONTENT_TYPE_JSON
        );

        if (!$returnResponse) {
            $json = $response->toArray();

            return $json['token'];
        } else {
            return $response;
        }

    }

    /**
     * @throws TransportExceptionInterface
     */
    public function doRequest(
        string  $method,
        string  $url,
        ?array  $json = null,
        ?string $token = null,
        ?string $contentType = null,
        ?array  $complementaryRequestOptions = null
    ): ResponseInterface
    {

        // Auth pour la rqt
        $options = [
            'auth_bearer' => $token,
        ];

        // Data send
        if (!is_null($json) && $method !== self::GET_METHOD) {
            $options['json'] = $json;
        }

        if (!is_null($contentType)) {
            $options['headers'] = [
                'content-type' => $contentType,
            ];
        } else {
            if ($method == self::PATCH_METHOD) {
                $options['headers'] = [
                    'content-type' => self::CONTENT_TYPE_PATCH_JSON,
                ];
            }
        }

        // Options supplémentaires
        if (!empty($complementaryRequestOptions)) {
            $options = array_merge($options, $complementaryRequestOptions);
        }

        // Envoie de la requête
        return $this->client->request(
            method: $method,
            url: $url,
            options: $options
        );

    }

    public function assertNotLogin()
    {
        $this->assertResponseStatusCodeSame(401);
    }

    public function assertAccessDenied()
    {
        $this->assertResponseHeaderSame('content-type', self::CONTENT_TYPE_LD_JSON);
        $this->assertResponseStatusCodeSame(403);
    }

    /**
     * @throws Exception
     */
    public static function getEntityManager(): EntityManagerInterface
    {
        if (empty(self::$entityManager)) {

            $entityManager = self::getContainer()->get(EntityManagerInterface::class);

            if ($entityManager instanceof EntityManagerInterface) {
                self::$entityManager = $entityManager;
            } else {
                throw new Exception('Invalid entity manager.');
            }

        }

        return self::$entityManager;
    }

    /**
     * @throws EnvVariableNotFoundExcpetion
     */
    protected static function getTestEmail()
    {
        if (empty($_SERVER['EMAIL_TEST'])) {
            throw new EnvVariableNotFoundExcpetion('EMAIL_TEST');
        }

        return $_SERVER['EMAIL_TEST'];
    }

    /**
     * Find a user from his userIndex and login it
     *
     * @param string $userIndex
     *
     * @return string|null
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    protected function getTokenFromUserIndex(string $userIndex): ?string
    {
        $userFixturesClean = self::getUserFixturesClean();

        if (isset($userFixturesClean[$userIndex])) {
            $userData = $userFixturesClean[$userIndex];

            return $this->getTokenFromUserData($userData, $userIndex);
        }

        return null;
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function getTotalItemsForRequest(string $route, ?string $token)
    {
        $response = $this->doRequest(
            method: self::GET_METHOD,
            url: $route,
            token: $token
        );

        return $response->toArray()[self::HYDRA_TOTAL_ITEMS_INDEX_JSON_LD];
    }

    /**
     * @return User[]
     */
    #[Pure] public static function getUserFixtures(): array
    {
        return self::getDataFixturesOfClass(User::class);
    }

    public static function getDataFixturesOfClass(string $class): array
    {
        $return = [];

        foreach (self::$fixtures as $index => $data) {

            if ($data instanceof $class) {
                $return[$index] = $data;
            }
        }

        return $return;
    }

    /**
     * @throws LoadingThrowable
     */
    public static function loadYamlFixturesFromObjectSet(string $filename, ObjectSet $objectSet = null): ObjectSet
    {
        return self::loadYamlFixtures(
            $filename,
            !empty($objectSet) ? $objectSet->getParameters() : [],
            !empty($objectSet) ? $objectSet->getObjects() : []
        );
    }

    /**
     * @throws LoadingThrowable
     */
    public static function loadYamlFixtures(string $filename, array $parameters = [], array $objects = []): ObjectSet
    {
        $loader = new NativeLoader();

        return $loader->loadFile(__DIR__ . '/../../fixtures/' . $filename, $parameters, $objects);
    }

    /**
     * @param Object $entity
     * @param bool   $clearNullValue
     *
     * @return array
     * @throws ExceptionInterface
     * @throws Exception
     */
    public static function parseEntityToArray(object $entity, bool $clearNullValue = true): array
    {
        $data = self::getNormalizer()->normalize($entity);

        return self::clearUselessData($data, $clearNullValue);
    }

    /**
     * @throws Exception
     */
    public static function getNormalizer(): NormalizerInterface
    {
        if (empty(self::$normalizer)) {

            $normalizer = self::getContainer()->get(NormalizerInterface::class);

            if ($normalizer instanceof NormalizerInterface) {
                self::$normalizer = $normalizer;
            } else {
                throw new Exception('Invalid normalizer.');
            }

        }

        return self::$normalizer;
    }

    private static function clearUselessData(array $data, bool $clearNullValue = true): array
    {

        $return = [];

        $uselessFieldToTest = [
            'updatedAt',
            'createdAt',
            'active',
            'deleted',
        ];

        foreach ($data as $field => $value) {
            if (
                (
                    !$clearNullValue || !is_null($value)
                ) &&
                !in_array($field, $uselessFieldToTest)
            ) {
                if (is_array($value)) {
                    $value = self::clearUselessData($value);
                }
                $return[$field] = $value;
            }
        }

        return $return;
    }

    /**
     * Remplace les paramètres envoyés par leurs valeurs dans la route envoyés
     *
     * @param string $route
     * @param array  $params
     *
     * @return string
     */
    public static function replaceParamsRoute(string $route, array $params): string
    {
        foreach ($params as $param => $value) {
            $route = str_replace('{' . $param . '}', $value, $route);
        }

        return $route;
    }

    public function setUp(): void
    {
        parent::setUp();
        $this->setInIsolation(true);
        $this->setRunTestInSeparateProcess(true);
        self::bootKernel();
        $this->initClient();
    }

    protected function initClient()
    {
        if (!empty($this->client)) {
            unset($this->client);
        }
        $this->client = self::createClient();
        $this->client->disableReboot();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        if (!empty(self::$entityManager)) {
            self::$entityManager->close();
        }
        self::ensureKernelShutdown();
        self::$entityManager = null;
        self::$slugger = null;
        self::$normalizer = null;
        self::$fixtures = null;
        $this->client = null;

        $refl = new ReflectionObject($this);
        foreach ($refl->getProperties() as $prop) {
            if (!$prop->isStatic() && !str_starts_with($prop->getDeclaringClass()->getName(), 'PHPUnit_')) {
                $prop->setAccessible(true);
                $prop->setValue($this, null);
            }
        }

        gc_collect_cycles();
    }

    /**
     * @throws Exception
     */
    public static function slugify(string $data): AbstractUnicodeString
    {
        return self::getSlugger()->slug(strtolower($data));
    }

    /**
     * @throws Exception
     */
    public static function getSlugger(): SluggerInterface
    {
        if (empty(self::$slugger)) {

            $slugger = self::getContainer()->get(SluggerInterface::class);

            if ($slugger instanceof SluggerInterface) {
                self::$slugger = $slugger;
            } else {
                throw new Exception('Invalid slugger.');
            }

        }

        return self::$slugger;
    }

}