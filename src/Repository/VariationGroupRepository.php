<?php

namespace App\Repository;

use App\Entity\Product\PurchasableProduct\Variation\VariationGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method VariationGroup|null find($id, $lockMode = null, $lockVersion = null)
 * @method VariationGroup|null findOneBy(array $criteria, array $orderBy = null)
 * @method VariationGroup[]    findAll()
 * @method VariationGroup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VariationGroupRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, VariationGroup::class);
    }

    // /**
    //  * @return VariationGroup[] Returns an array of VariationGroup objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?VariationGroup
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
