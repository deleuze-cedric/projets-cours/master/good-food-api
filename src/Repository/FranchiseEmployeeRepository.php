<?php

namespace App\Repository;

use App\Entity\User\Employee\FranchiseEmployee;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FranchiseEmployee|null find($id, $lockMode = null, $lockVersion = null)
 * @method FranchiseEmployee|null findOneBy(array $criteria, array $orderBy = null)
 * @method FranchiseEmployee[]    findAll()
 * @method FranchiseEmployee[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FranchiseEmployeeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FranchiseEmployee::class);
    }

    // /**
    //  * @return FranchiseEmployee[] Returns an array of FranchiseEmployee objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FranchiseEmployee
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
