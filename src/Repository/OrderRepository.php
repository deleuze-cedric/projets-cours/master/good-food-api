<?php

namespace App\Repository;

use App\Entity\Order\Line\OrderLine;
use App\Entity\Order\Order;
use App\Entity\Product\PurchasableProduct\PurchasableProduct;
use App\Entity\User\Customer\Address;
use App\Entity\User\Customer\Customer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use phpDocumentor\Reflection\Types\Boolean;

/**
 * @method Order|null find($id, $lockMode = null, $lockVersion = null)
 * @method Order|null findOneBy(array $criteria, array $orderBy = null)
 * @method Order[]    findAll()
 * @method Order[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Order::class);
    }

    public function getOrdersUsingAddress(Address $address): array{

        $query = $this->createQueryBuilder('o');
        $query->where('o.billingAddress = :address')
            ->orWhere('o.deliveryAddress = :address')
            ->setParameter('address', $address);

        return $query->getQuery()->getArrayResult();
    }

    // /**
    //  * @return Order[] Returns an array of Order objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Order
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    /**
     * @param Customer $customer
     * @param PurchasableProduct $product
     * @return Order[]
     */
    public function getOrdersWithProductIn(Customer $customer, PurchasableProduct $product): array
    {

        return $this->createQueryBuilder('o')
            ->distinct(true)
            ->innerJoin('o.orderLines', 'ol')
            ->where('o.customer = :customer')
            ->where('ol.product = :product')
            ->setParameter('product', $product)
            ->setParameter('customer', $customer)
            ->getQuery()
            ->getResult();
    }
}
