<?php

namespace App\Repository;

use App\Entity\Product\PurchasableProduct\Food\Extra\ExtraValue;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ExtraValue|null find($id, $lockMode = null, $lockVersion = null)
 * @method ExtraValue|null findOneBy(array $criteria, array $orderBy = null)
 * @method ExtraValue[]    findAll()
 * @method ExtraValue[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExtraValueRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ExtraValue::class);
    }

    // /**
    //  * @return ExtraValue[] Returns an array of ExtraValue objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ExtraValue
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
