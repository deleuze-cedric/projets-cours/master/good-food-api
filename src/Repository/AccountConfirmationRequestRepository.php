<?php

namespace App\Repository;

use App\Entity\User\User;
use App\Entity\User\UserRequest\AccountConfirmationRequest;
use App\Entity\User\UserRequest\NewPasswordRequest;
use DateInterval;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Exception;

/**
 * @method AccountConfirmationRequest|null find($id, $lockMode = null, $lockVersion = null)
 * @method AccountConfirmationRequest|null findOneBy(array $criteria, array $orderBy = null)
 * @method AccountConfirmationRequest[]    findAll()
 * @method AccountConfirmationRequest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AccountConfirmationRequestRepository extends UserRequestRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AccountConfirmationRequest::class);
    }

    public function getDelayBeforeTwoRequest(): int
    {
        return AccountConfirmationRequest::DELAY_IN_MINUTE_BETWEEN_TWO_REQUEST;
    }

    public function getUserFieldName(): string
    {
        return 'customerTarget';
    }
}
