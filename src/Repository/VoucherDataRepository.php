<?php

namespace App\Repository;

use App\Entity\Order\Voucher\VoucherData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method VoucherData|null find($id, $lockMode = null, $lockVersion = null)
 * @method VoucherData|null findOneBy(array $criteria, array $orderBy = null)
 * @method VoucherData[]    findAll()
 * @method VoucherData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VoucherDataRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, VoucherData::class);
    }

    // /**
    //  * @return VoucherData[] Returns an array of VoucherData objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?VoucherData
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
