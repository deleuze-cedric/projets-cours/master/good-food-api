<?php

namespace App\Command\TestBook\Classes\Test;


use App\Command\TestBook\Classes\Model\TestLine;
use ArrayIterator;
use JetBrains\PhpStorm\Pure;

class MobileTest extends TestType
{

    const TEST_TYPE = "Mobile";

    protected function getLinesToWriteInXlsxForCollectionOperation(): iterable
    {
        return new ArrayIterator();
    }

    protected function getLinesToWriteInXlsxForItemOperation(): iterable
    {
        return new ArrayIterator();
    }

    protected function getLinesToWriteInXlsxForSubResourceOperation(): iterable
    {
        return new ArrayIterator();
    }

    protected function getOtherLinesToWriteInXlsx(): iterable
    {
        yield APITest::getTestConnectionToApiLine($this->idTest, self::TEST_TYPE);
        $this->idTest++;

        $line = new TestLine();
        $line->id = $this->idTest;
        $line->task = "Tester la reponsivité des composants web";
        $line->step = "Test interface";
        $line->level = self::TEST_TYPE;
        $line->waitingResult = "Les composants sont correctement affichés et sont parfaitement utilisables";
        $line->resultCode = TestLine::TEST_OK;
        $line->deadline = self::DEADLINE;

        $this->idTest++;
        yield $line;

    }

    public function getTestType(): string
    {
        return self::TEST_TYPE;
    }

}