<?php

namespace App\Command\TestBook\Classes\Test;


use App\Command\TestBook\Classes\Model\TestLine;
use ArrayIterator;
use Generator;
use JetBrains\PhpStorm\Pure;

class APITest extends TestType
{

    const TEST_TYPE = "API";

    const SUFFIX_API = '/api/';

    const ACCESS_DENIED_CODE = 403;
    const NOT_AUTH_CODE = 401;
    const UNPROCESSABLE_ENTITY_CODE = 422;
    const NOT_FOUND_CODE = 404;

    protected function getLinesToWriteFromOperations(
        array $operations,
        bool  $isCollection,
        bool  $isSubResource,
    ): iterable
    {

        foreach ($operations as $name => $operation) {

            if (is_numeric($name) && is_string($operation)) {
                $name = $operation;
                $operation = [];
            }

            $waitingFor = $this->getWaitingForResult($operation, $name, $isCollection, $isSubResource);

            yield $this->getTestLine($operation, $name, $isCollection, $isSubResource, $waitingFor);

            $this->idTest++;

            $method = $this->getOperationMethod($operation, $name);

            switch (strtoupper($method)) {
                case self::GET:

                    if ($isCollection) {
                        $waitingForCollection = $waitingFor . " les données sont correctement filtrées et triées";
                        $line = $this->getTestLine($operation, $name, true, $isSubResource, $waitingForCollection);
                        $line->task .= " avec filtres et/ou tries appliqués";
                    }

                    break;
                case self::POST:
                case self::PUT:
                case self::PATCH:

                    $waitingForUnprocessableEntity = self::UNPROCESSABLE_ENTITY_CODE;
                    $line = $this->getTestLine($operation, $name, true, $isSubResource, $waitingForUnprocessableEntity);
                    $line->task .= " avec données erronées/manquantes";

                    break;
                case self::DELETE:

                    $waitingNotFoundCode = self::NOT_FOUND_CODE;
                    $line = $this->getTestLine($operation, $name, true, $isSubResource, $waitingNotFoundCode);
                    $line->task .= " sur une entité inexistante";

                    break;

            }

            $routeNeedAuth = !in_array($method, $this->entity->getRoutesDontNeedingAuth());

            if ($routeNeedAuth) {

                $line = $this->getTestLine($operation, $name, $isCollection, $isSubResource, self::ACCESS_DENIED_CODE);
                $line->task .= " avec rôle n'ayant pas autorisation d'utiliser la route";
                $this->idTest++;
                yield $line;

                $line = $this->getTestLine($operation, $name, $isCollection, $isSubResource, self::NOT_AUTH_CODE);
                $line->task .= " sans token d'authentification";
                $this->idTest++;
                yield $line;

            }

        }

    }

    protected function getTestLine(array $operation, string $name, bool $isCollection, bool $isSubResource, $waitingFor): TestLine
    {

        $line = new TestLine();
        $line->id = $this->idTest;
        $line->task = $this->getTaskNameFromOperation($operation, $name, $isCollection, $isSubResource);
        $line->step = "Test unitaire";
        $line->waitingResult = $waitingFor;
        $line->level = self::TEST_TYPE;
        $line->resultCode = $this->entity->isRoutesHaveBeenTested() ? TestLine::TEST_OK : TestLine::TEST_NOT_TESTED;
        $line->commentary = $this->entity->isRoutesHaveBeenTested() ? "" : "Fonctionnalité réalisée mais non testée unitairement.";
        $line->deadline = self::DEADLINE;

        return $line;

    }

    protected function getLinesToWriteInXlsxForCollectionOperation(): iterable
    {
        yield from $this->getLinesToWriteFromOperations(
            $this->entity->getCollectionOperations(),
            true,
            false
        );
    }

    protected function getLinesToWriteInXlsxForItemOperation(): iterable
    {
        yield from $this->getLinesToWriteFromOperations(
            $this->entity->getItemOperations(),
            false,
            false
        );
    }

    protected function getLinesToWriteInXlsxForSubResourceOperation(): iterable
    {
        yield from $this->getLinesToWriteFromOperations(
            $this->entity->getSubresourceOperations(),
            false,
            true
        );
    }

    protected function getWaitingForResult(array $operation, string $operationName, bool $isCollectionOperation, bool $isSubResource): string
    {

        if ($isSubResource) {
            return "200 + liste des données associées filtrées";
        }

        $method = $this->getOperationMethod($operation, $operationName);

        return match (strtoupper($method)) {
            self::GET => "200 + " . ($isCollectionOperation ? 'liste des données filtrées' : 'donnée'),
            self::POST => "201 + donnée créée",
            self::PUT, self::PATCH => "200 + donnée mise à jour",
            self::DELETE => "204 + donnée supprimée",
            default => "",
        };

    }

    protected function getTaskNameFromOperation(array $operation, string $operationName, bool $isCollectionOperation, bool $isSubResource): string
    {

        $method = $this->getOperationMethod($operation, $operationName);
        $methodPrefix = strtoupper($method) . " : ";

        $matchingRouteToTaskName = $this->entity->getMatchingRouteToTaskName();

        return match (strtoupper($operationName)) {
            self::GET => $methodPrefix . '"' . ($isCollectionOperation ? $this->getCollectionRoute() : $this->getItemRoute()) . '"',
            self::POST, self::PUT, self::PATCH, self::DELETE => $methodPrefix . '"' . $this->getItemRoute() . '"',
            default => ($isSubResource ? self::GET : ($method != $operationName ? $methodPrefix : '')) . '"' . ($matchingRouteToTaskName[$operationName] ?? $operationName) . '"',
        };

    }

    #[Pure]
    protected function getCollectionRoute(): string
    {
        return !empty($this->entity->getTestableEntityInterface()) ? self::SUFFIX_API . $this->entity->getEntityRouteName() : 'collection';
    }

    #[Pure]
    protected function getItemRoute(): string
    {
        return !empty($this->entity->getTestableEntityInterface()) ? $this->getCollectionRoute() . '/{' . $this->entity->getEntityIdentifierName() . '}' : 'item';
    }

    protected function getOtherLinesToWriteInXlsx(): iterable
    {
        return new ArrayIterator();
    }

    public function getTestType(): string
    {
        return self::TEST_TYPE;
    }

    #[Pure]
    public static function getTestConnectionToApiLine(int $idTest, string $level): TestLine
    {

        $line = new TestLine();
        $line->id = $idTest;
        $line->task = "Tester la communication avec l'api";
        $line->step = "Test unitaire";
        $line->level = $level;
        $line->waitingResult = "Les données sont correctements envoyées/reçues";
        $line->resultCode = TestLine::TEST_OK;
        $line->deadline = self::DEADLINE;

        return $line;
    }

}