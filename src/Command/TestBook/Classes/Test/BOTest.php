<?php

namespace App\Command\TestBook\Classes\Test;


use App\Command\TestBook\Classes\Model\TestLine;
use ArrayIterator;
use JetBrains\PhpStorm\Pure;

class BOTest extends TestType
{

    const TEST_TYPE = "BO";

    protected function getLinesToWriteFromOperations(
        array $operations,
        bool  $isCollection,
    ): iterable
    {

        foreach ($operations as $name => $operation) {

            if (is_numeric($name) && is_string($operation)) {
                $name = $operation;
                $operation = [];
            }

            $line = $this->getTestLine();
            $line->task = $this->getTaskNameFromOperation($name, $isCollection);
            $line->step = $this->getStepFromOperation($name, $isCollection);
            $line->waitingResult = $this->getWaitingForResult($name, $isCollection);
            $line->resultCode = TestLine::TEST_NOT_TESTED;

            $this->idTest++;

            yield $line;


            $method = $this->getOperationMethod($operation, $name);

            switch (strtoupper($method)) {
                case self::GET:

                    if ($isCollection) {
                        $collectionLine = $this->getTestLine();
                        $collectionLine->task = $line->task . " avec application de filtres/tries";
                        $collectionLine->step = $line->step . " choisir des filtres/tries et lancer une recherche";
                        $collectionLine->waitingResult = $line->waitingResult . " et les données sont correctement filtrées et triées";

                        $this->idTest++;
                        yield $collectionLine;
                    }

                    break;
                case self::POST:
                case self::PUT:
                case self::PATCH:

                    $submitLine = $this->getTestLine();
                    $submitLine->task = $line->task . " avec données eronnées";
                    $submitLine->step = $line->step . " (avec des valeurs non valides)";
                    $submitLine->waitingResult = "On reste sur la même page avec affichage des erreurs sous les champs associés";

                    $this->idTest++;
                    yield $submitLine;

                    break;
            }

        }

    }

    protected function getLinesToWriteInXlsxForCollectionOperation(): iterable
    {
        if ($this?->entity?->getTestableEntityInterface()?->requireBOTest()) {

            yield from $this->getLinesToWriteFromOperations(
                $this->entity->getCollectionOperations(),
                true
            );

        }

        return new ArrayIterator();
    }

    protected function getLinesToWriteInXlsxForItemOperation(): iterable
    {
        if ($this?->entity?->getTestableEntityInterface()?->requireBOTest()) {

            yield from $this->getLinesToWriteFromOperations(
                $this->entity->getItemOperations(),
                false
            );

        }

        return new ArrayIterator();
    }

    protected function getLinesToWriteInXlsxForSubResourceOperation(): iterable
    {
        return new ArrayIterator();
    }

    protected function getWaitingForResult(string $operationName, bool $isCollectionOperation): string
    {
        return match (strtoupper($operationName)) {
            self::GET => $isCollectionOperation ? 'La liste est correctement affichée' : 'On accède bien à la page de visualisation des données',
            self::POST => 'Le formulaire est validé et on est redirigé vers le formulaire de modification',
            self::PUT, self::PATCH => 'Le formulaire est validé et on reste sur la page',
            self::DELETE => 'L\'entité est bien supprimé et la liste se met bien à jour',
            default => '',
        };

    }

    #[Pure]
    protected function getTaskNameFromOperation(string $operationName, bool $isCollectionOperation): string
    {
        return match (strtoupper($operationName)) {
            self::GET => $isCollectionOperation ? 'Visualiser la liste des données' : 'Visualiser l\'entité',
            self::POST => 'Créer une nouvelle entité',
            self::PUT => 'Modifier complètement une entité',
            self::PATCH => 'Modifier partiellement une entité',
            self::DELETE => 'Supprimer une ou plusieurs entité',
            default => $this->entity->getMatchingRouteToTaskName()[$operationName] ?? $operationName,
        };
    }

    #[Pure]
    protected function getStepFromOperation(string $operationName, bool $isCollectionOperation): string
    {
        return match (strtoupper($operationName)) {
            self::GET => $isCollectionOperation ? 'Se rendre sur la page de listing' : 'Se rendre sur la page de visualisation',
            self::POST => 'Soumettre le formulaire de création',
            self::PUT, self::PATCH => 'Soumettre le formulaire de modification',
            self::DELETE => 'Cliquer sur le bouton supprimer (sur le listing ou sur le formulaire d\'édition)',
            default => '',
        };
    }

    protected function getOtherLinesToWriteInXlsx(): iterable
    {
        yield APITest::getTestConnectionToApiLine($this->idTest, self::TEST_TYPE);
        $this->idTest++;

        $line = $this->getTestLine();
        $line->task = "Vérifier l'accès au BO";
        $line->step = "Se rendre sur le BO sans être connecté";
        $line->waitingResult = "Redirection vers le login";
        $line->resultCode = TestLine::TEST_OK;
        $this->idTest++;
        yield $line;

        $line = $this->getTestLine();
        $line->task = "Vérifier l'accès au BO";
        $line->step = "Se rendre sur le BO en étant connecté sans la permission d'y accéder";
        $line->waitingResult = "Redirection vers la page d'accueil";
        $line->resultCode = TestLine::TEST_OK;
        $this->idTest++;
        yield $line;

    }

    #[Pure]
    protected function getTestLine(): TestLine
    {

        $line = new TestLine();
        $line->id = $this->idTest;
        $line->level = self::TEST_TYPE;
        $line->deadline = self::DEADLINE;
        $line->resultCode = TestLine::TEST_NOT_TESTED;

        return $line;

    }

    public function getTestType(): string
    {
        return self::TEST_TYPE;
    }

}