<?php


namespace App\EventListener\User\Customer;


use App\Entity\User\Customer\Customer;
use App\Entity\User\UserRequest\AccountConfirmationRequest;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;

class CustomerCreationListener
{
    public function __construct(
        protected EntityManagerInterface $entityManager,
    )
    {
    }

    public function postPersist(Customer $customer, LifecycleEventArgs $event)
    {
        if (!$customer->hasConfirmedAccount()) {
            $customerAccountConfirmRequest = new AccountConfirmationRequest();
            $customerAccountConfirmRequest->setCustomerTarget($customer);
            $this->entityManager->persist($customerAccountConfirmRequest);
            $this->entityManager->flush();
        }
    }
}