<?php


namespace App\EventListener\User\UserRequest\Complete;


use App\Entity\User\UserRequest\AccountConfirmationRequest;
use App\Event\UserRequestCompleteEvent;
use Doctrine\ORM\EntityManagerInterface;

class AccountConfirmationRequestCompleteListener
{

    public function __construct(
        private EntityManagerInterface $entityManager
    )
    {
    }

    public function onCompleteRequest(UserRequestCompleteEvent $event)
    {
        $userRequest = $event->getRequest();

        if ($userRequest instanceof AccountConfirmationRequest) {

            $customer = $userRequest->getCustomerTarget();
            $customer->confirmAccount()
                     ->setActive(true);
            $this->entityManager->persist($customer);

            $this->entityManager->flush();

        }
    }

}