<?php


namespace App\EventListener\User\UserRequest\Complete;


use App\Entity\User\UserRequest\NewPasswordRequest;
use App\Event\UserRequestCompleteEvent;
use App\Service\Tools\Tools;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class NewPasswordRequestCompleteListener
{

    public function __construct(
        private EntityManagerInterface $entityManager,
        private UserPasswordHasherInterface $passwordHasherEncoder
    )
    {
    }

    public function onCompleteRequest(UserRequestCompleteEvent $event)
    {
        $request = $event->getRequest();

        if ($request instanceof NewPasswordRequest) {

            $password = $request->getPassword();

            if (preg_match(Tools::PASSWORD_REGEX, $password)) {

                $customer = $request->getUserTarget()->setPlainPassword($password);
                $this->entityManager->persist($customer);

                $this->entityManager->flush();

            } else {
                throw new BadRequestHttpException(Tools::PASSWORD_ERROR);
            }
        }
    }
}