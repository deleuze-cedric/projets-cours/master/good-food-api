<?php


namespace App\EventListener\User\UserRequest\Creation;


use App\Entity\User\UserRequest\AccountConfirmationRequest;
use App\Service\Tools\SendMail;
use App\Service\Tools\Tools;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class AccountConfirmationRequestCreationListener
{

    public function __construct(
        private SendMail $mailer,
        private Tools $tools
    )
    {
    }

    /**
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     * @throws RuntimeError
     * @throws LoaderError
     */
    public function postPersist(AccountConfirmationRequest $userRequest, LifecycleEventArgs $event)
    {
        $this->mailer->sendMail(
            'email/account_confirmation.html.twig',
            [
                "user" => $userRequest->getCustomerTarget(),
                "routeConfirmAccount" => $this->tools->webSiteConfirmAccountUrl . '/' . $userRequest->getId()
            ],
            'Confirmation de création de votre compte',
            null,
            [
                $userRequest->getCustomerTarget()->getEmail()
            ]
        );
    }
}