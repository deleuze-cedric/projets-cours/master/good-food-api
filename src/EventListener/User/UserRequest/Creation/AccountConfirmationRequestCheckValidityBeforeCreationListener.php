<?php


namespace App\EventListener\User\UserRequest\Creation;


use App\Entity\User\Customer\Customer;
use App\Entity\User\UserRequest\AccountConfirmationRequest;
use App\Event\UserRequestCheckValidityBeforeCreationEvent;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

class AccountConfirmationRequestCheckValidityBeforeCreationListener
{
	const ALREADY_CONFIRMED_ACCOUNT = "The user has already confirmed his account.";
	const NOT_CUSTOMER = "The user send must be an instance of Customer.";

	public function onCheckValidityBeforeCreation(UserRequestCheckValidityBeforeCreationEvent $event)
	{
		$request = $event->getRequest();

		if ($request instanceof AccountConfirmationRequest) {

			$user = $request->getUserTarget();

			if ($user instanceof Customer) {
				if ($user->hasConfirmedAccount()) {
					throw new BadRequestException(self::ALREADY_CONFIRMED_ACCOUNT);
				}
			} else {
				throw new UnprocessableEntityHttpException(self::NOT_CUSTOMER);
			}

		}
	}
}