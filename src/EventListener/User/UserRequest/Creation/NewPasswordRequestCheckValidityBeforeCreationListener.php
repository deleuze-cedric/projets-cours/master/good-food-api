<?php


namespace App\EventListener\User\UserRequest\Creation;


use App\Event\UserRequestCheckValidityBeforeCreationEvent;

class NewPasswordRequestCheckValidityBeforeCreationListener
{
    public function onCheckValidityBeforeCreation(UserRequestCheckValidityBeforeCreationEvent $event)
    {
    }
}