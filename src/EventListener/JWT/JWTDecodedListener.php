<?php


namespace App\EventListener\JWT;


use App\Entity\User\User;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTDecodedEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class JWTDecodedListener
{
    public function __construct(
        private RequestStack $request,
        private EntityManagerInterface $entityManager
    )
    {
    }

    /**
     * @param JWTDecodedEvent $event
     *
     * @return void
     */
    public function onJWTDecoded(JWTDecodedEvent $event)
    {
        $payload = $event->getPayload();

        $repository = $this->entityManager->getRepository(User::class);
        /**
         * @var User $user
         */
        $user = $repository->find($payload['uuid']);

        if (empty($user) || empty($user->isActive()) || !empty($user->isDeleted())) {
            $event->markAsInvalid();
        }
    }
}