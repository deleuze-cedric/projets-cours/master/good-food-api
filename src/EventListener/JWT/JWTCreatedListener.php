<?php


namespace App\EventListener\JWT;


use App\Entity\Order\Order;
use App\Entity\Order\Status\OrderStatus;
use App\Entity\User\Customer\Customer;
use App\Entity\User\Employee\Employee;
use App\Entity\User\Employee\FranchiseEmployee;
use App\Entity\User\User;
use App\Repository\OrderRepository;
use App\Repository\OrderStatusRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class JWTCreatedListener
{
    /**
     * @param JWTCreatedEvent $event
     */
    public function onJWTCreated(JWTCreatedEvent $event)
    {
        $payload = $event->getData();
        $user = $event->getUser();

        if ($user instanceof User) {
            $payload['uuid'] = $user->getUuid();
            $payload['lastname'] = $user->getLastname();
            $payload['firstname'] = $user->getFirstname();
            $payload['active'] = $user->isActive();
            $payload['deleted'] = $user->isDeleted();

            if ($user instanceof FranchiseEmployee) {
                $payload['restaurant'] = $user->getRestaurant();
            }

        }

        $event->setData($payload);

        $header = $event->getHeader();
        $header['cty'] = 'JWT';

        $event->setHeader($header);
    }

}