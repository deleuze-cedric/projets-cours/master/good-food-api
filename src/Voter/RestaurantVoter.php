<?php

namespace App\Voter;

use App\Entity\Restaurant\Restaurant;
use Prophecy\Argument\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

class RestaurantVoter extends Voter
{

    const GRANT_USER_IS_LEADER = 'USER_IS_LEADER';

    public function __construct(private Security $security)
    {
    }

    protected function supports($attribute, $subject): bool
    {
        $supportsAttribute = $attribute == self::GRANT_USER_IS_LEADER;
        $supportsSubject = $subject instanceof Restaurant;

        return $supportsAttribute && $supportsSubject;
    }

    /**
     * @param string         $attribute
     * @param Restaurant     $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute(string $attribute, $subject, $token): bool
    {
        $user = $this->security->getUser();

        // L'utilisateur en cours doit être un des chef du restaurant
        if ($attribute == self::GRANT_USER_IS_LEADER) {
            foreach ($subject->getLeaders() as $leader) {
                if ($leader->getUserIdentifier() == $user->getUserIdentifier()) {
                    return true;
                }
            }
        }

        return false;
    }

}