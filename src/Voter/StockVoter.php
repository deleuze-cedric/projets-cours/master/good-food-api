<?php

namespace App\Voter;

use App\Entity\Product\Stock\Stock;
use App\Entity\User\Employee\FranchiseEmployee;
use App\Entity\User\Employee\FranchisorEmployee;
use Prophecy\Argument\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

class StockVoter extends Voter
{

    const IS_AUTHORIZED_TO_MANAGE_STOCK = 'MANAGE_STOCK';

    public function __construct(private Security $security)
    {
    }

    protected function supports($attribute, $subject): bool
    {
        $supportsAttribute = $attribute == self::IS_AUTHORIZED_TO_MANAGE_STOCK;
        $supportsSubject = $subject instanceof Stock;

        return $supportsAttribute && $supportsSubject;
    }

    /**
     * @param string         $attribute
     * @param Stock          $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute(string $attribute, $subject, $token): bool
    {
        $user = $this->security->getUser();
        $isAuthorized = false;

        if ($attribute == self::IS_AUTHORIZED_TO_MANAGE_STOCK) {

            // Admin ou super admin
            if ($this->security->isGranted(FranchisorEmployee::ROLE_ADMIN)) {
                $isAuthorized = true;
            } else {

                // Logisticien ou chef du restaurant
                return $this->security->isGranted(FranchiseEmployee::ROLE_LOGISTICIAN) &&
                       $user instanceof FranchiseEmployee &&
                       $user->getRestaurant() === $subject->getRestaurant();
            }
        }

        return $isAuthorized;
    }

}