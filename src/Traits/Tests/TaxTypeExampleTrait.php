<?php

namespace App\Traits\Tests;

use App\Classes\CustomApiTestCase;
use App\Entity\Country\Tax\TaxType;
use App\Tests\unit\TaxTypeTest;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;

trait TaxTypeExampleTrait
{

    private ?TaxType $taxTypeExample;

    private ?string $taxTypeExampleIndex;

    private ?string $taxTypeExampleItemIRI;

    /**
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function loadTaxType()
    {
        $this->taxTypeExampleIndex = TaxTypeTest::TAX_TYPE_EXAMPLE;
        $this->taxTypeExample = TaxTypeTest::getTaxTypeFixtures()[$this->taxTypeExampleIndex];
        $this->taxTypeExampleItemIRI = CustomApiTestCase::replaceParamsRoute(TaxTypeTest::TAX_TYPE_ITEM_ROUTE, ['slug' => $this->taxTypeExample->getSlug()]);
    }

}