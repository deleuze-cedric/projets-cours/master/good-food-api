<?php

namespace App\Traits\Tests;

use App\Classes\CustomApiTestCase;
use App\Entity\User\SuperAdmin\SuperAdmin;
use App\Tests\unit\SuperAdminTest;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

trait AdminExampleTrait
{
    use UserExampleTrait;

    private ?string $adminExampleIndex;
    private ?SuperAdmin $adminExample;
    private ?string $adminExampleUsername;
    private ?string $adminExamplePassword;
    private ?string $adminExampleToken;
    private ?string $adminExampleItemIRI;

    /**
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function loadAdmin()
    {
        $this->adminExampleIndex = CustomApiTestCase::CEDRIC_ADMIN;
        $this->adminExample = CustomApiTestCase::getUserFixturesClean()[$this->adminExampleIndex];
        $this->adminExampleUsername = $this->adminExample->getUserIdentifier();
        $this->adminExamplePassword = CustomApiTestCase::PASSWORDS[$this->adminExampleIndex];
        $this->adminExampleToken = $this->getTokenForUser($this->adminExampleUsername, $this->adminExamplePassword);
        $this->adminExampleItemIRI = CustomApiTestCase::replaceParamsRoute(SuperAdminTest::SUPER_ADMIN_ITEM_ROUTE, ['uuid' => $this->adminExample->getUuid()]);
    }
}