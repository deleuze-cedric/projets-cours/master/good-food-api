<?php

namespace App\Traits\Tests;

use App\Classes\CustomApiTestCase;
use App\Entity\User\Comment\Comment;
use App\Tests\unit\CommentTest;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;

trait CommentExampleTrait
{
    private ?string $commentExampleIndex;
    private ?Comment $commentExample;
    private ?string $commentExampleItemIRI;

    /**
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function loadComment()
    {
        $this->commentExampleIndex = CommentTest::COMMENT_EXAMPLE;
        $this->commentExample = CommentTest::getCommentFixtures()[$this->commentExampleIndex];
        $this->commentExampleItemIRI = CustomApiTestCase::replaceParamsRoute(CommentTest::COMMENT_ROUTE, ['id' => $this->commentExample->getId()]);
    }
}