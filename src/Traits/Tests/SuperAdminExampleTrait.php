<?php

namespace App\Traits\Tests;

use App\Classes\CustomApiTestCase;
use App\Entity\User\SuperAdmin\SuperAdmin;
use App\Tests\unit\SuperAdminTest;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

trait SuperAdminExampleTrait
{
    use UserExampleTrait;

    private ?string $superAdminExampleIndex;
    private ?SuperAdmin $superAdminExample;
    private ?string $superAdminExampleUsername;
    private ?string $superAdminExamplePassword;
    private ?string $superAdminExampleToken;
    private ?string $superAdminExampleItemIRI;

    /**
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function loadSuperAdmin()
    {
        $this->superAdminExampleIndex = CustomApiTestCase::CEDRIC_ADMIN;
        $this->superAdminExample = CustomApiTestCase::getUserFixturesClean()[$this->superAdminExampleIndex];
        $this->superAdminExampleUsername = $this->superAdminExample->getUserIdentifier();
        $this->superAdminExamplePassword = CustomApiTestCase::PASSWORDS[$this->superAdminExampleIndex];
        $this->superAdminExampleToken = $this->getTokenForUser($this->superAdminExampleUsername, $this->superAdminExamplePassword);
        $this->superAdminExampleItemIRI = CustomApiTestCase::replaceParamsRoute(SuperAdminTest::SUPER_ADMIN_ITEM_ROUTE, ['uuid' => $this->superAdminExample->getUuid()]);
    }

}