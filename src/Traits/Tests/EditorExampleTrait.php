<?php

namespace App\Traits\Tests;

use App\Classes\CustomApiTestCase;
use App\Entity\User\Employee\FranchisorEmployee;
use App\Tests\unit\FranchisorEmployeeTest;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

trait EditorExampleTrait
{
    use UserExampleTrait;

    private ?string $editorExampleIndex;
    private ?FranchisorEmployee $editorExample;
    private ?string $editorExampleUsername;
    private ?string $editorExamplePassword;
    private ?string $editorExampleToken;
    private ?string $editorExampleItemIRI;

    /**
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function loadEditor()
    {
        $this->editorExampleIndex = CustomApiTestCase::ROLE_EDITOR;
        $this->editorExample = CustomApiTestCase::getUserFixturesClean()[$this->editorExampleIndex];
        $this->editorExampleUsername = $this->editorExample->getUserIdentifier();
        $this->editorExamplePassword = CustomApiTestCase::PASSWORDS[$this->editorExampleIndex];
        $this->editorExampleToken = $this->getTokenForUser($this->editorExampleUsername, $this->editorExamplePassword);
        $this->editorExampleItemIRI = CustomApiTestCase::replaceParamsRoute(FranchisorEmployeeTest::FRANCHISOR_EMPLOYEE_ITEM_ROUTE, ['uuid' => $this->editorExample->getUuid()]);
    }
}