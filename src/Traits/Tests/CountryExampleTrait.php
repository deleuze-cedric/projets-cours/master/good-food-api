<?php

namespace App\Traits\Tests;

use App\Classes\CustomApiTestCase;
use App\Entity\Country\Country;
use App\Tests\unit\CountryTest;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;

trait CountryExampleTrait
{

    private ?string $countryExampleIndex;

    private ?Country $countryExample;

    private ?string $countryExampleItemIRI;

    /**
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function loadCountry()
    {
        $this->countryExampleIndex = CountryTest::COUNTRY_EXAMPLE;
        $this->countryExample = CountryTest::getCountryFixtures()[$this->countryExampleIndex];
        $this->countryExampleItemIRI = CustomApiTestCase::replaceParamsRoute(CountryTest::COUNTRY_ITEM_ROUTE, ['isoCode' => $this->countryExample->getIsoCode()]);
    }

}