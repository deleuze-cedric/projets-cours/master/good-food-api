<?php

namespace App\Traits\Tests;

use App\Classes\CustomApiTestCase;
use App\Entity\User\Customer\Address;
use App\Tests\unit\AddressTest;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;

trait AddressExampleTrait
{
    private ?string $addressExampleIndex;
    private ?Address $addressExample;
    private ?string $addressExampleItemIRI;

    /**
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function loadAddress()
    {
        $this->addressExampleIndex = AddressTest::ADDRESS_EXAMPLE;
        $this->addressExample = AddressTest::getAddressFixtures()[$this->addressExampleIndex];
        $this->addressExampleItemIRI = CustomApiTestCase::replaceParamsRoute(AddressTest::ADDRESS_ITEM_ROUTE, ['slug' => $this->addressExample->getSlug()]);
    }
}