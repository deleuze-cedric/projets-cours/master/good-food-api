<?php

namespace App\Traits\Tests;

use App\Classes\CustomApiTestCase;
use App\Entity\Country\Currency;
use App\Tests\unit\CurrencyTest;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;

trait CurrencyExampleTrait
{

    private ?string $currencyExampleIndex;

    private ?Currency $currencyExample;

    private ?string $currencyExampleItemIRI;

    /**
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function loadCurrency()
    {
        $this->currencyExampleIndex = CurrencyTest::CURRENCY_EXAMPLE;
        $this->currencyExample = CurrencyTest::getCurrencyFixtures()[$this->currencyExampleIndex];
        $this->currencyExampleItemIRI = CustomApiTestCase::replaceParamsRoute(CurrencyTest::CURRENCY_ITEM_ROUTE, ['isoCode' => $this->currencyExample->getIsoCode()]);
    }

}