<?php


namespace App\Traits\Fields;


use ApiPlatform\Core\Annotation\ApiProperty;
use App\Entity\Country\Country;
use App\Entity\Restaurant\Restaurant;
use App\Entity\User\Customer\Address;
use App\Service\Tools\Tools;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Regex;

trait AddressTrait
{

    #[Column(type: "text", nullable: true)]
    #[Groups([
        'address',
        "restaurant",
        "franchise_employee:read",
        "customer:read",
        'order:read',
                'reservation:read',
    ])]
    private ?string $additionalAddress;

    #[Column(type: "string", length: 64)]
    #[NotBlank(message: "La ville doit être renseigné (Ex: Paris, Lyon, ...).")]
    #[Length(
        min: 1,
        max: 64,
        minMessage: "La ville doit faire au moins {{ limit }} caractère.",
        maxMessage: "La ville doit faire maximum {{ limit }} caractères.",
    )]
    #[Groups([
        'address',
        "restaurant",
        "franchise_employee:read",
        "customer:read",
        'order:read',
                'reservation:read',
    ])]
    private string $city;

    #[ManyToOne(targetEntity: Country::class)]
    #[JoinColumn(referencedColumnName: "iso_code", nullable: false)]
    #[NotNull(message: "Le pays de l'adresse doit être renseigné")]
    #[ApiProperty(writableLink: false)]
    #[Groups([
        'address',
        "restaurant",
        "franchise_employee:read",
        "customer:read",
        'order:read',
                'reservation:read',
    ])]
    private Country $country;

    #[Column(type: "string", length: 30)]
    #[NotBlank(message: Tools::PHONE_ERROR)]
    #[Regex(Tools::PHONE_REGEX, message: Tools::PHONE_ERROR)]
    #[Groups([
        'address',
        'restaurant',
        "franchise_employee:read",
        "customer:read",
        'order:read',
                'reservation:read',
    ])]
    private string $phoneNumber;

    #[Column(type: "string", length: 100)]
    #[NotBlank(message: "La rue doit être renseigné (Ex: 15 grande rue, Rue du presbytère, ...).")]
    #[Length(
        min: 1,
        max: 100,
        minMessage: "La rue doit faire au moins {{ limit }} caractère.",
        maxMessage: "La rue doit faire maximum {{ limit }} caractères.",
    )]
    #[Groups([
        'address',
        'restaurant',
        "franchise_employee:read",
        "customer:read",
        'order:read',
                'reservation:read',
    ])]
    private string $street;

    #[Column(type: "string", length: 12)]
    #[NotBlank(message: "Le code postal doit être renseigné (Ex: 18650, 25000, ...).")]
    #[Length(
        min: 2,
        max: 12,
        minMessage: "Le code postal doit faire au moins {{ limit }} caractère.",
        maxMessage: "Le code postal doit faire maximum {{ limit }} caractères.",
    )]
    #[Groups([
        'address',
        'restaurant',
        "franchise_employee:read",
        "customer:read",
        'order:read',
                'reservation:read',
    ])]
    private string $zipCode;

    public function getAdditionalAddress(): ?string
    {
        return $this->additionalAddress ?? null;
    }

    public function setAdditionalAddress(?string $additionalAddress): self
    {
        $this->additionalAddress = $additionalAddress;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return Country
     */
    public function getCountry(): Country
    {
        return $this->country;
    }

    /**
     * @param Country $country
     *
     * @return Restaurant|Address|AddressTrait
     */
    public function setCountry(Country $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function setZipCode(string $zipCode): self
    {
        $this->zipCode = $zipCode;

        return $this;
    }

}