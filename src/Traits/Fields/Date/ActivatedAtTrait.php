<?php


namespace App\Traits\Fields\Date;

use DateTimeImmutable;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\PrePersist;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Trait ActivatedAtTrait
 *
 * @package App\Traits\Fields
 */
#[HasLifecycleCallbacks()]
trait ActivatedAtTrait
{

    #[Column(type: "datetime_immutable", nullable: true)]
    private ?DateTimeImmutable $activatedAt;

    #[PrePersist()]
    public function activeOnCreation()
    {
        if ($this->autoActiveOnCreation()) {
            $this->setActive(true);
        }
    }

    #[Groups([
        "active:write",
        "supplier:write",
        "variation_value:write",
        "variation_group:write",
        "tax:write",
        "tax_type:write",
        "category:write",
        "country:write",
        "currency:write",
        "restaurant:write",
        "extra_group:write",
        "extra_value:write",
        "carrier:write",
        "payment_method:write",
        'order:read',
        'reservation:read',
        'order_status:read',
    ])]
    public function setActive(bool $active)
    {
        $this->activatedAt = $active ? ($this->activatedAt ?? new DateTimeImmutable()) : null;
    }

    abstract protected function autoActiveOnCreation(): bool;

    public function getActivatedAt(): ?DateTimeImmutable
    {
        return $this->activatedAt;
    }

    #[Groups([
        "active:read",
        "product:read",
        "supplier:read",
        "variation_value:read",
        "variation_group:read",
        "tax:read",
        "tax_type:read",
        "user:read",
        "category:read",
        "country:read",
        "currency:read",
        "restaurant:read",
        "extra_group:read",
        "extra_value:read",
        "payment_method:read",
        'voucher:read',
        'voucher_data:read',
        'carrier_data:read',
        'carrier:read',
        'order:read',
        'order_line:read',
    ])]
    public function isActive(): bool
    {
        return !empty($this->activatedAt);
    }

}