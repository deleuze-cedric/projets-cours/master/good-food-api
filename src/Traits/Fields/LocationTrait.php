<?php


namespace App\Traits\Fields;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\LessThan;
use Symfony\Component\Validator\Constraints\NotNull;

trait LocationTrait
{

    #[ORM\Column(type: "float")]
    #[NotNull(message: "La latitude doit être renseigné.")]
    #[GreaterThan(
        value: -90,
        message: "La latitude doit être comprise entre -90 et -90"
    )]
    #[LessThan(
        value: 90,
        message: "La latitude doit être comprise entre -90 et -90"
    )]
    #[Groups([
        'location',
        "restaurant",
        'order:read',
        'reservation:read',
    ])]
    private float $latitude;

    #[ORM\Column(type: "float")]
    #[NotNull(message: "La longitude doit être renseigné.")]
    #[GreaterThan(
        value: -180,
        message: "La longitude doit être comprise entre -180 et -180"
    )]
    #[LessThan(
        value: 180,
        message: "La longitude doit être comprise entre -180 et -180"
    )]
    #[Groups([
        'location',
        "restaurant",
        'order:read',
        'reservation:read',
    ])]
    private float $longitude;

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function setLatitude(float $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function setLongitude(float $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

}