<?php


namespace App\Traits\Fields;

use Doctrine\ORM\Mapping\Column;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Trait EmailTrait
 *
 * @package App\Traits\Fields
 */
trait EmailTrait
{

    #[Column(type: "string", length: 255)]
    #[Email(message: "Le format de l'adresse email n'est pas valide")]
    #[NotBlank(message: "L'email doit être renseigné")]
    #[Groups([
        "email",
        "user",
        'voucher:read',
        'voucher_data:read',
        'comment:read',
        'order:read',
        'order_line:read',
        'restaurant:read',
        'reservation:read',
    ])]
    private string $email;

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

}