<?php


namespace App\Traits\Fields\Identifiers;

use ApiPlatform\Core\Annotation\ApiProperty;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Gedmo\Mapping\Annotation\Slug;
use Symfony\Component\Serializer\Annotation\Groups;

trait SlugNameTrait
{

    #[Id()]
    #[GeneratedValue()]
    #[Column(type: "integer")]
    #[ApiProperty(identifier: false)]
    private int $id;

    #[Slug(fields: ["name"])]
    #[Column(type: "string", length: 255, unique: true)]
    #[Groups([
        "slug:read",
        "supplier:read",
        "variation_value:read",
        "variation_group:read",
        "tax:read",
        "tax_type:read",
        "address:read",
        "category:read",
        "restaurant:read",
        "extra_group:read",
        "extra_value:read",
        "payment_method:read",
        'carrier:read',
        'carrier_data:read',
        'order:read',
        'reservation:read',
        'order_line:read',
        'order_status:read',
    ])]
    #[ApiProperty(identifier: true)]
    private string $slug;

    /**
     * @return ?int
     */
    public function getId(): ?int
    {
        return $this->id ?? null;
    }

    public function getSlug(): ?string
    {
        return $this->slug ?? null;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getEntityIdentifierName(): string
    {
        return "slug";
    }

}