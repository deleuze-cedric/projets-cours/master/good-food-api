<?php


namespace App\Traits\Fields\Identifiers;

use ApiPlatform\Core\Annotation\ApiProperty;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Id;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

trait IsoCodeTrait
{

    #[Id]
    #[Column(type: "string", length: 10)]
    #[NotBlank(message: "Le code iso doit être renseigné et unique.")]
    #[Length(
        min: 3,
        max: 10,
        minMessage: "Le code iso doit faire au moins {{limit}} caractères.",
        maxMessage: "Le code iso doit faire moins de {{limit}} caractères."
    )]
    #[Groups([
        "iso_code:read",
        "iso_code:post",
        "tax:read",
        "country:read",
        "currency:read",
        "restaurant:read",
        "franchise_employee:read",
        "customer:read",
        "address:read",
        'voucher:read',
        'voucher_data:read',
        'carrier_data:read',
        'carrier:read',
        'order:read',
        'order_line:read',
        'purchasable_product:read',
    ])]
    #[ApiProperty(identifier: true)]
    private string $isoCode;

    public function getIsoCode(): ?string
    {
        return $this->isoCode ?? null;
    }

    public function setIsoCode(string $isoCode): self
    {
        $this->isoCode = $isoCode;

        return $this;
    }

    public function getEntityIdentifierName(): string
    {
        return "isoCode";
    }

}