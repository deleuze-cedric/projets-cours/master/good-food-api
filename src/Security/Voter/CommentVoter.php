<?php

namespace App\Security\Voter;

use App\Repository\OrderRepository;
use phpDocumentor\Reflection\Types\Boolean;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Entity\User\Comment\Comment;
use App\Entity\User\Customer\Customer;

class CommentVoter extends Voter
{

    public function __construct(
        private OrderRepository $orderRepository
    )
    {
    }

    protected function supports(string $attribute, $subject): bool
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, ['COMMENT_CREATE', 'DELETE_COMMENT'])
            && $subject instanceof Comment;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        if ($subject instanceof Comment) {

            $user = $token->getUser();

            // if the user is anonymous, do not grant access
            if (!$user instanceof UserInterface) {
                return false;
            }

            $customer = $subject->getCustomer();

            if(!empty($subject->getRestaurant())){
                return true;
            }

            $product = $subject->getPurchasableProduct();

            if(empty($product)){
               return false;
            }

            // ... (check conditions and return true to grant permission) ...
            switch ($attribute) {
                case 'COMMENT_CREATE':

                    $orders = $this->orderRepository->getOrdersWithProductIn($customer, $product);

                    return !empty($orders);

                    // logic to determine if the user can EDIT
                    // return true or false
                case 'DELETE_COMMENT':

                    $orders = $this->orderRepository->getOrdersWithProductIn($customer, $product);

                    return !empty($orders);
                    // logic to determine if the user can VIEW
                    // return true or false
            }

        }

        return false;
    }
}
