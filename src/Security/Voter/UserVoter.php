<?php

namespace App\Security\Voter;

use App\Entity\User\Employee\FranchisorEmployee;
use App\Entity\User\User;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class UserVoter extends Voter
{

    const UPDATE_ROLES_SUPPORT = 'UPDATE_ROLES';

    public function __construct(
        private Security     $security,
        private RequestStack $request
    )
    {
    }

    protected function supports(string $attribute, $subject): bool
    {
        return $attribute == self::UPDATE_ROLES_SUPPORT && $subject instanceof User;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface || !$subject instanceof User) {
            return false;
        }

        $content = $this->request->getCurrentRequest()->getContent();
        $data = json_decode($content, true);


        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case self::UPDATE_ROLES_SUPPORT:
                $roles = $data['roles'] ?? [];

// Un rôle doit forcément être renseigné
                if (!empty($roles)) {

                    foreach ($roles as $role) {
                        if (in_array($role, $subject->getAvailableRoles())) {

                            if (
                                $this->security->isGranted($role) ||
                                $this->security->isGranted(FranchisorEmployee::ROLE_HR)
                            ) {
                                return true;
                            } else {
                                throw new AccessDeniedHttpException('You do not have the right access to set te role "' . $role . '" to an other user');
                            }
                        } else {
                            throw new BadRequestHttpException('The role "' . $role . '" is not available for entity of type "' . get_class($subject));
                        }
                    }
                }

                break;

        }


        return false;
    }

}
