<?php


namespace App\Service\Tools;


class PasswordGenerator
{

    const DEFAULT_LOWER_AVAILABLE = 'abcdefghijklmnopqrstuvwxyz';
    const DEFAULT_UPPER_AVAILABLE = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    const DEFAULT_NUMBER_AVAILABLE = '1234567890';
    const DEFAULT_SPE_CHAR_AVAILABLE = '@&"\'()\!-<>,;:=+.?';
//    const DEFAULT_SPE_CHAR_AVAILABLE = '@&é"\'§è()\!çà-<>,;:=+.?';

    public function generate(
        int $passwordSize = 20,
        int $nbLower = 2,
        int $nbUpper = 2,
        int $nbNumberAvailable = 2,
        int $nbSpeCaracAvailable = 2,
        string $lowerAvailable = self::DEFAULT_LOWER_AVAILABLE,
        string $upperAvailable = self::DEFAULT_UPPER_AVAILABLE,
        string $numberAvailable = self::DEFAULT_NUMBER_AVAILABLE,
        string $speCharAvailable = self::DEFAULT_SPE_CHAR_AVAILABLE
    ): string
    {
        $allChars = '';
        $pass = array();

        $charsInPassword = [
            $lowerAvailable => $nbLower,
            $upperAvailable => $nbUpper,
            $numberAvailable => $nbNumberAvailable,
            $speCharAvailable => $nbSpeCaracAvailable,
        ];

        //        Génération des caractères obligatoires
        foreach ($charsInPassword as $chars => $nbChar) {
            $charsArray = str_split($chars);
//            Tant que l'on a pas renseigné le nombre minimum de chaque type de carac on en ajoute au mdp
            for ($i = 0; $i < $nbChar; $i++) {
//            Caractère aléatoire dans la liste
                $posChar = rand(0, count($charsArray) - 1);
//               Ajout du caractère
                $pass[] = $charsArray[$posChar];
            }
//            On concat tout les types de caractères
            $allChars .= $chars;
        }

        $allChars = str_split($allChars);

//        tant que l'on a pas assez de caractères pour avoir le nb de carac demandé
        while (count($pass) < $passwordSize) {

//            Position du caractère aléatoire
            $posChar = rand(0, count($allChars) - 1);

//            Définissions de l'endroit où l'on va ajouter le caractère
            $slice = rand(0, count($pass));

            $newChar = array(count($pass) => $allChars[$posChar]);

            //            Ajout à l'endroit prévu du nouveau caractère
            $pass = array_slice($pass, 0, $slice, true) +
                $newChar +
                array_slice($pass, $slice, count($pass), true);

        }

        return implode($pass);
    }

}