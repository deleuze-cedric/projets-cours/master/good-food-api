<?php


namespace App\Service\Tools;


use App\Exceptions\EnvVariableNotFoundExcpetion;
use JetBrains\PhpStorm\Pure;

class Tools
{

    /**
     * (?=.*[a-z]) => match 1 caractère minuscule
     * (?=.*[A-Z]) => match 1 caractère majuscule
     * (?=.*[\d]) => match 1 caractère numérique
     * (?=.*[@&"'\(\)\\\/!\-<>,;:=\+\.\?]) => match 1 caractère spécial
     * ((?=.*[a-z])(?=.*[A-Z].*)(?=.*[\d])(?=.*[@&\(\)\\\/!\-<>,;:=\+\.\?])){2,} => match chaque test précédemment
     * expliqué au moins deux fois
     * ((?=.*[a-z])(?=.*[A-Z].*)(?=.*[\d])(?=.*[@&'\(\)\\\/!\-<>,;:=\+\.\?])){2,}.{8,} => check des caratères + min 8
     * caractères
     */
    const PASSWORD_REGEX = "/^((?=.*[a-z])(?=.*[A-Z].*)(?=.*[\d])(?=.*[@&\(\)\/\!\-<>,;:=\+\.\?])){2,}.{8,}$/";
    const PASSWORD_ERROR = "Le mot de passe doit comprendre (2 majuscules, 2 minuscules, 2 chiffres, 2 caractères dans la liste suivante [@&()\/!-<>,;:=+.?]).";

    /**
     * https://stackoverflow.com/questions/6478875/regular-expression-matching-e-164-formatted-phone-numbers
     */
    const PHONE_REGEX = "/^\+?[1-9]\d{1,14}$/";
    const PHONE_ERROR = "Le numéro de téléphone doit être au format e164 (+33912457823)";

    const UUID_REGEX = '([0-9a-f]{8}-([0-9a-f]{4}-){3}[0-9a-f]{12})';

    /**
     * /^
     *    [a-z0-9]+   # One or more repetition of given characters
     *    (?:         # A non-capture group.
     *    -           # A hyphen
     *    [a-z0-9]+   # One or more repetition of given characters
     *    )*          # Zero or more repetition of previous group
     *    $/
     */
    const SLUG_REGEX = '([a-z0-9]+(?:-[a-z0-9]+)*$)';

    const ISO_CODE_REGEX = '\w{3,10}';

    public string $webSiteConfirmAccountUrl;

    public string $webSiteResetPasswordUrl;

    public string $webSiteUrl;

    /**
     * @throws EnvVariableNotFoundExcpetion
     */
    public function __construct()
    {
        if (empty($_SERVER['WEB_SITE_URL'])) {
            throw new EnvVariableNotFoundExcpetion('WEB_SITE_URL');
        }

        if (empty($_SERVER['CONFIRM_ACCOUNT_WEB_ROUTE'])) {
            throw new EnvVariableNotFoundExcpetion('CONFIRM_ACCOUNT_WEB_ROUTE');
        }

        if (empty($_SERVER['CHANGE_PASSWORD_WEB_ROUTE'])) {
            throw new EnvVariableNotFoundExcpetion('CHANGE_PASSWORD_WEB_ROUTE');
        }

        $this->webSiteUrl = $_SERVER['WEB_SITE_URL'];

        $this->webSiteConfirmAccountUrl = $this->webSiteUrl . $_SERVER['CONFIRM_ACCOUNT_WEB_ROUTE'];

        $this->webSiteResetPasswordUrl = $this->webSiteUrl . $_SERVER['CHANGE_PASSWORD_WEB_ROUTE'];

    }

    public static function base64_url_decode(string $input): string
    {
        return base64_decode(strtr($input, '._-', '+/='));
    }

    public static function base64_url_encode(string $input): string
    {
        return strtr(base64_encode($input), '+/=', '._-');
    }

    public static function dumpMemoryUsage()
    {
        $mem_usage = memory_get_usage(true);

        if ($mem_usage < 1024) {
            dump($mem_usage . " bytes");
        } else {
            if ($mem_usage < 1048576) {
                dump(round($mem_usage / 1024, 2) . " kilobytes");
            } else {
                dump(round($mem_usage / 1048576, 2) . " megabytes");
            }
        }
    }

    public static function formatCurrency(?float $currency): string
    {
        return number_format($currency ?? 0, 6);
    }

    public static function formatPercentage(?float $percent): string
    {
        return number_format($percent ?? 0, 3);
    }

    public static function formatPrice(?float $price): string
    {
        return number_format($price ?? 0, 2);
    }

    public function getUsedTraits($classInstance): array|bool
    {
        $parentClasses = class_parents($classInstance);
        $traits = class_uses($classInstance);

        foreach ($parentClasses as $parentClass) {
            $traits = array_merge($traits, class_uses($parentClass));
        }

        return $traits ?? [];
    }

    public function parseHTMLToText(string $html): string
    {
        return preg_replace("/\n\s+/", "\n", rtrim(html_entity_decode(strip_tags($html))));
    }

    #[Pure] public function urlCall(): string
    {
        return $this->urlServer() . $_SERVER['REQUEST_URI'];
    }

    public function urlServer(): string
    {
        if (isset($_SERVER['HTTPS'])) {
            $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
        } else {
            $protocol = 'http';
        }

        return $protocol . "://" . $_SERVER['HTTP_HOST'];
    }

}