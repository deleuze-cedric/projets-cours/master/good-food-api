<?php

namespace App\Service\User\Customer;

use App\Entity\User\Customer\Customer;
use App\Exceptions\EnvVariableNotFoundExcpetion;
use App\Service\Tools\Cryptor;
use App\Service\Tools\SendMail;
use DateInterval;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use stdClass;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class CustomerConfirmAccount
{

    const DATA_SEPARTOR = "__";
    const DEADLINE_ACTIVATE_ACCOUNT_IN_MINUTES = 15;

    private Cryptor $cryptor;

    private string $passphrase;

    /**
     * @throws Exception
     */
    public function __construct(
        private SendMail               $mailer,
        private EntityManagerInterface $entityManager,
    )
    {

        if (empty($_SERVER['ACCOUNT_CONFIRMATION_PASSPHRASE'])) {
            throw new EnvVariableNotFoundExcpetion('ACCOUNT_CONFIRMATION_PASSPHRASE');
        }

        $cipherAlgo = 'aes-256-cbc';

        $this->passphrase = $_SERVER['ACCOUNT_CONFIRMATION_PASSPHRASE'];

        $this->cryptor = new Cryptor(
            $cipherAlgo
        );

    }

    /**
     * @param string $token
     *
     * @return Customer
     */
    public function activeAccount(string $token): Customer
    {
        $tokenInfos = $this->decodeToken($token);

        $response = new stdClass();
        $response->user = $tokenInfos[0] ?? null;
        $user = null;
        $time = new DateTime();

        if (!empty($response->user)) {
            $user = $this->entityManager->getRepository(Customer::class)->find($response->user);
        }

        if (!empty($user) && $user instanceof Customer) {

            if (!$user->hasConfirmedAccount()) {

                if (!empty($tokenInfos[2]) && $tokenInfos[2] > $time->getTimestamp()) {

                    $user->confirmAccount();
                    $user->setActive(true);

                    $this->entityManager->persist($user);
                    $this->entityManager->flush();

                } else {
                    throw new BadRequestException("The confirm token has expired.");
                }

            }

            return $user;

        }

        throw new NotFoundHttpException("User not found");

    }

    /**
     * @param String $token
     *
     * @return false|string[]
     */
    protected function decodeToken(string $token): array|bool
    {
        return explode(self::DATA_SEPARTOR, $this->cryptor->decrypt($token, $this->passphrase));
    }

    /**
     * @param Customer    $user
     * @param string|null $deadline
     *
     * @return string
     * @throws Exception
     */
    public function createTokenFromUser(Customer $user, string $deadline = null): string
    {
        $currentDateTime = new DateTime();
        $deadlineActivationAccountDateTime = new DateTime();
        $deadline = $deadline ?? self::DEADLINE_ACTIVATE_ACCOUNT_IN_MINUTES;
        $deadlineActivationAccountDateTime->add(new DateInterval('PT' . $deadline . 'M'));

        $tokenContent =
            $user->getUuid() . self::DATA_SEPARTOR .
            $currentDateTime->getTimestamp() . self::DATA_SEPARTOR .
            $deadlineActivationAccountDateTime->getTimestamp();

        return $this->cryptor->encrypt(
            $tokenContent,
            $this->passphrase
        );
    }

    /**
     * Retourne le token de confirmation
     *
     * @throws Exception|TransportExceptionInterface
     */
    public function sendMailActivation(UserInterface $user, string $to, string $routeToConfirmAccount)
    {
        $this->mailer->sendMail(
            'email/account_confirmation.html.twig',
            [
                "user"                => $user,
                "routeConfirmAccount" => $routeToConfirmAccount,
            ],
            'Confirmation de création de votre compte',
            null,
            [
                $to,
            ]
        );
    }

}