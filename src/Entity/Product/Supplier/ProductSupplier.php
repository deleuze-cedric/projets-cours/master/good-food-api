<?php

namespace App\Entity\Product\Supplier;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Country\Currency;
use App\Entity\Product\Product;
use App\Entity\User\Employee\FranchisorEmployee;
use App\Interfaces\TestableEntityInterface;
use App\Repository\ProductSupplierRepository;
use App\Traits\Fields\Date\CreatedAtTrait;
use App\Traits\Fields\Date\UpdatedAtTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;


#[ORM\Entity(repositoryClass: ProductSupplierRepository::class)]
#[ApiResource(
    collectionOperations: [
        "get"  => [],
        "post" => [
            "security"                => 'is_granted("' . FranchisorEmployee::ROLE_EDITOR . '")',
            "denormalization_context" => [
                'groups' => [
                    "product_supplier", "product_supplier:write", "product_supplier:post",
                    // "product_supplier", "product_supplier:write", "product_supplier:post",
                    // "date", "date:write",
                ],
            ],
        ],
    ],
    itemOperations: [
        "get"    => [],
        "patch"  => [
            "security" => 'is_granted("' . FranchisorEmployee::ROLE_EDITOR . '")',
        ],
        "delete" => [
            "security" => 'is_granted("' . FranchisorEmployee::ROLE_EDITOR . '")',
        ],
    ],
    attributes: [
        'composite_identifier' => false,
    ],
    denormalizationContext: [
        'groups' => [
            "product_supplier", "product_supplier:write",
        ],
    ],
    normalizationContext: [
        'groups' => [
            "product_supplier", "product_supplier:read",
        ],
    ]
)]
class ProductSupplier implements TestableEntityInterface
{

    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[ORM\ManyToOne(targetEntity: Currency::class)]
    #[ORM\JoinColumn(referencedColumnName: "iso_code", nullable: false)]
    #[Groups([
        'product_supplier',
        'product:read',
    ])]
    private Currency $currency;

    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: Product::class, inversedBy: 'suppliers')]
    #[ORM\JoinColumn(referencedColumnName: 'reference', nullable: false)]
    #[Groups([
        'product_supplier:post',
        'product_supplier:read',
    ])]
    private Product $product;

    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: Supplier::class, inversedBy: 'products')]
    #[ORM\JoinColumn(referencedColumnName: 'slug', nullable: false)]
    #[Groups([
        'product_supplier:post',
        'product_supplier:read',
        'product:read',
    ])]
    private Supplier $supplier;

    #[ORM\Column(type: 'float')]
    #[Groups([
        'product_supplier',
        'product:read',
    ])]
    private float $supplierPrixTaxExcluded;

    #[ORM\Column(type: 'string', length: 30, nullable: true)]
    #[Groups([
        'product_supplier',
        'product:read',
    ])]
    private string $supplierReference;

    function autoActiveOnCreation(): bool
    {
        return true;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getSupplier(): ?Supplier
    {
        return $this->supplier;
    }

    public function setSupplier(?Supplier $supplier): self
    {
        $this->supplier = $supplier;

        return $this;
    }

    public function getSupplierPrixTaxExcluded(): ?float
    {
        return $this->supplierPrixTaxExcluded;
    }

    public function setSupplierPrixTaxExcluded(float $supplierPrixTaxExcluded): self
    {
        $this->supplierPrixTaxExcluded = $supplierPrixTaxExcluded;

        return $this;
    }

    public function getSupplierReference(): ?string
    {
        return $this->supplierReference;
    }

    public function setSupplierReference(?string $supplierReference): self
    {
        $this->supplierReference = $supplierReference;

        return $this;
    }

    public function getEntityName(): string
    {
        return "Fournisseur du produit";
    }

    public function routesHaveBeenTested(): bool
    {
        return false;
    }

    public function getMappingRouteToTaskName(): array
    {
        return [];
    }

    public function getEntityRouteName(): string
    {
        return "product_suppliers";
    }

    public function getRoutesDontNeedingAuth(): array
    {
        return [
            "get"
        ];
    }

    public function requireBOTest(): bool
    {
        return true;
    }

    public function getEntityIdentifierName(): string
    {
        return 'product';
    }

}
