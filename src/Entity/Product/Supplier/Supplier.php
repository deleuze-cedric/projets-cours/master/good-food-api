<?php

namespace App\Entity\Product\Supplier;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use App\Entity\User\Employee\FranchisorEmployee;
use App\Interfaces\TestableEntityInterface;
use App\Repository\SupplierRepository;
use App\Traits\Fields\Date\ActivatedAtTrait;
use App\Traits\Fields\Date\CreatedAtTrait;
use App\Traits\Fields\Date\DeletedAtTrait;
use App\Traits\Fields\Date\UpdatedAtTrait;
use App\Traits\Fields\Identifiers\SlugNameTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation\SoftDeleteable;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotNull;


#[ORM\Entity(repositoryClass: SupplierRepository::class)]
#[SoftDeleteable(fieldName: "deletedAt")]
#[ApiResource(
    collectionOperations: [
        "get"  => [
            "security" => 'is_granted("' . FranchisorEmployee::ROLE_EDITOR . '")',
        ],
        "post" => [
            "security"                => 'is_granted("' . FranchisorEmployee::ROLE_EDITOR . '")',
            "denormalization_context" => [
                'groups' => [
                    "supplier", "supplier:write", "supplier:post",
                    // "supplier", "supplier:write", "supplier:post",
                    // "date", "date:write",
                    // "active", "active:write",
                    // "delete", "delete:write",
                    // "slug", "slug:write",
                    // "description", "description:write",
                ],
            ],
        ],
    ],
    itemOperations: [
        "get"    => [],
        "patch"  => [
            "security" => 'is_granted("' . FranchisorEmployee::ROLE_EDITOR . '")',
        ],
        "delete" => [
            "security" => 'is_granted("' . FranchisorEmployee::ROLE_EDITOR . '")',
        ],
    ],
    subresourceOperations: [
        'api_suppliers_products_get_subresource' => [
            "security" => 'is_granted("' . FranchisorEmployee::ROLE_EDITOR . '")',
        ],
    ],
    denormalizationContext: [
        'groups' => [
            "supplier", "supplier:write",
        ],
    ], normalizationContext: [
    'groups' => [
        "supplier", "supplier:read",
    ],
]
)]
class Supplier implements TestableEntityInterface
{

    use SlugNameTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;
    use ActivatedAtTrait;
    use DeletedAtTrait;

    #[ORM\Column(type: 'string', length: 30)]
    #[NotNull(message: "Le nom doit être défini")]
    #[Length(
        min: 1,
        max: 30,
        minMessage: "Le nom doit faire plus de {{ limit }} caractères",
        maxMessage: "Le nom doit faire moins de {{ limit }} caractères")]
    #[Groups('supplier')]
    private string $name;

    /**
     * @var ProductSupplier[]|Collection
     */
    #[ORM\OneToMany(mappedBy: 'supplier', targetEntity: ProductSupplier::class, orphanRemoval: true)]
    #[ApiSubresource]
    private array|Collection $products;

    #[Pure] public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    public function addProduct(ProductSupplier $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->setSupplier($this);
        }

        return $this;
    }

    function autoActiveOnCreation(): bool
    {
        return true;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|ProductSupplier[]
     */
    public function getProducts(): array|Collection
    {
        return $this->products;
    }

    function purgeDataOnDelete(): bool
    {
        return false;
    }

    public function removeProduct(ProductSupplier $product): self
    {
        if ($this->products->removeElement($product)) {
            // set the owning side to null (unless already changed)
            if ($product->getSupplier() === $this) {
                $product->setSupplier(null);
            }
        }

        return $this;
    }

    public function getEntityName(): string
    {
        return "Fournisseur";
    }

    public function routesHaveBeenTested(): bool
    {
        return false;
    }

    public function getMappingRouteToTaskName(): array
    {
        return [
            "api_suppliers_products_get_subresource" => "Récupère les fournisseurs associés à un produit"
        ];
    }

    public function getEntityRouteName(): string
    {
        return "suppliers";
    }

    public function getRoutesDontNeedingAuth(): array
    {
        return [
            "get"
        ];
    }

    public function requireBOTest(): bool
    {
        return true;
    }
}
