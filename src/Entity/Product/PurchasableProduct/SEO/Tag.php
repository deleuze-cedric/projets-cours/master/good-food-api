<?php

namespace App\Entity\Product\PurchasableProduct\SEO;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\User\Employee\FranchisorEmployee;
use App\Interfaces\TestableEntityInterface;
use App\Repository\TagRepository;
use App\Traits\Fields\Date\CreatedAtTrait;
use App\Traits\Fields\Identifiers\IDTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;


#[ORM\Entity(repositoryClass: TagRepository::class)]
#[UniqueEntity(fields: ['value'], message: "Un mot clef avec la valeur '{{value}}' existe déjà.")]
#[ApiResource(
    collectionOperations: [
        "get"  => [
            "security" => 'is_granted("' . FranchisorEmployee::ROLE_EDITOR . '")',
        ],
        "post" => [
            "security" => 'is_granted("' . FranchisorEmployee::ROLE_EDITOR . '")',
        ],
    ],
    itemOperations: [
        "get"    => [],
        "delete" => [
            "security" => 'is_granted("' . FranchisorEmployee::ROLE_EDITOR . '")',
        ],
    ],
    denormalizationContext: [
        'groups' => [
            "tag", "tag:write",
        ],
    ],
    normalizationContext: [
        'groups' => [
            "tag", "tag:read",
        ],
    ],
)]
class Tag implements TestableEntityInterface
{

    use IDTrait;
    use CreatedAtTrait;

    #[ORM\Column(type: 'string', length: 30)]
    #[NotBlank(message: "Le label du mot clef doit être définie.")]
    #[Length(
        min: 2,
        max: 30,
        minMessage: "Le label du mot clef doit faire plus de {{limit}} caractères.",
        maxMessage: "Le label du mot clef doit faire moins de {{limit}} caractères."
    )]
    #[Groups(['tag'])]
    private string $value;

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function purgeDataOnDelete(): bool
    {
        return false;
    }

    public function getEntityName(): string
    {
        return "Mot clef";
    }

    public function routesHaveBeenTested(): bool
    {
        return false;
    }

    public function getMappingRouteToTaskName(): array
    {
        return [];
    }

    public function getEntityRouteName(): string
    {
        return "tags";
    }

    public function getRoutesDontNeedingAuth(): array
    {
        return [
            "get"
        ];
    }

    public function requireBOTest(): bool
    {
        return true;
    }

}
