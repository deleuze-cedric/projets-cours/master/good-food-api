<?php

namespace App\Entity\Product\PurchasableProduct\Food\Extra;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Product\PurchasableProduct\Food\Food;
use App\Entity\User\Employee\FranchiseEmployee;
use App\Entity\User\Employee\FranchisorEmployee;
use App\Interfaces\TestableEntityInterface;
use App\Repository\ExtraRepository;
use App\Traits\Fields\Identifiers\IDTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\NotNull;


#[ORM\Entity(repositoryClass: ExtraRepository::class)]
#[ApiResource(
    collectionOperations: [
        "get"  => [
            "security" => 'is_granted("' . FranchiseEmployee::ROLE_LOGISTICIAN . '") or is_granted("' . FranchisorEmployee::ROLE_EDITOR . '")',
        ],
        "post" => [
            "security"                => 'is_granted("' . FranchisorEmployee::ROLE_EDITOR . '")',
            "denormalization_context" => [
                'groups' => [
                    "extra", "extra:write", "extra:post",
                ],
            ],
        ],
    ],
    itemOperations: [
        "get"    => [],
        "patch"  => [
            "security" => 'is_granted("' . FranchisorEmployee::ROLE_EDITOR . '")',
        ],
        "delete" => [
            "security" => 'is_granted("' . FranchisorEmployee::ROLE_EDITOR . '")',
        ],
    ],
    denormalizationContext: [
        'groups' => [
            "extra", "extra:write",
        ],
    ],
    normalizationContext: [
        'groups' => [
            "extra", "extra:read",
        ],
    ]
)]
class Extra implements TestableEntityInterface
{

    use IDTrait;

    #[ORM\ManyToMany(targetEntity: ExtraValue::class)]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups([
        'extra',
        'food:read',
    ])]
    private ExtraValue $extraValue;

    #[ORM\Column(type: 'boolean')]
    #[Groups([
        'extra',
        'food:read',
    ])]
    private bool $multiselect = false;

    #[ORM\ManyToOne(targetEntity: Food::class, inversedBy: 'extras')]
    #[ORM\JoinColumn(referencedColumnName: 'reference', nullable: false)]
    #[NotNull(message: "Le produit possédant l'extra doit être définie")]
    #[Groups([
        'extra',
        'extra:post',
    ])]
    private Food $product;

    public function getExtraValue(): ?ExtraValue
    {
        return $this->extraValue;
    }

    public function setExtraValue(?ExtraValue $extraValue): self
    {
        $this->extraValue = $extraValue;

        return $this;
    }

    public function getMultiselect(): ?bool
    {
        return $this->multiselect;
    }

    public function setMultiselect(bool $multiselect): self
    {
        $this->multiselect = $multiselect;

        return $this;
    }

    public function getProduct(): ?Food
    {
        return $this->product;
    }

    public function setProduct(?Food $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getEntityName(): string
    {
        return "Extra";
    }

    public function routesHaveBeenTested(): bool
    {
        return false;
    }

    public function getMappingRouteToTaskName(): array
    {
        return [];
    }

    public function getEntityRouteName(): string
    {
        return "extras";
    }

    public function getRoutesDontNeedingAuth(): array
    {
        return [
            "get"
        ];
    }

    public function requireBOTest(): bool
    {
        return true;
    }

}
