<?php

namespace App\Entity\Product\PurchasableProduct\Food\Extra;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\User\Employee\FranchiseEmployee;
use App\Entity\User\Employee\FranchisorEmployee;
use App\Interfaces\TestableEntityInterface;
use App\Repository\ExtraValueRepository;
use App\Traits\Fields\Date\ActivatedAtTrait;
use App\Traits\Fields\Date\CreatedAtTrait;
use App\Traits\Fields\Date\DeletedAtTrait;
use App\Traits\Fields\Date\UpdatedAtTrait;
use App\Traits\Fields\Identifiers\SlugNameTrait;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation\SoftDeleteable;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;


#[ORM\Entity(repositoryClass: ExtraValueRepository::class)]
#[SoftDeleteable(fieldName: "deletedAt")]
#[ApiResource(
    collectionOperations: [
        "get"  => [
            "security" => 'is_granted("' . FranchiseEmployee::ROLE_LOGISTICIAN . '") or is_granted("' . FranchisorEmployee::ROLE_EDITOR . '")',
        ],
        "post" => [
            "security" => 'is_granted("' . FranchisorEmployee::ROLE_EDITOR . '")',
        ],
    ],
    itemOperations: [
        "get"    => [],
        "patch"  => [
            "security" => 'is_granted("' . FranchisorEmployee::ROLE_EDITOR . '")',
        ],
        "delete" => [
            "security" => 'is_granted("' . FranchisorEmployee::ROLE_EDITOR . '")',
        ],
    ],
    denormalizationContext: [
        'groups' => [
            "extra_value", "extra_value:write",
        ],
    ],
    normalizationContext: [
        'groups' => [
            "extra_value", "extra_value:read",
        ],
    ]
)]
class ExtraValue implements TestableEntityInterface
{

    use SlugNameTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;
    use ActivatedAtTrait;
    use DeletedAtTrait;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups([
        "extra_value",
        'extra:read',
        'food:read',
    ])]
    private ?string $description;

    #[ORM\ManyToOne(targetEntity: ExtraGroup::class, inversedBy: 'extraValues')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups([
        "extra_value",
        'extra:read',
        'food:read',
        'order:read',
        'order_line:read',
    ])]
    private ExtraGroup $extraGroup;

    #[ORM\Column(type: 'string', length: 50)]
    #[NotBlank(message: "Le nom doit être renseigné.")]
    #[Length(
        min: 3,
        max: 50,
        minMessage: "Le nom doit faire au moins {{limit}} caractères.",
        maxMessage: "Le nom doit faire moins de {{limit}} caractères."
    )]
    #[Groups([
        "extra_value",
        'extra:read',
        'food:read',
        'order:read',
        'order_line:read',
    ])]
    private string $name;

    #[ORM\Column(type: 'integer')]
    #[NotNull(message: "La position doit être définie")]
    #[GreaterThanOrEqual(value: 0, message: "La position doit être supérieure ou égale à {{compared_value}}")]
    #[Groups([
        "extra_value",
        'extra:read',
        'food:read',
        'order:read',
        'order_line:read',
    ])]
    private int $position;

    protected function autoActiveOnCreation(): bool
    {
        return true;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getExtraGroup(): ?ExtraGroup
    {
        return $this->extraGroup;
    }

    public function setExtraGroup(?ExtraGroup $extraGroup): self
    {
        $this->extraGroup = $extraGroup;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function purgeDataOnDelete(): bool
    {
        return false;
    }

    public function getEntityName(): string
    {
        return "Valeur extra";
    }

    public function routesHaveBeenTested(): bool
    {
        return false;
    }

    public function getMappingRouteToTaskName(): array
    {
        return [];
    }

    public function getEntityRouteName(): string
    {
        return "extra_values";
    }

    public function getRoutesDontNeedingAuth(): array
    {
        return [
            "get"
        ];
    }

    public function requireBOTest(): bool
    {
        return true;
    }

}
