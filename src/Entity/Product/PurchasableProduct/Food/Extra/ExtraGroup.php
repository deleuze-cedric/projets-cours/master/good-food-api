<?php

namespace App\Entity\Product\PurchasableProduct\Food\Extra;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use App\Entity\Restaurant\Restaurant;
use App\Entity\User\Employee\FranchiseEmployee;
use App\Entity\User\Employee\FranchisorEmployee;
use App\Interfaces\TestableEntityInterface;
use App\Repository\ExtraGroupRepository;
use App\Traits\Fields\Date\ActivatedAtTrait;
use App\Traits\Fields\Date\CreatedAtTrait;
use App\Traits\Fields\Date\DeletedAtTrait;
use App\Traits\Fields\Date\UpdatedAtTrait;
use App\Traits\Fields\Identifiers\SlugNameTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation\SoftDeleteable;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;


#[ORM\Entity(repositoryClass: ExtraGroupRepository::class)]
#[SoftDeleteable(fieldName: "deletedAt")]
#[ApiResource(
    collectionOperations: [
        "get"  => [
            "security" => 'is_granted("' . FranchiseEmployee::ROLE_LOGISTICIAN . '") or is_granted("' . FranchisorEmployee::ROLE_EDITOR . '")',
        ],
        "post" => [
            "security" => 'is_granted("' . FranchisorEmployee::ROLE_EDITOR . '")',
        ],
    ],
    itemOperations: [
        "get"    => [],
        "patch"  => [
            "security" => 'is_granted("' . FranchisorEmployee::ROLE_EDITOR . '")',
        ],
        "delete" => [
            "security" => 'is_granted("' . FranchisorEmployee::ROLE_EDITOR . '")',
        ],
    ],
    subresourceOperations: [
        "api_extra_groups_extra_values_get_subresource" => [
            "security" => 'is_granted("' . FranchiseEmployee::ROLE_LOGISTICIAN . '") or is_granted("' . FranchisorEmployee::ROLE_EDITOR . '")',
        ],
    ],
    denormalizationContext: [
        'groups' => [
            "extra_group", "extra_group:write",
        ],
    ],
    normalizationContext: [
        'groups' => [
            "extra_group", "extra_group:read",
        ],
    ]
)]
class ExtraGroup implements TestableEntityInterface
{

    use SlugNameTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;
    use ActivatedAtTrait;
    use DeletedAtTrait;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups([
        "extra_group",
        "extra_value:read",
        'extra:read',
        'food:read',
        'order:read',
        'order_line:read',
    ])]
    private ?string $description;

    /**
     * @var ExtraValue[]|Collection
     */
    #[ORM\OneToMany(mappedBy: 'extraGroup', targetEntity: ExtraValue::class, orphanRemoval: true)]
    #[ApiSubresource]
    private array|Collection $extraValues;

    #[ORM\Column(type: 'string', length: 50)]
    #[NotBlank(message: "Le nom doit être renseigné.")]
    #[Length(
        min: 3,
        max: 50,
        minMessage: "Le nom doit faire au moins {{limit}} caractères.",
        maxMessage: "Le nom doit faire moins de {{limit}} caractères."
    )]
    #[Groups([
        "extra_group",
        "extra_value:read",
        'extra:read',
        'food:read',
        'order:read',
        'order_line:read',
    ])]
    private string $name;

    /**
     * @var Restaurant[]|Collection
     */
    #[ORM\ManyToMany(targetEntity: Restaurant::class, inversedBy: 'azertyuiop')]
    private array|Collection $onlyInRestaurants;

    #[ORM\Column(type: 'integer')]
    #[NotNull(message: "La position doit être définie")]
    #[GreaterThanOrEqual(value: 0, message: "La position doit être supérieure ou égale à {{compared_value}}")]
    #[Groups([
        "extra_group",
        "extra_value:read",
        'extra:read',
        'food:read',
        'order:read',
        'order_line:read',
    ])]
    private int $position;

    #[Pure] public function __construct()
    {
        $this->extraValues = new ArrayCollection();
        $this->onlyInRestaurants = new ArrayCollection();
    }

    public function addExtraValue(ExtraValue $extraValue): self
    {
        if (!$this->extraValues->contains($extraValue)) {
            $this->extraValues[] = $extraValue;
            $extraValue->setExtraGroup($this);
        }

        return $this;
    }

    public function addOnlyInRestaurant(Restaurant $onlyInRestaurant): self
    {
        if (!$this->onlyInRestaurants->contains($onlyInRestaurant)) {
            $this->onlyInRestaurants[] = $onlyInRestaurant;
        }

        return $this;
    }

    protected function autoActiveOnCreation(): bool
    {
        return true;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|ExtraValue[]
     */
    public function getExtraValues(): Collection|array
    {
        return $this->extraValues;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Restaurant[]
     */
    public function getOnlyInRestaurants(): array|Collection
    {
        return $this->onlyInRestaurants;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function purgeDataOnDelete(): bool
    {
        return false;
    }

    public function removeExtraValue(ExtraValue $extraValue): self
    {
        if ($this->extraValues->removeElement($extraValue)) {
            // set the owning side to null (unless already changed)
            if ($extraValue->getExtraGroup() === $this) {
                $extraValue->setExtraGroup(null);
            }
        }

        return $this;
    }

    public function removeOnlyInRestaurant(Restaurant $onlyInRestaurant): self
    {
        $this->onlyInRestaurants->removeElement($onlyInRestaurant);

        return $this;
    }

    public function getEntityName(): string
    {
        return "Groupe extra";
    }

    public function routesHaveBeenTested(): bool
    {
        return false;
    }

    public function getMappingRouteToTaskName(): array
    {
        return [
            "api_extra_groups_extra_values_get_subresource" => "Récupère les valeurs d'extras associées à un group d'extra"
        ];
    }

    public function getEntityRouteName(): string
    {
        return "extra_groups";
    }

    public function getRoutesDontNeedingAuth(): array
    {
        return [
            "get"
        ];
    }

    public function requireBOTest(): bool
    {
        return true;
    }

}
