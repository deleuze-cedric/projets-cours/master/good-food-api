<?php

namespace App\Entity\Product\PurchasableProduct\Food;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Product\PurchasableProduct\Menu\Menu;
use App\Filter\Product\SearchByNameOrReference;
use App\Repository\DessertRepository;
use App\Voter\ProductVoter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\Table;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Serializer\Annotation\Groups;


#[Entity(repositoryClass: DessertRepository::class)]
#[ApiResource(
    collectionOperations: [
        'get'  => [],
        'post' => [
            // Seulement les editeurs et + ou si il n'y a qu'un seul restaurant pour le produit et que ce restaurant et celui de l'employé franchisé connecté
            'security'                => 'is_granted("' . ProductVoter::IS_AUTHORIZED_TO_MANAGE_PRODUCT . '", object)',
            'denormalization_context' => [
                'groups' => [
                    "product", "product:read", "product:post",
                    "purchasable_product", "purchasable_product:read",
                    "food", "food:read",
                    "dessert", "dessert:read",
                ],
            ],
        ],
    ],
    itemOperations: [
        'get'    => [],
        'patch'  => [
            // Seulement les editeurs et + ou si il n'y a qu'un seul restaurant pour le produit et que ce restaurant et celui de l'employé franchisé connecté
            'security' => 'is_granted("' . ProductVoter::IS_AUTHORIZED_TO_MANAGE_PRODUCT . '", object)',
        ],
        'delete' => [
            // Seulement les editeurs et + ou si il n'y a qu'un seul restaurant pour le produit et que ce restaurant et celui de l'employé franchisé connecté
            'security' => 'is_granted("' . ProductVoter::IS_AUTHORIZED_TO_MANAGE_PRODUCT . '", object)',
        ],
    ],
    denormalizationContext: [
        'groups' => [
            "product", "product:write",
            "purchasable_product", "purchasable_product:write",
            "food", "food:write",
            "dessert", "dessert:write",
        ],
    ],
    normalizationContext: [
        'groups' => [
            "product", "product:read",
            "purchasable_product", "purchasable_product:read",
            "food", "food:read",
            "dessert", "dessert:read",
        ],
    ]
)]
#[ApiFilter(SearchByNameOrReference::class, properties: ['search_by_name_or_reference'])]
class Dessert extends Food
{

    /**
     * @var Menu[]|Collection
     */
    #[ManyToMany(targetEntity: Menu::class, mappedBy: "dessertChoices")]
    private array|Collection $useInMenus;

    #[Pure] public function __construct()
    {
        parent::__construct();
        $this->useInMenus = new ArrayCollection();
    }

    public function addUseInMenu(Menu $useInMenu): self
    {
        if (!$this->useInMenus->contains($useInMenu)) {
            $this->useInMenus[] = $useInMenu;
            $useInMenu->addDessertChoice($this);
        }

        return $this;
    }

    /**
     * @return Menu[]|Collection
     */
    public function getUseInMenus(): array|Collection
    {
        return $this->useInMenus;
    }

    public function removeUseInMenu(Menu $useInMenu): self
    {
        if ($this->useInMenus->removeElement($useInMenu)) {
            $useInMenu->removeDessertChoice($this);
        }

        return $this;
    }

    public function getEntityName(): string
    {
        return "Déssert";
    }

    public function routesHaveBeenTested(): bool
    {
        return false;
    }

    public function getMappingRouteToTaskName(): array
    {
        return [];
    }

    public function getEntityRouteName(): string
    {
        return "desserts";
    }

    public function getRoutesDontNeedingAuth(): array
    {
        return [
            "get"
        ];
    }

    public function requireBOTest(): bool
    {
        return true;
    }

    #[Groups(['purchasable_product:read'])]
    public function getMainCategory(): string
    {
        return 'Déssert';
    }

}
