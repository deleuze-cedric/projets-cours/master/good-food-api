<?php

namespace App\Entity\Product\PurchasableProduct;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Entity\Country\Currency;
use App\Entity\Country\Tax\TaxType;
use App\Entity\Product\Category\Category;
use App\Entity\Product\Product;
use App\Entity\Product\PurchasableProduct\SEO\Tag;
use App\Entity\Product\PurchasableProduct\Variation\Variation;
use App\Entity\Restaurant\Restaurant;
use App\Entity\User\Comment\Comment;
use App\Filter\Product\SearchByNameOrReference;
use App\Filter\Restaurant\SearchByNameOrCity;
use App\Repository\PurchasableProductRepository;
use App\Service\Tools\Tools;
use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\InverseJoinColumn;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\Table;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Context\ExecutionContextInterface;


#[Entity(repositoryClass: PurchasableProductRepository::class)]
#[DiscriminatorColumn(name: "type", type: "string")]
#[DiscriminatorMap(
    [
        "food"      => "App\Entity\Product\PurchasableProduct\Food\Food",
        "entree"    => "App\Entity\Product\PurchasableProduct\Food\Entree",
        "drink"     => "App\Entity\Product\PurchasableProduct\Food\Drink",
        "dish"      => "App\Entity\Product\PurchasableProduct\Food\Dish",
        "dessert"   => "App\Entity\Product\PurchasableProduct\Food\Dessert",
        "variation" => "App\Entity\Product\PurchasableProduct\Variation\Variation",
    ]
)]
#[ApiResource(
    collectionOperations: [
        'get' => [],
    ],
    itemOperations: [
        'get' => [],
    ],
    denormalizationContext: [
        'groups' => [],
    ],
    normalizationContext: [
        'groups' => [
            "product", "product:read",
            "purchasable_product", "purchasable_product:read",
        ],
    ]
)]
#[ApiFilter(
    SearchFilter::class,
    properties: [
        'reference' => 'partial',
        'name' => 'partial',
        'priceTaxExcluded' => 'exact',
    ]
)]
#[ApiFilter(
    OrderFilter::class,
    properties: [
        'reference',
        'name',
        'priceTaxExcluded',
        'currency.isoCode',
    ]
)]
#[ApiFilter(SearchByNameOrReference::class, properties: ['search_by_name_or_reference'])]
abstract class PurchasableProduct extends Product
{

    #[Column(type: "datetime", nullable: true)]
    #[Groups([
        'purchasable_product',
        'voucher:read',
        'voucher_data:read',
        'order:read',
        'order_line:read',
        'comment:read',
    ])]
    private ?DateTime $availableFrom;

    #[Column(type: "datetime", nullable: true)]
    #[Groups([
        'purchasable_product',
        'voucher:read',
        'voucher_data:read',
        'order:read',
        'order_line:read',
        'comment:read',
    ])]
    private ?Datetime $availableTo;

    /**
     * @var Category[]|Collection
     */
    #[ManyToMany(targetEntity: Category::class, mappedBy: 'products')]
    #[Groups([
        'purchasable_product',
    ])]
    #[ApiProperty(writableLink: false)]
    private array|Collection $categories;

    #[ManyToOne(targetEntity: Currency::class)]
    #[JoinColumn(referencedColumnName: "iso_code", nullable: false)]
    #[NotNull(message: "La devise doit être renseignée.")]
    #[Groups([
        'purchasable_product',
        'voucher:read',
        'voucher_data:read',
        'order:read',
        'order_line:read',
        'comment:read',
    ])]
    #[ApiProperty(writableLink: false)]
    private Currency $currency;

    #[Column(type: "float")]
    #[NotNull(message: "L'écotax' doit être renseigné.")]
    #[GreaterThanOrEqual(
        value: 0,
        message: "L'écotax doit être supérieure ou égale à {{ compared_value }}."
    )]
    #[Groups([
        'purchasable_product',
        'voucher:read',
        'voucher_data:read',
        'order:read',
        'order_line:read',
        'comment:read',
    ])]
    private float $ecotax;

    #[Column(type: "integer")]
    #[NotNull(message: "La quantité minimum pour vendre doit être renseignée.")]
    #[GreaterThanOrEqual(
        value: 0,
        message: "La quantité minimum en stock pour autoriser la vente doit être supérieure ou égal à {{ compared_value }}."
    )]
    #[Groups([
        'purchasable_product',
        'voucher:read',
        'voucher_data:read',
        'order:read',
        'order_line:read',
        'comment:read',
    ])]
    private int $minQtyInStockToSell;

    #[Column(type: "boolean")]
    #[NotNull(message: "L'état de vente du produit doit être renseigné.")]
    #[Groups([
        'purchasable_product',
        'voucher:read',
        'voucher_data:read',
        'order:read',
        'order_line:read',
        'comment:read',
    ])]
    private bool $onSale;

    #[Column(type: "float")]
    #[NotNull(message: "Le prix du produit doit être renseigné.")]
    #[GreaterThan(
        value: 0,
        message: "Le prix de vente doit être supérieure à {{ compared_value }}."
    )]
    #[Groups([
        'purchasable_product',
        'voucher:read',
        'voucher_data:read',
        'order:read',
        'order_line:read',
        'comment:read',
    ])]
    private float $priceTaxExcluded;

    /**
     * @var Collection|Tag[]
     */
    #[ManyToMany(targetEntity: Tag::class)]
    #[JoinColumn(referencedColumnName: "reference")]
    #[InverseJoinColumn(referencedColumnName: "id")]
    #[Groups('purchasable_product')]
    #[ApiProperty(writableLink: false)]
    private array|Collection $tags;

    /**
     * @var Variation[]|Collection
     */
    #[OneToMany(mappedBy: 'variableProduct', targetEntity: Variation::class, orphanRemoval: true)]
    #[ApiProperty(readableLink: false)]
    #[ApiSubresource]
    private array|Collection $variations;

    /**
     * @var Comment[]|Collection
     */
    #[OneToMany(mappedBy: "purchasableProduct", targetEntity: Comment::class)]
    #[Groups('purchasable_product:read')]
    #[ApiProperty(readableLink: false)]
    private array|Collection $comments;

    #[ManyToOne(targetEntity: TaxType::class)]
    #[JoinColumn(nullable: false)]
    #[NotNull(message: 'Le type de taxe a appliqué sur le produit doit être définie')]
    #[Groups([
        'purchasable_product',
        'voucher:read',
        'voucher_data:read',
        'order:read',
        'order_line:read',
        'comment:read',
    ])]
    private TaxType $taxType;

    #[Pure] public function __construct()
    {
        parent::__construct();
        $this->tags = new ArrayCollection();
        $this->variations = new ArrayCollection();
        $this->categories = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    #[Groups('purchasable_product:read')]
    public function getMeanNote(): int
    {
        if ($this->comments->count() < 1) {
            return 0;
        }

        $total = 0;

        foreach ($this->comments as $comment) {
            $total += $comment->getNote();
        }

        return $total / $this->comments->count();
    }

    public function getComments(): array|Collection
    {
        return $this->comments;
    }

    public function addCategory(Category $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
            $category->addProduct($this);
        }

        return $this;
    }

    public function addTag(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
        }

        return $this;
    }

    public function addVariation(Variation $variation): self
    {
        if (!$this->variations->contains($variation)) {
            $this->variations[] = $variation;
            $variation->setVariableProduct($this);
        }

        return $this;
    }

    public function getAvailableFrom(): ?DateTimeInterface
    {
        return $this->availableFrom;
    }

    public function setAvailableFrom(?DateTimeInterface $availableFrom): self
    {
        $this->availableFrom = $availableFrom;

        return $this;
    }

    public function getAvailableTo(): ?DateTimeInterface
    {
        return $this->availableTo;
    }

    public function setAvailableTo(?DateTimeInterface $availableTo): self
    {
        $this->availableTo = $availableTo;

        return $this;
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategories(): array|Collection
    {
        return $this->categories;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getEcotax(): ?float
    {
        return $this->ecotax;
    }

    public function setEcotax(float $ecotax): self
    {
        $this->ecotax = $ecotax;

        return $this;
    }

    public function getMinQtyInStockToSell(): ?int
    {
        return $this->minQtyInStockToSell;
    }

    public function setMinQtyInStockToSell(int $minQtyInStockToSell): self
    {
        $this->minQtyInStockToSell = $minQtyInStockToSell;

        return $this;
    }

    public function getOnSale(): ?bool
    {
        return $this->onSale;
    }

    public function setOnSale(bool $onSale): self
    {
        $this->onSale = $onSale;

        return $this;
    }

    #[Pure] public function getPriceTaxExcluded(): ?string
    {
        return Tools::formatPrice($this?->priceTaxExcluded);
    }

    public function setPriceTaxExcluded(float $priceTaxExcluded): self
    {
        $this->priceTaxExcluded = $priceTaxExcluded;

        return $this;
    }

    /**
     * @return Collection|Tag[]
     */
    public function getTags(): array|Collection
    {
        return $this->tags;
    }

    /**
     * @return Collection|Variation[]
     */
    public function getVariations(): Collection|array
    {
        return $this->variations;
    }

    public function removeCategory(Category $category): self
    {
        if ($this->categories->removeElement($category)) {
            $category->removeProduct($this);
        }

        return $this;
    }

    public function removeTag(Tag $tag): self
    {
        $this->tags->removeElement($tag);

        return $this;
    }

    public function removeVariation(Variation $variation): self
    {
        if ($this->variations->removeElement($variation)) {
            // set the owning side to null (unless already changed)
            if ($variation->getVariableProduct() === $this) {
                $variation->setVariableProduct(null);
            }
        }

        return $this;
    }

    #[Callback]
    public function validateAvailableDates(ExecutionContextInterface $context, $payload)
    {
        if (!is_null($this->availableFrom) && !is_null($this->availableTo) && $this->availableFrom > $this->availableTo) {
            $context->buildViolation('La date de départ doit être antérieur ou égale à la date de fin.')
                    ->atPath('availableFrom')
                    ->addViolation();
        }
    }

    public function getEntityName(): string
    {
        return "Produit achetable";
    }

    public function routesHaveBeenTested(): bool
    {
        return false;
    }

    public function getMappingRouteToTaskName(): array
    {
        return [];
    }

    public function getEntityRouteName(): string
    {
        return "purchasable_products";
    }

    public function getRoutesDontNeedingAuth(): array
    {
        return [
            "get",
        ];
    }

    public function requireBOTest(): bool
    {
        return true;
    }

    public function getTaxType(): ?TaxType
    {
        return $this->taxType;
    }

    public function setTaxType(?TaxType $taxType): self
    {
        $this->taxType = $taxType;

        return $this;
    }

    #[Pure]
    public function getTaxRateForRestaurant(?Restaurant $restaurant): float
    {

        $taxRate = 0;

        if(!empty($restaurant)) {

            foreach ($this?->getTaxType()?->getTaxes() as $tax) {

                if ($tax->getCountry()->getIsoCode() === $restaurant->getCountry()->getIsoCode()) {
                    $taxRate = $tax->getRate();
                }

            }

        }

        return $taxRate;

    }

    #[Pure]
    public function getPriceForRestaurant(Restaurant $restaurant): float
    {

        $taxRate = $this->getTaxRateForRestaurant($restaurant);

        return $this->priceTaxExcluded * (1 + $taxRate);

    }

    #[Groups(['purchasable_product:read'])]
    public abstract function getMainCategory(): string;

}
