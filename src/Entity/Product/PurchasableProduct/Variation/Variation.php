<?php

namespace App\Entity\Product\PurchasableProduct\Variation;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Product\PurchasableProduct\PurchasableProduct;
use App\Entity\User\Employee\FranchisorEmployee;
use App\Repository\VariationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\NotNull;


#[ORM\Entity(repositoryClass: VariationRepository::class)]
#[ApiResource(
    collectionOperations: [
        "get"  => [],
        "post" => [
            "security"                => 'is_granted("' . FranchisorEmployee::ROLE_EDITOR . '")',
            "denormalization_context" => [
                'groups' => [
                    "product", "product:write", "product:post",
                    "purchasable_product", "purchasable_product:write",
                    "variation", "variation:write", "variation:post"
                    // "product", "product:write",
                    // "purchasable_product", "purchasable_product:write",
                    // "date", "date:write",
                    // "active", "active:write",
                    // "delete", "delete:write",
                    // "slug", "slug:write",
                    // "variation", "variation:write", "variation:post",
                ],
            ],
        ],
    ],
    itemOperations: [
        "get"    => [],
        "patch"  => [
            "security" => 'is_granted("' . FranchisorEmployee::ROLE_EDITOR . '")',
        ],
        "delete" => [
            "security" => 'is_granted("' . FranchisorEmployee::ROLE_EDITOR . '")',
        ],
    ],
    denormalizationContext: [
        'groups' => [
            "product", "product:write",
            "purchasable_product", "purchasable_product:write",
            "variation", "variation:write",
        ],
    ],
    normalizationContext: [
        'groups' => [
            "product", "product:read",
            "purchasable_product", "purchasable_product:read",
            "variation", "variation:read",
        ],
    ]
)]
class Variation extends PurchasableProduct
{

    #[ORM\ManyToOne(targetEntity: PurchasableProduct::class, inversedBy: 'variations')]
    #[ORM\JoinColumn(referencedColumnName: 'reference', nullable: false)]
    #[NotNull(message: "Le produit déclinable doit être renseigné")]
    #[Groups([
        'variation:post',
        'variation:read',
    ])]
    #[ApiProperty(readableLink: false)]
    private PurchasableProduct $variableProduct;

    /**
     * @var Variation[]|Collection
     */
    #[ORM\ManyToMany(targetEntity: VariationValue::class)]
    #[ORM\JoinColumn(referencedColumnName: 'reference')]
    #[Groups([
        'variation',
    ])]
    private array|Collection $variationsValues;

    #[Pure] public function __construct()
    {
        parent::__construct();
        $this->variationsValues = new ArrayCollection();
    }

    public function addVariationsValue(VariationValue $variationsValue): self
    {
        if (!$this->variationsValues->contains($variationsValue)) {
            $this->variationsValues[] = $variationsValue;
        }

        return $this;
    }

    public function getVariableProduct(): ?PurchasableProduct
    {
        return $this->variableProduct;
    }

    public function setVariableProduct(?PurchasableProduct $variableProduct): self
    {
        $this->variableProduct = $variableProduct;

        return $this;
    }

    /**
     * @return Collection|VariationValue[]
     */
    public function getVariationsValues(): array|Collection
    {
        return $this->variationsValues;
    }

    public function removeVariationsValue(VariationValue $variationsValue): self
    {
        $this->variationsValues->removeElement($variationsValue);

        return $this;
    }

    public function getEntityName(): string
    {
        return "Déclinaison";
    }

    public function routesHaveBeenTested(): bool
    {
        return false;
    }

    public function getMappingRouteToTaskName(): array
    {
        return [];
    }

    public function getEntityRouteName(): string
    {
        return "variations";
    }

    public function getRoutesDontNeedingAuth(): array
    {
        return [
            "get"
        ];
    }

    public function requireBOTest(): bool
    {
        return true;
    }

    #[Groups(['purchasable_product:read'])]
    public function getMainCategory(): string
    {
        return $this->variableProduct->getMainCategory();
    }

}
