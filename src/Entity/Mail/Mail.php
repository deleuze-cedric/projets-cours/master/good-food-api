<?php

namespace App\Entity\Mail;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\User\Employee\FranchisorEmployee;
use App\Interfaces\TestableEntityInterface;
use App\Repository\MailRepository;
use App\Traits\Fields\Date\CreatedAtTrait;
use App\Traits\Fields\Date\UpdatedAtTrait;
use App\Traits\Fields\Identifiers\IDTrait;
use DateTime;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Symfony\Component\Serializer\Annotation\Groups;


#[Entity(repositoryClass: MailRepository::class)]
#[ApiResource(
    collectionOperations: [
        "get" => [
            "security" => "is_granted('" . FranchisorEmployee::ROLE_ADMIN . "')",
        ],
    ],
    itemOperations: [
        "get" => [
            "security" => "is_granted('" . FranchisorEmployee::ROLE_ADMIN . "')",
        ],
    ],
    denormalizationContext: [
        "groups" => [
            "mail", "mail:write",
        ],
    ],
    normalizationContext: [
        "groups" => [
            "mail", "mail:read",
        ],
    ],
)]
class Mail implements TestableEntityInterface
{

    use IDTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[Column(type: "json")]
    #[Groups('mail')]
    private array $addresses = [];

    #[Column(type: "text")]
    #[Groups('mail')]
    private string $content;

    #[Column(type: "json")]
    #[Groups('mail:read')]
    private array $debug = [];

    #[Column(type: "datetime", nullable: true)]
    #[Groups('mail:read')]
    private DateTime $failedAt;

    #[Column(type: "datetime")]
    #[Groups('mail:read')]
    private DateTime $lastAttemptSendAt;

    #[Column(type: "string", length: 255)]
    #[Groups('mail')]
    private string $sender;

    #[Column(type: "string", length: 255)]
    #[Groups('mail')]
    private string $subject;

    public function getAddresses(): ?array
    {
        return $this->addresses;
    }

    public function setAddresses(array $addresses): self
    {
        $this->addresses = $addresses;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getFailedAt(): DateTime
    {
        return $this->failedAt;
    }

    /**
     * @return DateTime
     */
    public function getLastAttemptSendAt(): DateTime
    {
        return $this->lastAttemptSendAt;
    }

    public function getSender(): ?string
    {
        return $this->sender;
    }

    public function setSender(string $sender): self
    {
        $this->sender = $sender;

        return $this;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function setSubject(string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function newFail(array $debug): self
    {
        $this->failedAt = new DateTime();
        $this->debug = $debug;

        return $this;
    }

    public function newSendingAttempts(): self
    {
        $this->lastAttemptSendAt = new DateTime();

        return $this;
    }

    public function getEntityName(): string
    {
        return "Mail";
    }

    public function routesHaveBeenTested(): bool
    {
        return false;
    }

    public function getMappingRouteToTaskName(): array
    {
        return [];
    }

    public function getEntityRouteName(): string
    {
        return "mails";
    }

    public function getRoutesDontNeedingAuth(): array
    {
        return [];
    }

    public function requireBOTest(): bool
    {
        return false;
    }

}
