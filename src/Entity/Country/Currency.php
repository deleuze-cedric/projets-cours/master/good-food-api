<?php

namespace App\Entity\Country;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\User\Employee\FranchisorEmployee;
use App\Interfaces\TestableEntityInterface;
use App\Repository\CurrencyRepository;
use App\Service\Tools\Tools;
use App\Traits\Fields\Date\ActivatedAtTrait;
use App\Traits\Fields\Date\CreatedAtTrait;
use App\Traits\Fields\Date\DeletedAtTrait;
use App\Traits\Fields\Date\UpdatedAtTrait;
use App\Traits\Fields\Identifiers\IsoCodeTrait;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Table;
use Gedmo\Mapping\Annotation\SoftDeleteable;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;


#[Entity(repositoryClass: CurrencyRepository::class)]
#[UniqueEntity(fields: ["isoCode"], message: "Le code iso de la devise doit être unique.")]
#[SoftDeleteable(fieldName: "deletedAt")]
#[HasLifecycleCallbacks()]
#[ApiResource(
    collectionOperations: [
        //	Tout le monde peut récupérer la liste des devises
        "get"  => [],
        // Seuls les admins ou + peuvent créer une devise
        "post" => [
            "security"                => "is_granted('" . FranchisorEmployee::ROLE_ADMIN . "')",
            'denormalization_context' => [
                "groups" => [
                    "iso_code", "iso_code:post",
                    "currency", "currency:write",
                ],
            ],
        ],
    ],
    itemOperations: [
        //	Tout le monde peut récupérer une devise
        "get"    => [],
        // Seuls les admins ou + peuvent modifier une devise
        "patch"  => [
            "security" => "is_granted('" . FranchisorEmployee::ROLE_ADMIN . "')",
        ],
        // Seuls les admins ou + peuvent supprimer une devise
        "delete" => [
            "security" => "is_granted('" . FranchisorEmployee::ROLE_ADMIN . "')",
        ],
    ],
    denormalizationContext: [
        "groups" => [
            "currency", "currency:write",
            // "currency", "currency:write",
        ],
    ],
    normalizationContext: [
        "groups" => [
            "currency", "currency:read",
        ],
    ],
)]
class Currency implements TestableEntityInterface
{

    use IsoCodeTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;
    use ActivatedAtTrait;
    use DeletedAtTrait;

    #[Column(type: "string", length: 50)]
    #[NotBlank(message: "Le nom doit être renseigné.")]
    #[Length(
        min: 3,
        max: 50,
        minMessage: "Le nom doit faire au moins {{limit}} caractères.",
        maxMessage: "Le nom doit faire moins de {{limit}} caractères."
    )]
    #[Groups([
        "currency",
        "country:read",
        "restaurant:read",
        'voucher:read',
        'voucher_data:read',
        'carrier:read',
        'carrier_data:read',
        'order:read',
        'order_line:read',
        'purchasable_product:read',
    ])]
    private string $name;

    #[Column(type: "decimal", precision: 10, scale: 5)]
    #[NotNull(message: "Le taux de change pour un euro doit être renseigné.")]
    #[GreaterThan(
        value: 0,
        message: "Le taux de change doit être supérieur à 0."
    )]
    #[Groups([
        "currency",
        "country:read",
        "restaurant:read",
        'voucher:read',
        'voucher_data:read',
        'carrier:read',
        'carrier_data:read',
        'order:read',
        'order_line:read',
        'purchasable_product:read',
    ])]
    private float $rateForOneEuro;

    #[Column(type: "string", length: 20)]
    #[NotBlank(message: "Le symbole doit être renseigné.")]
    #[Length(
        min: 1,
        max: 20,
        minMessage: "Le symbole doit faire au moins {{limit}} caractères.",
        maxMessage: "Le symbole doit faire moins de {{limit}} caractères."
    )]
    #[Groups([
        "currency",
        "country:read",
        "restaurant:read",
        'voucher:read',
        'voucher_data:read',
        'carrier:read',
        'carrier_data:read',
        'order:read',
        'order_line:read',
        'purchasable_product:read',
    ])]
    private string $symbol;

    function autoActiveOnCreation(): bool
    {
        return true;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    #[Pure] public function getRateForOneEuro(): ?string
    {
        return Tools::formatCurrency($this?->rateForOneEuro);
    }

    public function setRateForOneEuro(float $rateForOneEuro): self
    {
        $this->rateForOneEuro = $rateForOneEuro;

        return $this;
    }

    public function getSymbol(): ?string
    {
        return $this->symbol;
    }

    public function setSymbol(string $symbol): self
    {
        $this->symbol = $symbol;

        return $this;
    }

    function purgeDataOnDelete(): bool
    {
        return false;
    }

    public function getEntityName(): string
    {
        return "Devise";
    }

    public function routesHaveBeenTested(): bool
    {
        return true;
    }

    public function getMappingRouteToTaskName(): array
    {
        return [];
    }

    public function getEntityRouteName(): string
    {
        return "currencies";
    }

    public function getRoutesDontNeedingAuth(): array
    {
        return [
            "get",
        ];
    }

    public function requireBOTest(): bool
    {
        return true;
    }

}
