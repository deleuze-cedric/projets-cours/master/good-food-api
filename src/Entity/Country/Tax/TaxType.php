<?php

namespace App\Entity\Country\Tax;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use App\Entity\User\Employee\FranchisorEmployee;
use App\Interfaces\TestableEntityInterface;
use App\Repository\TaxTypeRepository;
use App\Traits\Fields\Date\ActivatedAtTrait;
use App\Traits\Fields\Date\CreatedAtTrait;
use App\Traits\Fields\Date\DeletedAtTrait;
use App\Traits\Fields\Date\UpdatedAtTrait;
use App\Traits\Fields\Identifiers\SlugNameTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\OneToMany;
use Gedmo\Mapping\Annotation\SoftDeleteable;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;


#[Entity(repositoryClass: TaxTypeRepository::class)]
#[SoftDeleteable(fieldName: "deletedAt")]
#[ApiResource(
    collectionOperations: [
        //	Tout le monde peut récupérer la liste des types de taxe
        "get"  => [],
        // Seuls les admins ou + peuvent créer un type de taxe
        "post" => [
            "security" => "is_granted('" . FranchisorEmployee::ROLE_ADMIN . "')",
        ],
    ],
    itemOperations: [
        //	Tout le monde peut récupérer la liste des types de taxe
        "get"    => [],
        // Seuls les admins ou + peuvent modifier un type de taxe
        "patch"  => [
            "security" => "is_granted('" . FranchisorEmployee::ROLE_ADMIN . "')",
        ],
        // Seuls les admins ou + peuvent supprimer un type de taxe
        "delete" => [
            "security" => "is_granted('" . FranchisorEmployee::ROLE_ADMIN . "')",
        ],
    ],
    subresourceOperations: [
        "api_tax_types_taxes_get_subresource" => [
            "security" => "is_granted('" . FranchisorEmployee::ROLE_ADMIN . "')",
        ],
    ],
    denormalizationContext: [
        "groups" => [
            "tax_type", "tax_type:write",
        ],
    ],
    normalizationContext: [
        "groups" => [
            "tax_type", "tax_type:read",
        ],
    ],
)]
class TaxType implements TestableEntityInterface
{

    use SlugNameTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;
    use ActivatedAtTrait;
    use DeletedAtTrait;

    #[Column(type: "string", length: 30)]
    #[NotBlank(message: "Le nom doit être défini.")]
    #[Length(
        min: 2,
        max: 30,
        minMessage: "Le nom doit faire plus de {{ limit }} caractères.",
        maxMessage: "Le nom doit faire moins de {{ limit }} caractères."
    )]
    #[Groups([
        'tax_type',
        "tax:read",
        'carrier:read',
        'carrier_data:read',
        'purchasable_product',
        'voucher:read',
        'voucher_data:read',
        'order:read',
        'order_line:read',
        'comment:read',
    ])]
    private string $name;

    /**
     * @var Tax[]|Collection
     */
    #[OneToMany(mappedBy: "type", targetEntity: Tax::class, orphanRemoval: true)]
    #[ApiSubresource]
    #[Groups([
        'purchasable_product:read',
        'voucher:read',
        'voucher_data:read',
        'order:read',
        'order_line:read',
        'comment:read',
    ])]
    private array|Collection $taxes;

    #[Pure] public function __construct()
    {
        $this->taxes = new ArrayCollection();
    }

    public function addTax(Tax $tax): self
    {
        if (!$this->taxes->contains($tax)) {
            $this->taxes[] = $tax;
            $tax->setType($this);
        }

        return $this;
    }

    function autoActiveOnCreation(): bool
    {
        return true;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Tax[]|Collection
     */
    public function getTaxes(): array|Collection
    {
        return $this->taxes;
    }

    function purgeDataOnDelete(): bool
    {
        return false;
    }

    public function removeTax(Tax $tax): self
    {
        if ($this->taxes->removeElement($tax)) {
            // set the owning side to null (unless already changed)
            if ($tax->getType() === $this) {
                $tax->setType(null);
            }
        }

        return $this;
    }

    public function getEntityName(): string
    {
        return "Type de taxes";
    }

    public function routesHaveBeenTested(): bool
    {
        return true;
    }

    public function getMappingRouteToTaskName(): array
    {
        return [
            "api_tax_types_taxes_get_subresource" => "Récupération des taxes associées à un type de taxe"
        ];
    }

    public function getEntityRouteName(): string
    {
        return "tax_types";
    }

    public function getRoutesDontNeedingAuth(): array
    {
        return [
            "get",
        ];
    }

    public function requireBOTest(): bool
    {
        return true;
    }

}
