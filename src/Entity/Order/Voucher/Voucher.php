<?php

namespace App\Entity\Order\Voucher;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\User\Employee\FranchiseEmployee;
use App\Entity\User\Employee\FranchisorEmployee;
use App\Interfaces\TestableEntityInterface;
use App\Repository\VoucherRepository;
use App\Traits\Fields\Date\ActivatedAtTrait;
use App\Traits\Fields\Date\CreatedAtTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Serializer\Annotation\Groups;


#[ORM\Entity(repositoryClass: VoucherRepository::class)]
#[ApiResource(
    collectionOperations: [
        'get'  => [
            'security' => 'is_granted("' . FranchiseEmployee::ROLE_LOGISTICIAN . '") or is_granted("' . FranchisorEmployee::ROLE_EDITOR . '")',
        ],
        'post' => [
            'security'                => 'is_granted("' . FranchisorEmployee::ROLE_EDITOR . '")',
            'denormalization_context' => [
                'groups' => [
                    'voucher', 'voucher:write', 'voucher:post',
                ],
            ],
        ],
    ],
    itemOperations: [
        'get'   => [],
        'patch' => [
            'security' => 'is_granted("' . FranchisorEmployee::ROLE_EDITOR . '")',
        ],
    ],
    denormalizationContext: [
        'groups' => [
            'voucher', 'voucher:write',
        ],
    ],
    normalizationContext: [
        'groups' => [
            'voucher', 'voucher:read',
        ],
    ]
)]
class Voucher implements TestableEntityInterface
{

    use CreatedAtTrait;
    use ActivatedAtTrait;

    #[ORM\Id]
    #[ORM\Column(type: 'string', length: 10)]
    #[ApiProperty(identifier: true)]
    #[Groups(['voucher:post', 'voucher:read', 'voucher_data:read'])]
    private string $code;

    /**
     * @var VoucherData[]|Collection
     */
    #[ORM\OneToMany(mappedBy: 'voucher', targetEntity: VoucherData::class)]
    private array|Collection $voucherData;

    #[Pure] public function __construct()
    {
        $this->voucherData = new ArrayCollection();
    }

    public function addVoucherData(VoucherData $voucherData): self
    {
        if (!$this->voucherData->contains($voucherData)) {
            $this->voucherData[] = $voucherData;
            $voucherData->setVoucher($this);
        }

        return $this;
    }

    protected function autoActiveOnCreation(): bool
    {
        return false;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    #[Groups('voucher:read')]
    public function getCurrentVoucherData(): ?VoucherData
    {
        $newestVoucherData = null;

        foreach ($this->voucherData as $voucher_data) {
            if (is_null($newestVoucherData) || $voucher_data->getCreatedAt() > $newestVoucherData->getCreateAt()) {
                $newestVoucherData = $voucher_data;
            }
        }

        return $newestVoucherData;
    }

    /**
     * @return Collection|VoucherData[]
     */
    public function getVoucherData(): array|Collection
    {
        return $this->voucherData;
    }

    public function removeVoucherData(VoucherData $voucherData): self
    {
        if ($this->voucherData->removeElement($voucherData)) {
            // set the owning side to null (unless already changed)
            if ($voucherData->getVoucher() === $this) {
                $voucherData->setVoucher(null);
            }
        }

        return $this;
    }

    public function getEntityName(): string
    {
        return "Coupon de réduction";
    }

    public function routesHaveBeenTested(): bool
    {
        return false;
    }

    public function getMappingRouteToTaskName(): array
    {
        return [];
    }

    public function getEntityRouteName(): string
    {
        return "vouchers";
    }

    public function getRoutesDontNeedingAuth(): array
    {
        return [];
    }

    public function requireBOTest(): bool
    {
        return true;
    }

    public function getEntityIdentifierName(): string
    {
        return 'code';
    }

}
