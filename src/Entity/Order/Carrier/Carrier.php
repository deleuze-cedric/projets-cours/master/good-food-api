<?php

namespace App\Entity\Order\Carrier;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use App\Entity\User\Employee\FranchisorEmployee;
use App\Interfaces\TestableEntityInterface;
use App\Repository\CarrierRepository;
use App\Traits\Fields\Date\ActivatedAtTrait;
use App\Traits\Fields\Date\DeletedAtTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\Table;
use Gedmo\Mapping\Annotation\SoftDeleteable;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Url;


#[Entity(repositoryClass: CarrierRepository::class)]
#[SoftDeleteable(fieldName: "deletedAt")]
#[ApiResource(
    collectionOperations: [
        "get"  => [],
        "post" => [
            "security"                => 'is_granted("' . FranchisorEmployee::ROLE_EDITOR . '")',
            "denormalization_context" => [
                'groups' => [
                    "carrier", "carrier:write", "carrier:post",
                ],
            ],
        ],
    ],
    itemOperations: [
        "get"    => [],
        "patch"  => [
            "security" => 'is_granted("' . FranchisorEmployee::ROLE_EDITOR . '")',
        ],
        "delete" => [
            "security" => 'is_granted("' . FranchisorEmployee::ROLE_EDITOR . '")',
        ],
    ],
    subresourceOperations: [
        'api_carriers_carrier_datas_get_subresource' => [
            "security" => 'is_granted("' . FranchisorEmployee::ROLE_EDITOR . '")',
        ],
    ],
    denormalizationContext: [
        'groups' => [
            "carrier", "carrier:write",
        ],
    ],
    normalizationContext: [
        'groups' => [
            "carrier", "carrier:read",
        ],
    ]
)]
class Carrier implements TestableEntityInterface
{

    use ActivatedAtTrait;
    use DeletedAtTrait;

    /**
     * @var CarrierData[]|ArrayCollection
     */
    #[OneToMany(mappedBy: 'carrierData', targetEntity: CarrierData::class, orphanRemoval: true)]
    #[ApiSubresource]
    private array|Collection $carrierData;

    #[Column(type: "string", length: 30)]
    #[NotBlank(message: "Le nom doit être renseigné.")]
    #[Length(
        min: 3,
        max: 30,
        minMessage: "Le nom doit faire au moins {{limit}} caractères.",
        maxMessage: "Le nom doit faire moins de {{limit}} caractères."
    )]
    #[Groups([
        'carrier',
        'carrier_data:read',
    ])]
    private string $name;

    #[Id()]
    #[ApiProperty(identifier: true)]
    #[Column(type: "string", length: 10)]
    #[NotBlank(message: "La référence doit être renseignée.")]
    #[Length(
        min: 10,
        max: 10,
        minMessage: "La référence doit faire au moins {{limit}} caractères.",
        maxMessage: "La référence doit faure au maximum {{limit}} caractères."
    )]
    #[Groups([
        'carrier:read',
        'carrier:post',
        'carrier_data:read',
    ])]
    private string $reference;

    #[Column(type: "string", length: 255, nullable: true)]
    #[Url]
    #[Groups([
        'carrier',
        'carrier_data:read',
    ])]
    private string $siteUrl;

    #[Pure] public function __construct()
    {
        $this->carrierData = new ArrayCollection();
    }

    public function addCarrierData(CarrierData $carrierData): self
    {
        if (!$this->carrierData->contains($carrierData)) {
            $this->carrierData[] = $carrierData;
            $carrierData->setCarrier($this);
        }

        return $this;
    }

    protected function autoActiveOnCreation(): bool
    {
        return true;
    }

    /**
     * @return Collection|CarrierData[]
     */
    public function getCarrierData(): array|Collection
    {
        return $this->carrierData;
    }

    #[Groups('carrier:read')]
    public function getCurrentCarrierData(): ?CarrierData
    {
        $newestCarrierData = null;

        foreach ($this->carrierData as $carrier) {
            if (is_null($newestCarrierData) || $carrier->getCreatedAt() > $newestCarrierData->getCreateAt()) {
                $newestCarrierData = $carrier;
            }
        }

        return $newestCarrierData;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Carrier
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getReference(): string
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     *
     * @return Carrier
     */
    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * @return string
     */
    public function getSiteUrl(): string
    {
        return $this->siteUrl;
    }

    /**
     * @param string $siteUrl
     *
     * @return Carrier
     */
    public function setSiteUrl(string $siteUrl): self
    {
        $this->siteUrl = $siteUrl;

        return $this;
    }

    public function purgeDataOnDelete(): bool
    {
        return true;
    }

    public function removeCarrierData(CarrierData $carrierData): self
    {
        if ($this->carrierData->removeElement($carrierData)) {
            // set the owning side to null (unless already changed)
            if ($carrierData->getCarrier() === $this) {
                $carrierData->setCarrier(null);
            }
        }

        return $this;
    }

    public function getEntityName(): string
    {
        return "Livreur";
    }

    public function routesHaveBeenTested(): bool
    {
        return false;
    }

    public function getMappingRouteToTaskName(): array
    {
        return [
            'api_carriers_carrier_datas_get_subresource' => 'Récupère la liste des données associées au livreur'
        ];
    }

    public function getEntityRouteName(): string
    {
        return "carriers";
    }

    public function getRoutesDontNeedingAuth(): array
    {
        return [
            "get"
        ];
    }

    public function requireBOTest(): bool
    {
        return true;
    }

    public function getEntityIdentifierName(): string
    {
        return 'reference';
    }

}