<?php

namespace App\Entity\Order\PaymentMethod;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Order\Order;
use App\Entity\User\Employee\FranchisorEmployee;
use App\Entity\User\SuperAdmin\SuperAdmin;
use App\Interfaces\TestableEntityInterface;
use App\Repository\PaymentMethodRepository;
use App\Traits\Fields\Date\ActivatedAtTrait;
use App\Traits\Fields\Date\CreatedAtTrait;
use App\Traits\Fields\Date\UpdatedAtTrait;
use App\Traits\Fields\Identifiers\SlugNameTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;


#[ORM\Entity(repositoryClass: PaymentMethodRepository::class)]
#[ApiResource(
    collectionOperations: [
        "get"  => [],
        "post" => [
            "security" => "is_granted('" . SuperAdmin::DEFAULT_ROLE_SUPER_ADMIN . "')",
        ],
    ],
    itemOperations: [
        "get"   => [],
        "patch" => [
            "security" => "is_granted('" . FranchisorEmployee::ROLE_EDITOR . "')",
        ],
    ],
    denormalizationContext: [
        "groups" => [
            "payment_method", "payment_method:write",
        ],
    ],
    normalizationContext: [
        "groups" => [
            "payment_method", "payment_method:read",
        ],
    ],
)]
abstract class PaymentMethod implements TestableEntityInterface
{

    use SlugNameTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;
    use ActivatedAtTrait;

    #[ORM\Column(type: 'string', length: 50)]
    #[NotBlank(message: "Le nom doit être défini")]
    #[Length(
        min: 2,
        max: 50,
        minMessage: "Le nom doit faire plus de {{ limit }} caractères",
        maxMessage: "Le nom doit faire moins de {{ limit }} caractères"
    )]
    #[Groups([
        'payment_method',
        'order:read',
    ])]
    private string $name;

    protected function autoActiveOnCreation(): bool
    {
        return false;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    abstract public function pay(Order $order);

    abstract public function refund(Order $order);

    abstract public function validPaymentIdentifier(?string $paymentIdentifier);

    public function getEntityName(): string
    {
        return "Méthode de paiement";
    }

    public function routesHaveBeenTested(): bool
    {
        return false;
    }

    public function getMappingRouteToTaskName(): array
    {
        return [];
    }

    public function getEntityRouteName(): string
    {
        return "payment_methods";
    }

    public function getRoutesDontNeedingAuth(): array
    {
        return [
            "get"
        ];
    }

    public function requireBOTest(): bool
    {
        return false;
    }
}
