<?php

namespace App\Entity\Media;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\Media\CreateMediaAction;
use App\Interfaces\TestableEntityInterface;
use App\Traits\Fields\Date\CreatedAtTrait;
use App\Traits\Fields\Identifiers\IDTrait;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation\Uploadable;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints\NotNull;
use Vich\UploaderBundle\Mapping\Annotation as Vich;


#[ORM\Entity]
/**
 * @Vich\Uploadable
 */
#[ApiResource(
    collectionOperations: [
        'get',
        'post' => [
            'controller'        => CreateMediaAction::class,
            'deserialize'       => false,
            'validation_groups' => ['Default', 'media:create'],
            'openapi_context'   => [
                'requestBody' => [
                    'content' => [
                        'multipart/form-data' => [
                            'schema' => [
                                'type'       => 'object',
                                'properties' => [
                                    'file' => [
                                        'type'   => 'string',
                                        'format' => 'binary',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    iri: 'https://schema.org/Media',
    itemOperations: ['get'],
    normalizationContext: ['groups' => ['media_object:read']]
)]
class Media implements TestableEntityInterface
{

    use IDTrait;
    use CreatedAtTrait;

    #[ApiProperty(iri: 'https://schema.org/contentUrl')]
    #[Groups(['media_object:read'])]
    public ?string $contentUrl = null;

    /**
     * @Vich\UploadableField(mapping="media_object", fileNameProperty="filePath")
     */
    #[NotNull(groups: ['media_object_create'])]
    public ?File $file = null;

    #[ORM\Column(nullable: true)]
    public ?string $filePath = null;

    #[SerializedName('mimeType')]
    #[Groups([
        'media:read',
        'product:read',
        'category:read',
        'restaurant:read',
        'order:read',
    ])]
    public function getMimeType(): ?string
    {
        return $this?->file?->getMimeType();
    }

    public function getEntityName(): string
    {
        return "Média";
    }

    public function routesHaveBeenTested(): bool
    {
        return false;
    }

    public function getMappingRouteToTaskName(): array
    {
        return [];
    }

    public function getEntityRouteName(): string
    {
        return "medias";
    }

    public function getRoutesDontNeedingAuth(): array
    {
        return [];
    }

    public function requireBOTest(): bool
    {
        return false;
    }
}