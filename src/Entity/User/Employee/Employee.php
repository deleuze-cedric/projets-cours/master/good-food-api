<?php

namespace App\Entity\User\Employee;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Entity\User\User;
use App\Repository\EmployeeRepository;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\InheritanceType;
use Doctrine\ORM\Mapping\Table;


#[ApiResource(
    collectionOperations: [
        'get' => [
            "security" => 'is_granted("' . Employee::DEFAULT_ROLE_EMPLOYEE . '")',
        ],
    ]
)]
#[Entity(repositoryClass: EmployeeRepository::class)]
#[InheritanceType("JOINED")]
#[DiscriminatorColumn(name: "type", type: "string")]
#[DiscriminatorMap(
    [
        "franchise"  => "FranchiseEmployee",
        "franchisor" => "FranchisorEmployee",
    ]
)]
#[ApiFilter(
    SearchFilter::class,
    properties: [
        'firstname'       => 'partial',
        'lastname'        => 'partial',
        'email'           => 'partial',
        'restaurant.name' => 'partial',
        'roles'           => 'partial',
    ]
)]
#[ApiFilter(
    OrderFilter::class,
    properties: [
        'firstname',
        'lastname',
        'email',
        'restaurant.slug' => 'partial',
        'roles'           => 'partial',
    ]
)]
abstract class Employee extends User
{

    const DEFAULT_ROLE_EMPLOYEE = 'ROLE_EMPLOYEE';

    const AVAILABLE_ROLES = [self::DEFAULT_ROLE_EMPLOYEE];

    public function getAvailableRoles(): array
    {
        return array_merge(parent::getAvailableRoles(), self::AVAILABLE_ROLES);
    }

    protected function initRoles(): array
    {
        return [self::DEFAULT_ROLE_EMPLOYEE];
    }

    public function getEntityName(): string
    {
        return "Employé";
    }

    public function routesHaveBeenTested(): bool
    {
        return true;
    }

    public function getMappingRouteToTaskName(): array
    {
        return [];
    }

    public function getEntityRouteName(): string
    {
        return "employees";
    }

    public function getRoutesDontNeedingAuth(): array
    {
        return [];
    }

    public function requireBOTest(): bool
    {
        return false;
    }

}
