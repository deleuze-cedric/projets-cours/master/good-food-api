<?php

namespace App\Entity\User\Employee;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Restaurant\Restaurant;
use App\Repository\FranchiseEmployeeRepository;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;
use Symfony\Component\Serializer\Annotation\Groups;


#[Entity(FranchiseEmployeeRepository::class)]
#[ApiResource(
    collectionOperations: [
        //	Les gérants, rh ou + peuvent récupérer la liste des clients
        "get"  => [
            "security" => 'is_granted("' . FranchiseEmployee::ROLE_LEAD . '") or is_granted("' . FranchisorEmployee::ROLE_HR . '")',
        ],
        // Les gérants, rh ou + peuvent créer un compte
        "post" => [
            "security"          => 'is_granted("' . FranchiseEmployee::ROLE_LEAD . '") or is_granted("' . FranchisorEmployee::ROLE_HR . '")',
            "validation_groups" => [
                "Default",
                "user:post:write",
            ],
        ],
    ],
    itemOperations: [
        //	Les gérants, rh, possésseurs ou + peuvent voir les infos du compte
        "get"                               => [
            "security" => 'is_granted("' . FranchiseEmployee::ROLE_LEAD . '") or is_granted("' . FranchisorEmployee::ROLE_HR . '") or object === user',
        ],
        //	Le possesseur du compte peut modifier les infos du compte
        "patch"                             => [
            "security" => "object === user",
        ],
        // Les logicisticiens ou + peuvent désactiver un compte
        'franchise_employee_active_account' => [
            'method'                  => 'PATCH',
            'path'                    => '/franchise_employees/{uuid}/active',
            "security"                => 'is_granted("' . FranchiseEmployee::ROLE_LEAD . '") or is_granted("' . FranchisorEmployee::ROLE_HR . '")',
            'denormalization_context' => [
                "groups" => [
                    "active:write",
                ],
            ],
        ],
    ],
    denormalizationContext: [
        "groups" => [
            "user", "user:write",
            "employee", "employee:write",
            "franchise_employee", "franchise_employee:write",
        ],
    ],
    normalizationContext: [
        "groups" => [
            "user", "user:read",
            "employee", "employee:read",
            "franchise_employee", "franchise_employee:read",
        ],
    ]

)]
class FranchiseEmployee extends Employee
{

    const DEFAULT_ROLE_FRANCHISE = 'ROLE_FRANCHISE';
    const ROLE_LOGISTICIAN = 'ROLE_LOGISTICIAN';
    const ROLE_RESTORATOR = 'ROLE_RESTORATOR';
    const ROLE_LEAD = 'ROLE_LEAD';

    const AVAILABLE_ROLES = [
        self::DEFAULT_ROLE_FRANCHISE,
        self::ROLE_LOGISTICIAN,
        self::ROLE_RESTORATOR,
        self::ROLE_LEAD,
    ];

    #[ManyToOne(targetEntity: Restaurant::class, inversedBy: "employees")]
    #[JoinColumn(nullable: false)]
    #[Groups("franchise_employee")]
    private Restaurant $restaurant;

    public function getAvailableRoles(): array
    {
        return array_merge(parent::getAvailableRoles(), self::AVAILABLE_ROLES);
    }

    public function getRestaurant(): ?Restaurant
    {
        return $this->restaurant;
    }

    public function setRestaurant(?Restaurant $restaurant): self
    {
        $this->restaurant = $restaurant;

        return $this;
    }

    protected function initRoles(): array
    {
        return array_merge(parent::initRoles(), [self::DEFAULT_ROLE_FRANCHISE]);
    }

    public function getEntityName(): string
    {
        return "Employé de restaurant franchisé";
    }

    public function routesHaveBeenTested(): bool
    {
        return true;
    }

    public function getMappingRouteToTaskName(): array
    {
        return [
            "franchise_employee_active_account" => "Active/désactive un compte employé"
        ];
    }

    public function getEntityRouteName(): string
    {
        return "franchise_employees";
    }

    public function getRoutesDontNeedingAuth(): array
    {
        return [];
    }

    public function requireBOTest(): bool
    {
        return true;
    }
}
