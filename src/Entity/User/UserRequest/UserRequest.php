<?php

namespace App\Entity\User\UserRequest;

use App\Entity\User\User;
use App\Interfaces\TestableEntityInterface;
use App\Repository\UserRequestRepository;
use App\Traits\Fields\Date\CreatedAtTrait;
use App\Traits\Fields\Identifiers\IDTrait;
use DateInterval;
use DateTime;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\InheritanceType;
use Doctrine\ORM\Mapping\PrePersist;
use Doctrine\ORM\Mapping\Table;
use Exception;
use Symfony\Component\Serializer\Annotation\Groups;


#[Entity(repositoryClass: UserRequestRepository::class)]
#[InheritanceType("JOINED")]
#[DiscriminatorColumn(name: "type", type: "string")]
#[DiscriminatorMap(
    [
        "newPassword"         => "NewPasswordRequest",
        "accountConfirmation" => "AccountConfirmationRequest",
    ]
)]
#[HasLifecycleCallbacks()]
abstract class UserRequest implements TestableEntityInterface
{

    use IDTrait;
    use CreatedAtTrait;

    #[Column(type: "boolean")]
    #[Groups(['user_request:read'])]
    private bool $alreadyUsed;

    #[Column(type: "datetime")]
    #[Groups(['user_request:read'])]
    private ?DateTime $expiredAt;

    public function getAlreadyUsed(): ?bool
    {
        return $this->alreadyUsed;
    }

    public function setAlreadyUsed(): self
    {
        $this->alreadyUsed = true;

        return $this;
    }

    public abstract function getDeadlineInMinute(): int;

    public abstract function getDelayInMinuteBetweenTwoRequest(): int;

    public function getExpiredAt(): ?DateTime
    {
        return $this->expiredAt;
    }

    public function setExpiredAt(DateTime $expiredAt): self
    {
        $this->expiredAt = $expiredAt;

        return $this;
    }

    public abstract function getUserTarget(): ?User;

    /**
     * @throws Exception
     */
    #[PrePersist()]
    public function onCreation()
    {
        $this->alreadyUsed = false;
        $this->expiredAt = new DateTime();
        $this->expiredAt->add(new DateInterval('PT' . $this->getDeadlineInMinute() . 'M'));
    }

    public function requestHasExpired(): bool
    {
        $time = new DateTime();

        return $time->getTimestamp() > $this->expiredAt->getTimestamp();
    }

    public function getEntityName(): string
    {
        return "Requête utilisateur";
    }

    public function routesHaveBeenTested(): bool
    {
        return true;
    }

    public function getMappingRouteToTaskName(): array
    {
        return [];
    }

    public function getEntityRouteName(): string
    {
        return "user_requests";
    }

    public function getRoutesDontNeedingAuth(): array
    {
        return [];
    }

    public function requireBOTest(): bool
    {
        return false;
    }
}
