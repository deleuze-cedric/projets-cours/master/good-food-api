<?php

namespace App\Entity\User\UserRequest;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\User\Request\UserRequestCreationAction;
use App\Controller\User\Request\UserRequestValidationAction;
use App\Entity\User\Customer\Customer;
use App\Entity\User\SuperAdmin\SuperAdmin;
use App\Entity\User\User;
use App\Repository\AccountConfirmationRequestRepository;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Serializer\Annotation\Groups;


#[Entity(repositoryClass: AccountConfirmationRequestRepository::class)]
#[ApiResource(
    collectionOperations: [
        "post"                           => [
            "controller" => UserRequestCreationAction::class,
            // "security_post_denormalize" => "object.getCustomerTarget() === user",
        ],
    ],
    itemOperations: [
        "validate" => [
            'method'                  => 'PATCH',
            'path'                    => '/account_confirmation_requests/{id}/validate',
            "controller"              => UserRequestValidationAction::class,
            "denormalization_context" => [
                'groups' => [
                    'user_request:validate:write', 'user_request:validate:write',
                    'account_confirm_request:validate:write', 'account_confirm_request:validate:write',
                ],
            ],
        ],
        "get"      => [
            "security" => 'is_granted("' . SuperAdmin::DEFAULT_ROLE_SUPER_ADMIN . '")',
        ],
    ],
    denormalizationContext: [
        "groups" => [
            'user_request', 'user_request:write',
            'account_confirm_request', 'account_confirm_request:write',
        ],
    ],
    normalizationContext: [
        "groups" => [
            'user_reqdessertsuest', 'user_request:read',
            'account_confirm_request', 'account_confirm_request:read',
        ],
    ]

)]
class AccountConfirmationRequest extends UserRequest
{

    const DEADLINE_IN_MINUTE = 15;
    const DELAY_IN_MINUTE_BETWEEN_TWO_REQUEST = 2;

    #[ManyToOne(targetEntity: Customer::class)]
    #[JoinColumn(referencedColumnName: "uuid", nullable: false)]
    #[Groups(['account_confirm_request'])]
    #[ApiProperty(readableLink: false, writableLink: false)]
    private ?Customer $customerTarget;

    public function getCustomerTarget(): ?Customer
    {
        return $this->customerTarget ?? null;
    }

    public function setCustomerTarget(?Customer $customerTarget): self
    {
        $this->customerTarget = $customerTarget;

        return $this;
    }

    public function getDeadlineInMinute(): int
    {
        return self::DEADLINE_IN_MINUTE;
    }

    public function getDelayInMinuteBetweenTwoRequest(): int
    {
        return self::DELAY_IN_MINUTE_BETWEEN_TWO_REQUEST;
    }

    #[Pure] public function getUserTarget(): User
    {
        return $this->getCustomerTarget();
    }

    public function getEntityName(): string
    {
        return "Requête de création de compte";
    }

    public function routesHaveBeenTested(): bool
    {
        return true;
    }

    public function getMappingRouteToTaskName(): array
    {
        return [
            "validate" => "Valide la requête",
        ];
    }

    public function getEntityRouteName(): string
    {
        return "account_confirmation_requests";
    }

    public function getRoutesDontNeedingAuth(): array
    {
        return [
            "post",
            "validate",
        ];
    }

}
