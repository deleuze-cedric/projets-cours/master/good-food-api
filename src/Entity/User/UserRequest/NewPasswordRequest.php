<?php

namespace App\Entity\User\UserRequest;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\User\Request\UserRequestCreationAction;
use App\Controller\User\Request\UserRequestValidationAction;
use App\Entity\User\SuperAdmin\SuperAdmin;
use App\Entity\User\User;
use App\Repository\NewPasswordRequestRepository;
use App\Service\Tools\Tools;
use App\Validator\Constraint\CheckReservationNotAlreadyDone;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Regex;


#[Entity(repositoryClass: NewPasswordRequestRepository::class)]
#[ApiResource(
    collectionOperations: [
        "post" => [
            "controller"                => UserRequestCreationAction::class
        ],
    ],
    itemOperations: [
        "validate" => [
            'method'                  => 'PATCH',
            'path'                    => '/new_password_requests/{id}/validate',
            "controller"              => UserRequestValidationAction::class,
            "denormalization_context" => [
                'groups' => [
                    'user_request:validate:write', 'user_request:validate:write',
                    'new_password_request:validate', 'new_password_request:validate:write',
                ],
            ],
            "validation_groups"       => ['new_password_request:validate:write'],
        ],
        "get"      => [
            "security" => 'is_granted("' . SuperAdmin::DEFAULT_ROLE_SUPER_ADMIN . '")',
        ],
    ],
    denormalizationContext: [
        "groups" => [
            'user_request', 'user_request:write',
            'new_password_request', 'new_password_request:write',
        ],
    ],
    normalizationContext: [
        "groups" => [
            'user_request', 'user_request:read',
            'new_password_request', 'new_password_request:read',
        ],
    ],

)]
class NewPasswordRequest extends UserRequest
{

    const DEADLINE_IN_MINUTE = 15;
    const DELAY_IN_MINUTE_BETWEEN_TWO_REQUEST = 2;

    #[NotBlank(
        message: Tools::PASSWORD_ERROR,
        groups: ["new_password_request:validate:write"]
    )]
    #[Regex(
        pattern: Tools::PASSWORD_REGEX,
        message: Tools::PASSWORD_ERROR,
        groups: ["new_password_request:validate:write"]
    )]
    #[Groups("new_password_request:validate:write")]
    private ?string $password = null;

    #[JoinColumn(referencedColumnName: "uuid", nullable: false)]
    #[ManyToOne(targetEntity: User::class)]
    #[ApiProperty(readableLink: false, writableLink: false)]
    private ?User $userTarget;

    #[Groups(['new_password_request'])]
    #[NotNull(message: 'L\'email doit être renseigné')]
    #[Email(message: 'L\'email doit être valide')]
    #[CheckReservationNotAlreadyDone]
    public string $email;

    public function getDeadlineInMinute(): int
    {
        return self::DEADLINE_IN_MINUTE;
    }

    public function getDelayInMinuteBetweenTwoRequest(): int
    {
        return self::DELAY_IN_MINUTE_BETWEEN_TWO_REQUEST;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string|null $password
     */
    public function setPassword(?string $password): void
    {
        $this->password = $password;
    }

    public function getUserTarget(): ?User
    {
        return $this->userTarget ?? null;
    }

    public function setUserTarget(?User $userTarget): self
    {
        $this->userTarget = $userTarget;

        return $this;
    }

    public function getEntityName(): string
    {
        return "Requête maj mdp";
    }

    public function routesHaveBeenTested(): bool
    {
        return true;
    }

    public function getMappingRouteToTaskName(): array
    {
        return [
            "validate" => "Valide la requête",
        ];
    }

    public function getEntityRouteName(): string
    {
        return "new_password_requests";
    }

    public function getRoutesDontNeedingAuth(): array
    {
        return [
            "post",
            "validate",
        ];
    }

}
