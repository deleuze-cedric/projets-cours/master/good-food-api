<?php

namespace App\Entity\User\Customer;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use ApiPlatform\Core\Bridge\Doctrine\Common\Filter\DateFilterInterface;
use ApiPlatform\Core\Bridge\Doctrine\Common\Filter\SearchFilterInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Controller\Restaurant\ListRestaurantsInOrdersAction;
use App\Controller\Restaurant\ListRestaurantsInReservationsAction;
use App\Controller\User\Customer\ListCustomersInOrdersAction;
use App\Controller\User\Customer\ListCustomersInReservationsAction;
use App\Entity\Order\Order;
use App\Entity\Order\Voucher\VoucherData;
use App\Entity\Reservation;
use App\Entity\User\Comment\Comment;
use App\Entity\User\Employee\FranchiseEmployee;
use App\Entity\User\User;
use App\Repository\CustomerRepository;
use App\Traits\Fields\Date\ConfirmedAtTrait;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OrderBy;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;


#[Entity(repositoryClass: CustomerRepository::class)]
#[ApiFilter(
    filterClass: SearchFilter::class,
    properties: [
        'uuid'      => SearchFilterInterface::STRATEGY_EXACT,
        'firstname' => SearchFilterInterface::STRATEGY_PARTIAL,
        'lastname'  => SearchFilterInterface::STRATEGY_PARTIAL,
        'email'     => SearchFilterInterface::STRATEGY_PARTIAL,
    ]
)]
#[ApiFilter(
    filterClass: OrderFilter::class,
    properties: [
        'createdAt' => 'desc',
        'uuid',
        'firstname',
        'lastname',
        'email',
    ],
    arguments: ['orderParameterName' => 'order']
)]
#[ApiFilter(
    filterClass: BooleanFilter::class,
    properties: [
        'active',
    ]
)]
#[ApiFilter(
    filterClass: DateFilter::class,
    properties: [
        'createdAt' => DateFilterInterface::EXCLUDE_NULL,
        'updatedAt' => DateFilterInterface::EXCLUDE_NULL,
    ]
)]
#[ApiResource(
    collectionOperations: [
        //	Les logisticiens ou + peuvent récupérer la liste des clients
        "get"             => [
            "security" => 'is_granted("' . FranchiseEmployee::ROLE_LOGISTICIAN . '")',
        ],
        // Tout le monde peut créer un compte client
        "post"            => [
            "validation_groups"       => [
                "Default",
                "user:post:write",
            ],
            'denormalization_context' => [
                "groups" => [
                    "user", "user:write",
                    "user:post", "user:post:write",
                    "customer", "customer:write",
                ],
            ],
        ],
        "in_orders"       => [
            "method"             => "get",
            "path"               => "/customers/in_orders",
            "controller"         => ListCustomersInOrdersAction::class,
            "pagination_enabled" => false,
        ],
        "in_reservations" => [
            "method"             => "get",
            "path"               => "/customers/in_reservations",
            "controller"         => ListCustomersInReservationsAction::class,
            "pagination_enabled" => false,
        ],
    ],
    itemOperations: [
        //	Les logisticiens ou + ou le possesseur du compte peuvent récupérer les infos du compte
        "get"                     => [
            "security" => 'is_granted("' . FranchiseEmployee::ROLE_LOGISTICIAN . '") or object === user',
        ],
        "get_cart"                => [
            "security"              => 'is_granted("' . Customer::DEFAULT_ROLE_CUSTOMER . ')',
            'normalization_context' => [
                "groups" => [
                    "order",
                    "order:read",
                ],
            ],
            "output",
        ],
        //	Le possesseur <du compte peut modifier les infos du compte
        "patch"                   => [
            "security" => "object === user",
        ],
        //	Le possesseur du compte peut supprimer son compte
        "delete"                  => [
            "security" => "object === user",
        ],
        // Les logicisticiens ou + peuvent désactiver un compte
        'customer_active_account' => [
            'method'                  => 'PATCH',
            'path'                    => '/customers/{uuid}/active',
            "security"                => 'is_granted("' . FranchiseEmployee::ROLE_LOGISTICIAN . '")',
            'denormalization_context' => [
                "groups" => [
                    "active:write",
                ],
            ],
        ],
    ],
    denormalizationContext: [
        "groups" => [
            "user", "user:write",
            "customer", "customer:write",
        ],
    ],
    normalizationContext: [
        "groups" => [
            "user", "user:read",
            "customer", "customer:read",
        ],
    ]
)]
#[ApiFilter(
    SearchFilter::class,
    properties: [
        'firstname' => 'partial',
        'lastname'  => 'partial',
        'email'     => 'partial',
    ]
)]
#[ApiFilter(
    OrderFilter::class,
    properties: [
        'firstname',
        'lastname',
        'email',
    ]
)]
class Customer extends User
{

    const DEFAULT_ROLE_CUSTOMER = 'ROLE_CUSTOMER';

    const AVAILABLE_ROLES = [self::DEFAULT_ROLE_CUSTOMER];

    use ConfirmedAtTrait;

    /**
     * @var Address[]|Collection
     */
    #[OneToMany(mappedBy: "customer", targetEntity: Address::class)]
    #[ApiSubresource]
    private array|Collection $addresses;

    #[Column(type: "date")]
    #[Assert\LessThan(value: "-18 years", message: "Vous devez au moins avoir 18 ans pour créer un compte.")]
    #[Assert\NotNull(message: "La date de naissancce doit être renseignée.")]
    #[Groups([
        'customer',
        'account_confirm_request:read',
        'voucher:read',
        'voucher_data:read',
        'comment:read',
        'order:read',
        'reservation:read',
    ])]
    private DateTime $birthdate;

    /**
     * @var Order[]|Collection
     */
    #[OneToMany(mappedBy: 'customer', targetEntity: Order::class)]
    #[ApiSubresource]
    private array|Collection $orders;

    /**
     * @var VoucherData[]|Collection
     */
    #[ManyToMany(targetEntity: VoucherData::class, mappedBy: 'onlyForCustomers')]
    #[ApiSubresource]
    private array|Collection $voucher;

    /**
     * @var Comment[]|Collection
     */
    #[OneToMany(mappedBy: "customer", targetEntity: Comment::class)]
    #[ApiSubResource]
    private array|Collection $comments;

    /**
     * @var Reservation[]|Collection
     */
    #[OneToMany(mappedBy: 'customer', targetEntity: Reservation::class, orphanRemoval: true)]
    #[ApiSubResource]
    private array|Collection $reservations;

    #[Pure] public function __construct()
    {
        $this->addresses = new ArrayCollection();
        $this->voucher = new ArrayCollection();
        $this->orders = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->reservations = new ArrayCollection();
    }

    public function addAddress(Address $address): self
    {
        if (!$this->addresses->contains($address)) {
            $this->addresses[] = $address;
            $address->setCustomer($this);
        }

        return $this;
    }

    public function addOrder(Order $order): self
    {
        if (!$this->orders->contains($order)) {
            $this->orders[] = $order;
            $order->setCustomer($this);
        }

        return $this;
    }

    public function addVoucher(VoucherData $voucher): self
    {
        if (!$this->voucher->contains($voucher)) {
            $this->voucher[] = $voucher;
            $voucher->addOnlyForCustomer($this);
        }

        return $this;
    }

    /**
     * @return Address[]|Collection
     */
    public function getAddresses(): array|Collection
    {
        return $this->addresses;
    }

    public function getAvailableRoles(): array
    {
        return array_merge(parent::getAvailableRoles(), self::AVAILABLE_ROLES);
    }

    /**
     * @return DateTime
     */
    public function getBirthdate(): DateTime
    {
        return $this->birthdate;
    }

    /**
     * @param DateTime $birthdate
     */
    public function setBirthdate(DateTime $birthdate): void
    {
        $birthdate->setTime(0, 0);
        $this->birthdate = $birthdate;
    }

    /**
     * @return Collection|Order[]
     */
    public function getOrders(): array|Collection
    {
        return $this->orders;
    }

    /**
     * @return Collection|VoucherData[]
     */
    public function getVoucher(): array|Collection
    {
        return $this->voucher;
    }

    public function getComments(): array|Collection
    {
        return $this->comments;
    }

    protected function initRoles(): array
    {
        return [self::DEFAULT_ROLE_CUSTOMER];
    }

    public function purgeData()
    {
        parent::purgeData();
        $this->birthdate = self::getDateTimeClearValue();

        foreach ($this->addresses as $address) {
            $address->purgeData();
        }
    }

    public function removeAddress(Address $address): self
    {
        if ($this->addresses->removeElement($address)) {
            // set the owning side to null (unless already changed)
            if ($address->getCustomer() === $this) {
                $address->setCustomer(null);
            }
        }

        return $this;
    }

    public function removeOrder(Order $order): self
    {
        if ($this->orders->removeElement($order)) {
            // set the owning side to null (unless already changed)
            if ($order->getCustomer() === $this) {
                $order->setCustomer(null);
            }
        }

        return $this;
    }

    public function removeVoucher(VoucherData $voucher): self
    {
        if ($this->voucher->removeElement($voucher)) {
            $voucher->removeOnlyForCustomer($this);
        }

        return $this;
    }


    public function getEntityName(): string
    {
        return "Client";
    }

    public function routesHaveBeenTested(): bool
    {
        return true;
    }

    public function getMappingRouteToTaskName(): array
    {
        return [
            "api_customers_addresses_get_subresource" => "Récupère les adresses du client",
            "api_customers_vouchers_get_subresource"  => "Récupère les bons de réduction du client",
        ];
    }

    public function getEntityRouteName(): string
    {
        return "customers";
    }

    public function getRoutesDontNeedingAuth(): array
    {
        return [];
    }

    public function requireBOTest(): bool
    {
        return true;
    }

    #[Groups('customer:read')]
    public function getNbOrders(): int
    {
        return $this->orders->count();
    }

    #[Groups('customer:read')]
    public function getNbComments(): int
    {
        return $this->comments->count();
    }

    #[Groups('customer:read')]
    public function getNbReservations(): int
    {
        return $this->reservations->count();
    }

    #[Groups('customer:read')]
    public function getNbAddresses(): int
    {
        return $this->addresses->count();
    }

    #[Groups('customer:read')]
    public function getMeanComments(): float
    {
        $nbComments = $this->getNbComments();

        if ($nbComments < 1) {
            return 0;
        }

        $total = 0;

        foreach ($this->comments as $comment) {
            $total += $comment->getNote();
        }

        return $total / $nbComments;
    }

    #[Groups('customer:read')]
    public function getNoteDivision(): array
    {
        $notes = [
            0 => [
                'nb'   => 0,
                'rate' => 0,
            ],
            1 => [
                'nb'   => 0,
                'rate' => 0,
            ],
            2 => [
                'nb'   => 0,
                'rate' => 0,
            ],
            3 => [
                'nb'   => 0,
                'rate' => 0,
            ],
            4 => [
                'nb'   => 0,
                'rate' => 0,
            ],
            5 => [
                'nb'   => 0,
                'rate' => 0,
            ],
        ];

        $nbComments = $this->getNbComments();

        if ($nbComments >= 1) {

            foreach ($this->comments as $comment) {
                $notes[$comment->getNote()]['nb'] += 1;
            }

            foreach ($notes as &$note) {
                $note['rate'] = ($note['nb'] / $nbComments) * 100;
            }

        }

        return $notes;
    }

    /**
     * @return Collection<int, Reservation>
     */
    public function getReservations(): Collection
    {
        return $this->reservations;
    }

    public function addReservation(Reservation $reservation): self
    {
        if (!$this->reservations->contains($reservation)) {
            $this->reservations[] = $reservation;
            $reservation->setCustomer($this);
        }

        return $this;
    }

    public function removeReservation(Reservation $reservation): self
    {
        if ($this->reservations->removeElement($reservation)) {
            // set the owning side to null (unless already changed)
            if ($reservation->getCustomer() === $this) {
                $reservation->setCustomer(null);
            }
        }

        return $this;
    }

}
