<?php

namespace App\Validator\Constraint;

use Attribute;
use Symfony\Component\Validator\Constraint;

#[Attribute]
class CheckReservationNotAlreadyDone extends Constraint
{

}