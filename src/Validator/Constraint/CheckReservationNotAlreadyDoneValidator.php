<?php

namespace App\Validator\Constraint;

use App\Entity\Reservation;
use App\Repository\ReservationRepository;
use App\Repository\UserRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

#[\Attribute]
class CheckReservationNotAlreadyDoneValidator extends ConstraintValidator
{

    public function __construct(
        private ReservationRepository $reservationRepository
    )
    {
    }

    public function validate(mixed $value, Constraint $constraint)
    {
        if (!$constraint instanceof CheckReservationNotAlreadyDone) {
            throw new UnexpectedTypeException($constraint, CheckReservationNotAlreadyDone::class);
        }

        $reservation = $this->context->getObject();

        if (!$reservation instanceof Reservation) {
            $this->context->buildViolation('La contrainte ne s\'applique qu\'aux réservations')->addViolation();
        }

        $reservationAlreadyExists = $this->reservationRepository->reservationAlreadyExists($reservation);

        if ($reservationAlreadyExists) {
            $this->context->buildViolation('Vous avez déjà une réservation en cours pour ce restaurant, allez la voir dans votre profil !')->addViolation();
        }
    }

}